"""
title: ddtree.py
language: python 3.5

author: bue

description:
    dictionary of dictionary tree opbejct inmpelmentation
run:
    from pyglue import ddtree
"""

import csv
import json
import re
import sys
import numpy as np

import matplotlib.pyplot as plt

# re data type
# constant
ts_none = ('NONE','None','none','Null','Null','null')  #'empty'

# function
def retype(o_value):
    """ string to type """
    if not((type(o_value) == bool) or (o_value == None)):
        # if not boolean or None
        if not((type(o_value) == float) or (type(o_value) == int) or (type(o_value) == complex)):
            # strip
            o_value = str(o_value)
            o_value = o_value.strip()

            # number
            if re.fullmatch("[-|+|0-9|.|e|E|j|(|)]+", o_value):
                # complex
                if re.search("j", o_value):
                    try:
                        o_value = complex(o_value)
                    except ValueError:
                        pass  # still a string
                # float
                elif re.search("[.|e|E]", o_value):
                    try:
                        o_value = float(o_value)
                    except ValueError:
                        pass  # still a string
                # integer
                else:
                    try:
                        o_value = int(o_value)
                    except ValueError:
                        pass  # still a string

            # boolean
            elif (o_value in ('TRUE','True','true')):
                o_value = True
            elif (o_value in ('FALSE','False','false')):
                o_value = False
            elif (o_value in ts_none):
                o_value = None

            # real string
            else:
                pass
    # output
    return(o_value)


def tsv_writeline(d_calxargs):
    """ tsv_write internal method """
    # extract d_calxargs
    d_leaf = d_calxargs["d_leaf"]
    ls_branch = d_calxargs["ls_branch"]
    s_ofile = d_calxargs["s_ofilename"]

    # handle leaf keys
    ls_key = list(d_leaf.keys())
    ls_key.sort()

    # handle data row
    s_branch = "\t".join(ls_branch)
    s_leaf = None
    for s_key in ls_key:
        if (s_leaf == None):
            s_leaf = str(d_leaf[s_key])
        else:
            s_leaf = s_leaf + "\t" + str(d_leaf[s_key])

    # write line
    f = open(s_ofile, 'a')
    s_out = s_branch + "\t" + s_leaf + "\n"
    f.write(s_out)

    # closse output file
    f.close()
    return(None)


# main class
class ddtree(dict):
    """ dictionary tree object class """

    def __init__(self):
        """ set dictionry tree seed """
        #self = {}
        # bue 20151223: this is not working
        self.set_ddtree()
        self.set_ifilename()
        self.set_ofilename()
        self.set_value()
        self.set_twig()
        self.set_verbose()
        self.set_development()

    def set_ddtree(self, s_ddtree=None):
        """ input ddtree sting """
        if not hasattr(self, "s_ddtree"):
            self.s_ddtree = None
        self.s_ddtree = s_ddtree


    def set_ifilename(self, s_ifilename=None):
        """ input path and filename """
        if not hasattr(self, "s_ifilename"):
            self.s_ifilename = None
        self.s_ifilename = s_ifilename

    def set_ofilename(self, s_ofilename=None):
        """ input path and filename """
        if not hasattr(self, "s_ofilename"):
            self.s_ofilename = None
        self.s_ofilename = s_ofilename

    def set_value(self, s_value="value"):
        """ input basic value label.
        one leaf have to be labeled like this.
        otherwise leafs get not detected. """
        if not hasattr(self, "s_value"):
            self.s_value = None
        self.s_value = s_value

    def set_twig(self, s_twig="twig"):
        """ input twig label.
            this label will be used name twig column at tsv_write. """
        if not hasattr(self, "s_twig"):
            self.s_twig = None
        self.s_twig = s_twig

    def set_verbose(self, b_verbose=False):
        """ boolean to set verbosity of the standard output """
        if not hasattr(self, "b_verbose"):
            self.b_verbose = None
        self.b_verbose = b_verbose

    def set_development(self, b_development=False):
        """ boolean to set verbosity of the standard output """
        if not hasattr(self, "b_development"):
            self.b_development = None
        self.b_development = b_development

    def get(self,  ls_branchtwigleaf=None, s_input ="value"):
        """
        get values from specified ls_branchtwigleaf.
        ls_branchtwigleaf=None get s_input
        from all bracnhe twig leafs
        """
        # bue: this is hardcore
        # initialize output
        l_get = None
        # intialize stack tape and branch path tape
        ls_stack = ["#root#"]  # stack tape
        ls_branch = []  # brach path tape
        s_key = None
        d_leaf = self
        ld_stack = [d_leaf]  # list of dictionaries
        ls_key = list(d_leaf.keys())
        if self.b_verbose:
            print("\nInitialization:")
            print("Stack :", ls_stack)
            print("Branch :", ls_branch)
            print("Key :", s_key)
            print("Ls_Key :", ls_key)
            #print("ld_Stack:", ld_stack)

        while (len(ls_stack) > 0):
            # process

            # on leaf
            if (s_input in ls_key):

                # on ls_branchtwigleaf?
                if (ls_branchtwigleaf == None):
                    b_get = True
                else:
                    # and gate
                    b_get = False
                    for s_get in ls_branchtwigleaf:
                        if s_get in ls_branch:
                            b_get = True
                        else:
                            b_get = False
                if self.b_verbose:
                    print("Get leaf :", b_get)

                # get leaf values
                if b_get:
                    o_result = d_leaf[s_input]
                    if (l_get  == None):
                        l_get = []
                    if (type(o_result) == list):  # only list, neither set nor tuple
                        l_get.extend(o_result)
                    else:
                        l_get.append(o_result)

                # handle stack
                if (len(ls_stack) != 1):
                    ls_branch.pop(-1)
                else:
                    # one leaf tree case
                    ls_stack.append("#fork#")
                    ls_branch.append("#root#")

            # on branch
            else:
                # handle d stack
                for s_key in ls_key:
                    ld_stack.append(d_leaf[s_key])
                # handle s stack
                ls_key.insert(0, "#fork#")
                ls_stack.extend(ls_key)

            # process branch
            s_key = ls_stack.pop(-1)
            d_leaf = ld_stack.pop(-1)
            ls_key = list(d_leaf.keys())


            # on fork ?
            while (s_key == "#fork#"):
                s_key = ls_stack.pop(-1)
                # not on root ?
                if (s_key != "#root#"):
                    ls_branch.pop(-1)

            # update branch
            ls_branch.append(s_key)

            # output
            if self.b_verbose:
                print("\nStack :", ls_stack)
                print("Branch :", ls_branch)
                print("Key :", s_key)
                print("Ls_Key :", ls_key)
                #print("Ld_Stack :", ld_stack)
                #print("Self :", self)
        # output
        return(l_get)


    def update_branch(self, d_leaf, ls_branch):
        """ update dictionary tree at ls_branch with d_leaf"""
        # bue 20151105: there should be a geneeric way to do so.
        # bue 20160125: can be written as loop, accessing dictionary in dictionary through call by reference.
        # input handle
        i_deep = len(ls_branch)
        # implementation limit
        if (i_deep > 5):
            sys.exit("Error: ddtree.update_branch deep {}".format(i_deep) +
                     " implementation missung.")
        # update deep 0
        if (i_deep == 0):
            self = d_leaf

        # update deep 1
        if (i_deep >= 1):
            s_key0 = ls_branch[0]
            try:
                d_fractal = self[s_key0]
            except KeyError:
                self.update({s_key0 : {}})
            if (i_deep == 1):
                self.update({s_key0 : d_leaf})

        # update deep 2
        if (i_deep >= 2):
            s_key1 = ls_branch[1]
            try:
                d_fractal = self[s_key0][s_key1]
            except KeyError:
                self[s_key0].update({s_key1 : {}})
            if (i_deep == 2):
                self[s_key0].update({s_key1: d_leaf})

        # update deep 3
        if (i_deep >= 3):
            s_key2 = ls_branch[2]
            try:
                d_fractal = self[s_key0][s_key1][s_key2]
            except KeyError:
                self[s_key0][s_key1].update({s_key2 : {}})
            if (i_deep == 3):
                self[s_key0][s_key1].update({s_key2 : d_leaf})

        # update deep 4
        if (i_deep >= 4):
            s_key3 = ls_branch[3]
            try:
                d_fractal = self[s_key0][s_key1][s_key2][s_key3]
            except KeyError:
                self[s_key0][s_key1][s_key2].update({s_key3 : {}})
            if (i_deep == 4):
                self[s_key0][s_key1][s_key2].update({ s_key3 : d_leaf})

        # update deep 5
        if (i_deep >= 5):
            s_key4 = ls_branch[4]
            try:
                d_fractal = self[s_key0][s_key1][s_key2][s_key3][s_key4]
            except KeyError:
                self[s_key0][s_key1][s_key2][s_key3].update({s_key4 : {}})
            if (i_deep == 5):
                self[s_key0][s_key1][s_key2][s_key3].update({ s_key4 : d_leaf})


    def put(self, s_input, o_value, ls_branchtwigleaf=None):
        """
        put s_input : o_value to specified branch twig leaf.
        ls_branchtwigleaf=None will put s_input : o_value to all leafs.
        """
        # bue: this is hardcore
        # intialize stack tape and branch path tape
        ls_stack = ["#root#"]  # stack tape
        ls_branch = []  # brach path tape
        s_key = None
        d_leaf = self
        ld_stack = [d_leaf]  # list of dictionaries
        ls_key = list(d_leaf.keys())
        if self.b_verbose:
            print("\nInitialization:")
            print("Stack :", ls_stack)
            print("Branch :", ls_branch)
            print("Key :", s_key)
            print("Ls_Key :", ls_key)
            #print("ld_Stack:", ld_stack)

        while (len(ls_stack) > 0):
            # process

            # on leaf
            if (self.s_value in ls_key):
                # on ls_branchtwigleaf?
                if (ls_branchtwigleaf == None):
                    b_put = True
                else:
                    # and gate
                    b_put = False
                    for s_put in ls_branchtwigleaf:
                        if s_put in ls_branch:
                            b_put = True
                        else:
                            b_put = False
                if self.b_verbose:
                    print("Put input :", b_put)

                # write input value to leaf
                if b_put:
                    d_leaf.update({ s_input : o_value})
                    self.update_branch(d_leaf=d_leaf, ls_branch=ls_branch)

                # handle stack
                if (len(ls_stack) != 1):
                    ls_branch.pop(-1)
                else:
                    # one leaf tree case
                    ls_stack.append("#fork#")
                    ls_branch.append("#root#")

            # on branch
            else:
                # handle d stack
                for s_key in ls_key:
                    ld_stack.append(d_leaf[s_key])
                # handle s stack
                ls_key.insert(0, "#fork#")
                ls_stack.extend(ls_key)

            # process branch
            s_key = ls_stack.pop(-1)
            d_leaf = ld_stack.pop(-1)
            ls_key = list(d_leaf.keys())


            # on fork ?
            while (s_key == "#fork#"):
                s_key = ls_stack.pop(-1)
                # not on root ?
                if (s_key != "#root#"):
                    ls_branch.pop(-1)

            # update branch
            ls_branch.append(s_key)

            # output
            if self.b_verbose:
                print("\nStack :", ls_stack)
                print("Branch :", ls_branch)
                print("Key :", s_key)
                print("Ls_Key :", ls_key)
                #print("Ld_Stack :", ld_stack)
                #print("Self :", self)

    def rename():
        """ to rename leafs. renamed leaf will be set to None """
        pass

    def calx(self, o_calx, d_calxargs=None):  # s_outcsv=None
        """ call o_calx is mathematical function with d_calxargs arguments on any leaf """
        # bue: this is hardcore
        # intialize stack tape and branch path tape
        ls_stack = ["#root#"]  # stack tape
        ls_branch = []  # brach path tape
        s_key = None
        d_popu = self
        ld_stack = [d_popu]  # list of dictionaries
        ls_key = list(d_popu.keys())
        if self.b_verbose:
            print("\nInitialization:")
            print("Stack :", ls_stack)
            print("Branch :", ls_branch)
            print("Key :", s_key)
            print("Ls_Key :", ls_key)
            #print("ld_Stack:", ld_stack)

        while (len(ls_stack) > 0):
            # process

            # on leaf
            if (self.s_value in ls_key):  # value
                # fuse calx arguments
                # branch and leaf is always part if the args
                if (d_calxargs == None):
                    d_calxargs = {"d_leaf":d_popu, "ls_branch":ls_branch, "s_value":self.s_value, "b_verbose":self.b_verbose}
                else:
                    d_calxargs.update({"d_leaf":d_popu, "ls_branch":ls_branch, "s_value":self.s_value, "b_verbose":self.b_verbose})
                # get calculation
                o_result = o_calx(d_calxargs=d_calxargs)

                # write to leaf
                if (o_result != None):
                    d_popu.update({ o_calx.__name__ : o_result})
                    self.update_branch(d_leaf=d_popu, ls_branch=ls_branch)  # s_outcsv=s_outcsv

                # handle stack
                if (len(ls_stack) != 1):
                    ls_branch.pop(-1)
                else:
                    # one leaf tree case
                    ls_stack.append("#fork#")
                    ls_branch.append("#root#")

            # on branch
            else:
                # handle d stack
                for s_key in ls_key:
                    ld_stack.append(d_popu[s_key])
                # handle s stack
                ls_key.insert(0, "#fork#")
                ls_stack.extend(ls_key)

            # process branch
            s_key = ls_stack.pop(-1)
            d_popu = ld_stack.pop(-1)
            ls_key = list(d_popu.keys())

            # on fork ?
            while (s_key == "#fork#"):
                s_key = ls_stack.pop(-1)
                # not on root ?
                if (s_key != "#root#"):
                    ls_branch.pop(-1)

            # update branch
            ls_branch.append(s_key)

            # output
            if self.b_verbose:
                print("\nStack :", ls_stack)
                print("Branch :", ls_branch)
                print("Key :", s_key)
                print("Ls_Key :", ls_key)
                #print("Ld_Stack :", ld_stack)
                #print("Self :", self)

    def json_read(self):
        # read file with stanadrd python json libray
        print("\nProcess read_json ...")
        print("Read input file: {}".format(self.s_ifilename))
        with open(self.s_ifilename, newline='') as f_json:
            d_json = json.load(f_json)
            self.update(d_json)
        # output
        f_json.close()
        print("0K")


    def json_write(self):
        # read file with stanadrd python json libray
        print("\nProcess write_json ...")
        print("Write output file: {}".format(self.s_ofilename))
        with open(self.s_ofilename, 'w', newline='') as f_json:
            d_json = json.dump(self, f_json)
        # output
        f_json.close()
        print("0K")


    def pars_ddtree(self):
        """ parses s_ddtree,
            outputs lsi_branch and llssi_leaf

        examples:

        s_ddtree = "{cellline:{run:{well:{(twig488:leaf488),\
        (twig555:leaf555),(twig647:leaf647)}}}}"

        s_ddtree = "{cellline:{run:{well:{(twig:value),\
        (twig:mean),(twig:s3remove)}}}}"

        s_ddtree = "{cellline:{run:{well:{(twig488:leaf488),\
        (twig:mean),(twig:s3remove)}}}}"
        """
        print("Parse ddtree input string: {}".format(self.s_ddtree))
        # empty output
        llsi_branch = [] # [["branch1",1],["branch2",2],["branch",3]]
        llsi_twig = []   # [["twig488",4],["twig555",5],["twig647",6]]  [["twig",10]] [["twig488"],["twig",10]]
        llssi_leaf = []  # [["twig488","leaf488",7],["twig555","leaf555",8],["twig647","leaf647",9]]
                         # [["twig","value",11],["twig","mean",12],["twig","s3remove",13]]
                         # [["twig488","leaf488",4],["twig","mean",12],["twig","s3remove",13]]

        # get branch and leaf split
        self.s_ddtree = self.s_ddtree.replace(" ","")
        self.s_ddtree = self.s_ddtree.replace("}","")
        ls_branch = self.s_ddtree.split(":{")
        s_twigleaf = ls_branch.pop(-1)
        ls_twigleaf = s_twigleaf.split(",")

        # process ls_branch
        #print("Get branch, twig, leaf construct.")
        for s_branch in ls_branch:
            if (s_branch != ""):
                # update llsi branch
                s_branch = s_branch.replace("{","")
                lsi_branch = [s_branch,None]
                llsi_branch.append(lsi_branch)

        # process ls_twigleaf
        for s_twigleaf in ls_twigleaf:
            s_twigleaf = s_twigleaf.replace("(","")
            s_twigleaf = s_twigleaf.replace(")","")
            s_twig,s_leaf = s_twigleaf.split(":")
            # set llsi twig
            lsi_twig = [s_twig,None]
            # update llsi twig
            llsi_twig.append(lsi_twig)
            # update llssi leaf
            lssi_leaf = [s_twig,s_leaf,None]
            llssi_leaf.append(lssi_leaf)

        # output
        tl_blueprint = (llsi_branch, llsi_twig, llssi_leaf)
        if self.b_verbose:
            print("Branch, twig, leaf construct:", tl_blueprint)
        return(tl_blueprint)

    def tsv_read(self):
        """ read tsv formated file input. e.g. scanr dane formated single cell files """
        print("\nProcess read_tsv ...")

        # parse s_ddtree dictionary blueprint
        tl_blueprint = self.pars_ddtree()

        # read file with stanadrd python csv libray
        print("Read input file: {}".format(self.s_ifilename))
        if self.b_verbose:
            ls_monitor = [] # for display only
        with open(self.s_ifilename, newline='') as f_csv:
            f_reader = csv.reader(f_csv, delimiter="\t")
            b_header = True
            for ls_row in f_reader:
                if (re.search("^#", ls_row[0])):
                    # handle comented out rows
                    if self.b_verbose:
                        print("Handle commented out rows.")
                else:
                    if (b_header):
                        # handle header row
                        if self.b_verbose:
                            print("Handle header row.")
                        ls_row = [n.replace(" ","") for n in ls_row]

                        # populate llsi_branch with index integers
                        llsi_branch = tl_blueprint[0]
                        for lsi_branch in llsi_branch:
                            s_index = lsi_branch[0]
                            i_index = ls_row.index(s_index)
                            lsi_branch[-1] = i_index # mutable

                        # populate llsi_twig construct
                        llsi_twig = tl_blueprint[1]
                        for lsi_twig in llsi_twig:
                            s_index = lsi_twig[0]
                            i_index = ls_row.index(s_index)
                            lsi_twig[-1] = i_index # mutable

                        # populate llssi_leaf construct
                        llssi_leaf = tl_blueprint[2]
                        for lssi_leaf in llssi_leaf:
                            s_index = lssi_leaf[1]
                            i_index = ls_row.index(s_index)
                            lssi_leaf[-1] = i_index # mutable

                        # stdout
                        if self.b_verbose:
                            print("\nBranch costruct:", llsi_branch)
                            print("Twig construct:", llsi_twig)
                            print("Leaf construct:", llssi_leaf)

                        # set header flag
                        b_header = False

                    else:
                        # handle data row
                        # read out data row branch
                        ls_branchreal = []
                        for lsi_branch in llsi_branch:
                            i_key = lsi_branch[-1]
                            s_key = ls_row[i_key]
                            ls_branchreal.append(s_key)

                        # read out data row twig leaf
                        ltssso_twigleaf = []
                        for lssi_leaf in llssi_leaf:
                            # twig part
                            s_twigtag = lssi_leaf[0]
                            for lsi_twig in llsi_twig:
                                if (lsi_twig[0] == s_twigtag):
                                    i_twig = lsi_twig[-1]
                                    s_twigreal = ls_row[i_twig]
                                    s_twigreal = s_twigreal.strip()
                                    break
                            # leaf part
                            s_leaftag = lssi_leaf[1]
                            i_leaf = lssi_leaf[2]
                            s_leafreal = ls_row[i_leaf]
                            s_leafreal = s_leafreal.strip()

                            # handel list of values
                            if re.fullmatch("\s*\[.+\]\s*", s_leafreal):
                                o_leafreal = []
                                s_leafreal = s_leafreal.replace("[","")
                                s_leafreal = s_leafreal.replace("]","")
                                ls_leafreal = s_leafreal.split(",")
                                for s_leafreal in ls_leafreal:
                                     o_leafreal.append(retype(s_leafreal))
                            # handle single value
                            else:
                                o_leafreal = retype(s_leafreal)
                            # append twig leaf
                            ltssso_twigleaf.append((s_twigtag, s_twigreal, s_leaftag, o_leafreal))

                        # branch dictionary of dictionary construction
                        if (self.b_development):
                            print("\nBranch twig leaf real:\n",ls_branchreal,"\n", ltssso_twigleaf)

                        # populate branch construct
                        d_popu = self
                        for s_branchreal in ls_branchreal:
                            try:
                                d_subpopu = d_popu[s_branchreal]
                            except KeyError:
                                d_subpopu = {}
                            # next iterstion
                            d_popu = d_subpopu

                        # populate twig leaf construct
                        for tssso_twigleaf in ltssso_twigleaf:
                            # get real data
                            s_twigtag = tssso_twigleaf[0]
                            s_twigreal = tssso_twigleaf[1]
                            s_leaftag = tssso_twigleaf[2]
                            o_leafreal = tssso_twigleaf[3]
                            # populate leaf
                            if (s_twigtag == self.s_twig):
                                # populate non basic value leaf
                                try:
                                    o_leaftree = d_popu[s_twigreal][s_leaftag]
                                    # add value
                                    if (type(o_leaftree) != list):
                                        o_leaftree = [o_leaftree]
                                    if (type(o_leafreal) == list):
                                        o_leaftree.extend(o_leafreal)
                                    else:
                                        o_leaftree.append(o_leafreal)
                                    try:
                                        d_leaf =  d_popu[s_twigreal]
                                        d_leaf.update({s_leaftag : o_leafreal})
                                    except KeyError:
                                        d_leaf = {s_leaftag : o_leafreal}
                                    d_popu.update({s_twigreal : d_leaf})
                                except KeyError:
                                    # first value
                                    print("First non basic twig and leaf value", s_leaftag, o_leafreal)
                                    try:
                                        d_leaf =  d_popu[s_twigreal]
                                        d_leaf.update({s_leaftag : o_leafreal})
                                    except KeyError:
                                        d_leaf = {s_leaftag : o_leafreal}
                                    d_popu.update({s_twigreal : d_leaf})
                            else:
                                # populate basic value leaf
                                try:
                                    o_leaftree = d_popu[s_twigreal][self.s_value]
                                    # add value
                                    if (type(o_leaftree) != list):
                                        o_leaftree = [o_leaftree]
                                    if (type(o_leafreal) == list):
                                        o_leaftree.extend(o_leafreal)
                                    else:
                                        o_leaftree.append(o_leafreal)
                                    try:
                                        d_leaf =  d_popu[s_twigreal]
                                        d_leaf.update({self.s_value : o_leafreal})
                                    except KeyError:
                                        d_leaf = {self.s_value : o_leafreal}
                                    d_popu.update({s_twigreal : d_leaf})
                                except KeyError:
                                    # first value
                                    print("First basic twig and leaf value", self.s_value, o_leafreal)
                                    try:
                                        d_leaf =  d_popu[s_twigreal]
                                        d_leaf.update({self.s_value : o_leafreal})
                                    except KeyError:
                                        d_leaf = {self.s_value : o_leafreal}
                                    d_popu.update({s_twigreal : d_leaf})

                            # update output dictionary
                            self.update_branch(d_leaf=d_popu, ls_branch=ls_branchreal)

                        # for display only
                        if self.b_verbose:
                            if (ls_monitor != ls_branchreal):
                                ls_monitor = ls_branchreal
                                print("Handle ddtree branch: {}".format(ls_branchreal))
            # output
            f_csv.close()
            print("0K")


    def tsv_write(self):
        """ ddtree obj into write tsv """
        print("\nProcess tsv_write ...")

        # get branch labels
        ls_branch = []
        tl_blueprint = self.pars_ddtree()
        llsi_branch = tl_blueprint[0]
        for lsi_branch in llsi_branch:
            s_branch = lsi_branch[0]
            ls_branch.append(s_branch)
        s_branch = "\t".join(ls_branch)

        # get leaf labels
        b_get = True
        d_twigleaf = self
        while b_get:
            ls_key=list(d_twigleaf.keys())
            print("HALLO", ls_key)
            try:
                o_value = d_twigleaf[self.s_value]
                ls_twig = list(d_twigleaf.keys())
                ls_twig.sort()
                s_twig = "\t".join(ls_twig)
                b_get = False
            except KeyError:
                ls_key = list(d_twigleaf.keys())
                d_twigleaf = d_twigleaf[ls_key[0]]

        # get header line
        s_out = s_branch + "\t" + self.s_twig + "\t" + s_twig + "\n"

        # write header line
        f = open(self.s_ofilename, 'w')
        f.write(s_out)
        f.close()

        # write data lines
        d_calxargs = {}
        d_calxargs["s_ofilename"] = self.s_ofilename
        d_calxargs["b_header"] = True
        self.calx(o_calx=tsv_writeline, d_calxargs=d_calxargs)
