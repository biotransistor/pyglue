""""
# file: pyglue.py
# 
# history: 
#     autor: bue
#     date: 2014-01-08
#     license: >= GPL3
#     language: Python 3.3, 3.4
#
# description: 
#     the glue library is interface betwen tab delimited text files  and python 3 standard object 
#     this particular code is an extension interface between standard python dictionary of dictionary object and pandas dataframes
#
"""

# load library 
#import sys  # error exit
import pandas as pd


#############################
# pandas data frame modules #
#############################
def pydd2pydf(dd_matrix):
    """
    pydd2pydf - transform dictionary of dictionary into pandas data frame 
    input: 
        dd_matrix : dictionary of dictionary
    output: 
        df_matrix : pandas data frame
    """ 
    # input
    print("i pydd2pydf dd_matrix :", dd_matrix)
    # set output variable 
    df_matrix = None
    # processing
    df_matrix = pd.DataFrame(dd_matrix)  # bue 20140129: is this dataframe (y axis, x axis) correct alphabetically ordered?
    df_matrix = df_matrix.transpose()
    # output
    print("o pydd2pydf df_matrix :\n", df_matrix)
    return(df_matrix) 


def pydf2pydd(df_matrix): 
    """
    pydf2pydd - transform pandas data frame into dictionary of dictionary
    input: 
        df_matrix : pandas data frame 
    output: 
        dd_matrix : dictionary of dictionary
    """
    # input 
    print("i pydf2pydd df_matrix :\n", df_matrix)
    # set output variable 
    dd_matrix = {} 
    # processing
    # dd_matrix = df_matrix.to_dict() 
    # bue: this builds a python dictionary of dictionary with numpy dtypes, not what I am looking for
    for dtype_y in df_matrix.index:
        #print("dtype_y :", dtype_y)
        for dtype_x in df_matrix.columns:
            #print("dtype_x :", dtype_x)
            # { dtype_y : {dtype_x : dd_matrix[dtype_y][dtype_x]} }
            try:  # check if for this y key a dictionary exist
                d_matrix = dd_matrix[str(dtype_y)]
            except KeyError:  # bad ending 
                d_matrix = {}
            # store 
            # bue 20140214 : code is not yet checking if x key exist, simply overwrites value. 
            # bue: real y and x access is transposed in pandas dataframe
            # bue: ix access like df_matrix.ix[dtype_y,dtype_x] is a workaround
            d_matrix.update({str(dtype_x):str(df_matrix[dtype_x][dtype_y])})  # update row dictionary 
            dd_matrix.update({str(dtype_y):d_matrix})  # update dictionary of row dictionaries
    # output
    print("o pydf2pydd dd_matrix :", dd_matrix)
    return(dd_matrix)



####################
# modues self test #
####################
if __name__=='__main__': 
    # pydd2pydf
    print('\n*** selftest : py dd 2 py df ***')
    dd_matrix = {'yaxis2':{'xaxis2':2.2, 'xaxis1':2.1, 'xaxis3':2.3}, 'yaxis1':{'xaxis1':1.1, 'xaxis2':1.2, 'xaxis3':1.3}, 'yaxis3':{'xaxis1':3.1, 'xaxis2':3.2, 'xaxis3':None}}
    pydd2pydf(dd_matrix)

    # pydf2pydd
    print('\n*** selftest : py df 2 py dd ***')
    df_matrix = pd.DataFrame([[1.1, 1.2, 1.3], [2.1, 2.2, 2.3], [3.1, 3.2, None]], index=('yaxis1','yaxis2','yaxis3',), columns=('xaxis1','xaxis2','xaxis3',))
    pydf2pydd(df_matrix)
