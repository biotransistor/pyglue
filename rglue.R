###
# file: rglue.R 
# 
# history: 
#     function: oligo2txt.f 
#     author: bue 
#     date: 2013-08-09
#     license: GPL >=3
#     language: R 3.0.1; bioconductor 2.12
#     describtion: bioconductor oligo pkg buritto. input: oligonucleotide array raw-data. output: normalized txt-data
#   
###

# norm.v options: mas5, rma
# cdf.v options: HS133A_HS_UG 
# exontarget.v options are = NULL (no exon array), 'probeset', 'core', 'extended', 'full'
oligo2txt.f <- function(prj.v = "experiment",pwd.v = getwd(), exontarget.v = NULL) {
    # load library and work directory path
    require(oligo)  
    pwd00.v <- getwd() 
    setwd(pwd.v)

    # check exontarget.v option 
    if (!((exontarget.v %in% c('probeset','core','extended','full')) || is.null(exontarget.v))) {
        stop("Error: *'", exontarget.v,"'* is not a valid exontarget.v option. Choose from NULL, 'probeset','core', 'extended', and 'full'") 
    }

    # load raw data files
    celfiles.v <- list.celfiles(full.names=TRUE)
    xysfiles.v <- list.xysfiles(full.names=TRUE)
    if ((length(celfiles.v) == 0) & (length(xysfiles.v) == 0)) { 
        stop("Error: No raw celfiles or xysfiles found under path *", pwd.v,"*. Set meaningfull pwd.d parameter!")
    } else { 
        if (length(celfiles.v) > length(xysfiles.v)) { 
            # celfiles
            raw.es <- read.celfiles(celfiles.v)
        } else { 
            # xys files
            raw.es <- read.xysfiles(xysfiles.v)
        }
    }

    # normalization
    if (is.null(exontarget.v)) { 
        norm.es <- rma(raw.es)
    } else {
        norm.es <- rma(raw.es, target=exontarget.v)
    }

    # ctrl plot output 
    cat("output ctrl plots...\t")
    pdf(file= paste(prj.v,"oligo2txt_ctrlplots.pdf", sep = '_'))
    par(mfcol=c(2, 1))
    # raw and norm data ctrl plots
    boxplot(raw.es, main="raw data intensity")
    boxplot(norm.es, transfo=identity, main="rma normalized data intensitya")
    # bue 20130809: par is somehow ignored by the hist plot 
    hist(raw.es,  main="raw data intensity")
    hist(norm.es, transfo=identity, main="rma normalized data intensity")
    dev.off()
    cat("ok\n\n")

    # txt output
    #require(Biobase)
    cat("begin detach normalized eset\n")
    # assaydata
    cat("store assaydata... ")
    assaydata.m <- exprs(norm.es)
    # add rownames
    colname.v <- colnames(assaydata.m) 
    assaydata.m <- cbind(rownames(assaydata.m), assaydata.m)
    colnames(assaydata.m) <- c("label",colname.v)
    # transpose the matrix
    python.m <- t(assaydata.m)
    #python.m <- python.m[,1:5]
    write.table(python.m, file = paste(prj.v,"assaydata.tsv", sep = '_'), quote = FALSE, sep = '\t', row.names = TRUE, col.names = FALSE)
    cat("ok\n")
    # feature
    cat("store featuredata... ")
    feature.df <- fData(norm.es)
    if (dim(feature.df)[2] == 0) {
        # NULL featuredata bridge 
        feature.df <- cbind(feature.df, rownames(feature.df))
        colnames(feature.df) <- c("featurenames")
    }
    # add rownames
    colname.v <- colnames(feature.df)  
    feature.df <- cbind(rownames(feature.df), feature.df) 
    colnames(feature.df) <- c("label",colname.v) 
    write.table(feature.df, file = paste(prj.v,"feature.tsv", sep = '_'), quote = FALSE, sep = '\t', row.names = FALSE, col.names = TRUE)
    cat("ok\n")     

    # pheno
    cat("store phenodata... ")
    pheno.df <- pData(norm.es)
    if (dim(pheno.df)[2] == 0) {
        # NULL phenodata bridge
        pheno.df <- cbind(pheno.df, rownames(pheno.df))
        colnames(pheno.df) <- c("phenonames")
    }
    # add rownames
    colname.v <- colnames(pheno.df)
    pheno.df <- cbind(rownames(pheno.df), pheno.df) 
    colnames(pheno.df) <- c("label",colname.v)
    write.table(pheno.df, file = paste(prj.v,"pheno.tsv", sep = '_'), quote = FALSE, sep = '\t', row.names = FALSE, col.names = TRUE)
    cat("ok\n")
    # the end
    cat("end detach eset\n\n")
    
    # set work directory path back 
    setwd(pwd00.v)

    # function output
    return(norm.es)
} 
