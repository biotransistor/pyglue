"""
# file : gltoboxwhisker.py
# 
# history: 
#     author: bue
#     date: 2014-03-11 
#     license: >= GPL3 
#     language: Python 3.3.3
#     description: box and whisker plots
#
"""
# plots should not ploted direct form rdb txt files but insted from the dictonary objects

# load libraries
import matplotlib.pyplot as plt
from pyanalysis.glueto import pyd7tbseluowr2populate # test case
from pyanalysis.glueto import pyd5tbselur2populate # test case

# modules 
def pyd7tbseluowr2pltbox_cellcount( d7t_bseluowr, ts_cellcount=None, s_outputfile='prj_norm', tf_ylim=None, t_figsize=(8.5,8.5) ): 
    """
    plot one cellcount box and whisker per biology layout drug, one page per endpointset endpoint out of a d7t_bseluowr object 
    input: 
        d7t_bseluowr : python dictionary of dictionary of biology, endpointset, endpoint, layout, drug, dose, well, response tuple ([measure],[rawdatafile])
        ts_cellcount : tuple of strings of cell count endpoints
        s_outputfile : string with outputfile name, without extension
        tf_ylim : set y axis limit. None = auto scaling. (0,1.25)  
        t_figsize : tuple of flots, contains paper y length and x whide in inch 
    output: 
        png file with plot 
    """
    print( "i pyd7tbseluowr2pltbox_cellcount d7t_bseluowr : print supperessed" )
    #print( "i pyd7tbseluowr2pltbox_cellcount :", d7t_bseluowr )
    print( "i pyd7tbseluowr2pltbox_cellcount ts_cellcount :", ts_cellcount )
    print( "i pyd7tbseluowr2pltbox_cellcount s_outputfile :", s_outputfile )
    print( "i pyd7tbseluowr2pltbox_cellcount tf_ylim :", tf_ylim )
    print( "i pyd7tbseluowr2pltbox_cellcount t_figsize :", t_figsize )
    # set color and marker palette 
    # purple, blue, lightblue, cyan, green, lightgreen, yellow, orange, lightred, red, brown, lightgrey, grey, darkgrey, 
    ls_color = ['#6F3D86','#352879','#6C5EB5','#70A4B2','#588D43','#9AD284','#B8C76F','#6F4F25','#9A6759','#68372B','#433900','#959595','#6C6C6C','#444444',] 

    # empty set 
    es_biology = set()
    es_endpointset = set()
    es_endpoint = set()
    es_layout = set()
    es_drug = set()
    es_dose = set()
    es_well = set()
    # populate sets
    for s_biology in d7t_bseluowr.keys():
        es_biology.update( [s_biology,] )  # handle biology
        for s_endpointset in d7t_bseluowr[s_biology].keys():
            es_endpointset.update( [s_endpointset,] )  # handle endpoint
            for s_endpoint in d7t_bseluowr[s_biology][s_endpointset].keys():
                es_endpoint.update( [s_endpoint,] )  # handle endpoint
                for s_layout in d7t_bseluowr[s_biology][s_endpointset][s_endpoint].keys():
                    es_layout.update( [s_layout,] )  # handle layout
                    for s_drug in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                        es_drug.update( [s_drug,] )  # handle drug
                        for s_dose in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys():
                            es_dose.update( [s_dose,] )  # handle dose
                            for s_well in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                es_well.update( [s_well,] )  # handle well
    # order drugs 
    ls_drug = list( es_drug )
    ls_drug.sort()
    ts_drug = tuple( ls_drug )

    # for each biology == plot
    for s_biology in es_biology: 
        # for each endpointset 
        for s_endpointset in es_endpointset: 
            # for each biology endpointset endpoint (cellcount) combination 
            for s_cellcount in ts_cellcount: 
                # for each biology endpointset endpoint layout combination  == plot
                for s_layout in es_layout: 
                    try:  
                        d7t_bseluowr[s_biology][s_endpointset][s_cellcount][s_layout].keys()
                        # for each plot one time 
                        plt.figure( figsize=t_figsize )  # set figure figsize=(11,8.5) figsize=(8.5,11) 
                        ax = plt.subplot( 1,1,1 )  # generate just one figure and one subplot
                        # set label 
                        s_out = s_outputfile+'_'+s_biology+'_'+s_endpointset+'_'+s_cellcount+'_'+s_layout
                        s_title = s_biology+' : '+s_endpointset+' : '+s_cellcount+' : '+s_layout
                        plt.title(s_title)
                        plt.ylabel( 'cell_count\n' )
                        # reset 
                        llf_value = []
                        ls_xlabel = []
                        #i_color = 0  # set first color 
                        # for each biology endpointset endpoint layout drug combination == box
                        for s_drug in ts_drug:
                            lf_value = []
                            # for each biology endpoint layout drug dose combination
                            for s_dose in es_dose: 
                                # for each biology endpoint layout drug dose well combination
                                for s_well in es_well: 
                                    try: 
                                        s_value =  d7t_bseluowr[s_biology][s_endpointset][s_cellcount][s_layout][s_drug][s_dose][s_well][0]
                                        # get value 
                                        if ( s_value != 'None' ):
                                            f_value = float( s_value )
                                        else:
                                            f_value = 0
                                        # put value
                                        lf_value.append( f_value )
                                    except KeyError: 
                                        pass  # nop 
                            # go to chatch them all
                            if ( len(lf_value) > 0 ):   
                                llf_value.append( lf_value )                           
                                ls_xlabel.append( s_drug )
                        # plot 
                        ax.boxplot( llf_value, 0 )
                        if ( tf_ylim != None ): 
                            ax.set_ylim( tf_ylim )
                        # set axes and grid
                        axes = plt.gca()
                        axes.yaxis.grid( True, linestyle='-', which='major', color='lightgrey', alpha=0.5 )
                        axes.set_xticklabels( ls_xlabel, rotation=90, fontsize='x-small' )
                        # output to file 
                        plt.tight_layout() # tight 
                        plt.savefig( s_out+'_pltboxwhisker_cellcount.pdf', format='pdf' )  # file format: png, svg, pdf; bue 2014-01-10: dpi dots per inch can be set!
                        #plt.show()
                    # nop 
                    except KeyError: 
                        pass  
    # return
    print( "o pyd7tbseluowr2pltbox_cellcount : nop" )
    return()                    
 

def pyd7tbseluowr2pltbox_intensity( d7t_bseluowr, s_outputfile='prj_norm', tf_ylim=None, t_figsize=(8.5,8.5) ): 
    """
    plot one endpoint intensity box and whisker per biology layout drug, one page per endpointset endpoint out of a d7t_bseluowr object 
    input: 
        d7t_bseluowr : python dictionary of dictionary of biology, endpointset, endpoint, layout, drug, dose, well, response tuple ([measure],[rawdatafile])
        ts_cellcount : tuple of strings of cell count endpoints
        s_outputfile : string with outputfile name, without extension
        tf_ylim : set y axis limit. None = auto scaling. (-0.6,0.6)  
        t_figsize : tuple of flots, contains paper y length and x whide in inch 
    output: 
        png file with plot 
    """
    print( "i pyd7tbseluowr2pltbox_intensity  d7t_bseluowr : suppressed" )
    #print( "i pyd7tbseluowr2pltbox_intensity  d7t_bseluowr :", d7t_bseluowr )
    print( "i pyd7tbseluowr2pltbox_intensity  s_outputfile :", s_outputfile )
    print( "i pyd7tbseluowr2pltbox_intensity  tf_ylim :", tf_ylim )
    print( "i pyd7tbseluowr2pltbox_intensity  t_figsize :", t_figsize )
 
    # set color and marker palette 
    # purple, blue, lightblue, cyan, green, lightgreen, yellow, orange, lightred, red, brown, lightgrey, grey, darkgrey, 
    ls_color = ['#6F3D86','#352879','#6C5EB5','#70A4B2','#588D43','#9AD284','#B8C76F','#6F4F25','#9A6759','#68372B','#433900','#959595','#6C6C6C','#444444',] 

    # empty set 
    es_biology = set()
    es_endpointset = set()
    es_endpoint = set()
    es_layout = set()
    es_drug = set()
    es_dose = set()
    es_well = set()
    # populate sets
    for s_biology in d7t_bseluowr.keys():
        es_biology.update( [s_biology,] )  # handle biology
        for s_endpointset in d7t_bseluowr[s_biology].keys():
            es_endpointset.update( [s_endpointset,] )  # handle endpoint
            for s_endpoint in d7t_bseluowr[s_biology][s_endpointset].keys():
                es_endpoint.update( [s_endpoint,] )  # handle endpoint
                for s_layout in d7t_bseluowr[s_biology][s_endpointset][s_endpoint].keys():
                    es_layout.update( [s_layout,] )  # handle layout
                    for s_drug in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                        es_drug.update( [s_drug,] )  # handle drug
                        for s_dose in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys():
                            es_dose.update( [s_dose,] )  # handle dose
                            for s_well in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                es_well.update( [s_well,] )  # handle well
    # order biology
    ls_biology = list( es_biology )
    ls_biology.sort()

    # for each endpoint == plot
    for s_endpoint in es_endpoint:
        # for each plot one time 
        plt.figure( figsize=t_figsize )  # set figure figsize=(11,8.5) figsize=(8.5,11) 
        ax = plt.subplot(1,1,1)  # generate just one figure and one subplot
        if ( tf_ylim != None ): 
            ax.set_ylim( tf_ylim )
        # set label 
        s_out = s_outputfile +'_'+s_endpoint
        plt.title( s_endpoint )
        plt.ylabel( 'intensity\n' )
        # reset 
        llf_value = []
        ls_xlabel = []
        #i_color = 0  # set first color 
        # for each endpoint biology combination == box 
        for s_biology in ls_biology:
            lf_value = []
            # for each biology endpointset endpoint combination
            for s_endpointset in es_endpointset:
                # for each biology endpointset endpoint layout combination
                for s_layout in es_layout: 
                    # for each biology endpointset endpoint layout drug combination
                    for s_drug in es_drug:
                        # for each biology endpointset endpoint layout drug dose combination
                        for s_dose in es_dose:
                            # for each biology endpointset endpoint layout drug dose well combination
                            for s_well in es_well:
                                # get value  
                                try: 
                                    s_value = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][0]
                                    if ( s_value != 'None' ): 
                                        f_value = float( s_value )
                                        lf_value.append( f_value )
                                #except ValueError:
                                #    pass # non a number
                                except TypeError: 
                                    pass # a NoneType
                                except KeyError:
                                    pass # not a valid combination 
            # go to chatch them all  
            llf_value.append( lf_value )
            ls_xlabel.append( s_biology )
        # plot 
        ax.boxplot( llf_value,0 )
        # set axes and grid
        axes = plt.gca()
        axes.yaxis.grid( True, linestyle='-', which='major', color='lightgrey', alpha=0.5 )
        axes.set_xticklabels( ls_xlabel, rotation=90, fontsize='small' )
        # output to file 
        # plt.tight_layout() # tight 
        plt.savefig( s_out+'_pltboxwhisker_intensity.pdf', format='pdf' )  # file format: png, svg, pdf; bue 2014-01-10: dpi dots per inch can be set!
        #plt.show()
    # return
    print( "o pyd7tbseluowr2pltbox_intensity  : nop" )
    return()                    
 

def pyd5tbselur2pltbox_metric( d5t_bselur, s_metric='metric', s_outputfile='prj_norm', tf_ylim=None, t_figsize=(8.5,8.5) ):
    """
    plot one metric box and whisker contains all bioogy per endpointset endpoint, one page per layout drug of a d5t_bselur metric object 
    input: 
        d5t_bselur : metric related python dictionary of dictionary of biology, endpointset, endpoint, layout, drug, response tuple ('measure','...' )
        ts_cellcount : tuple of strings of cell count endpoints
        s_metric : string describeing metric (e.g. slope, auc) 
        s_outputfile : string with outputfile name, without specified metric, without extension
        tf_ylim : set y axis limit. None = auto scaling  
        t_figsize : tuple of flots, contains paper y length and x whide in inch 
    output: 
        png file with plot 
    """
    print( "i pyd5tbselur2pltbox_metric d5t_bselur : suppressed" )
    #print( "i pyd5tbselur2pltbox_metric d5t_bselur :", d5t_bselur )
    print( "i pyd5tbselur2pltbox_metric s_outputfile :", s_outputfile )
    print( "i pyd5tbselur2pltbox_metric tf_ylim :", tf_ylim )
    print( "i pyd5tbselur2pltbox_metric t_figsize :", t_figsize )
 
    # set color and marker palette 
    # purple, blue, lightblue, cyan, green, lightgreen, yellow, orange, lightred, red, brown, lightgrey, grey, darkgrey, 
    ls_color = ['#6F3D86','#352879','#6C5EB5','#70A4B2','#588D43','#9AD284','#B8C76F','#6F4F25','#9A6759','#68372B','#433900','#959595','#6C6C6C','#444444',] 

    # empty set 
    es_biology = set()
    es_endpointset = set()
    es_endpoint = set()
    es_layout = set()
    es_drug = set()
    es_dose = set()
    es_well = set()
    # populate sets
    for s_biology in d5t_bselur.keys():
        es_biology.update( [s_biology,] )  # handle biology
        for s_endpointset in d5t_bselur[s_biology].keys():
            es_endpointset.update( [s_endpointset,] )  # handle endpoint
            for s_endpoint in d5t_bselur[s_biology][s_endpointset].keys():
                es_endpoint.update( [s_endpoint,] )  # handle endpoint
                for s_layout in d5t_bselur[s_biology][s_endpointset][s_endpoint].keys():
                    es_layout.update( [s_layout,] )  # handle layout
                    for s_drug in d5t_bselur[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                        es_drug.update( [s_drug,] )  # handle drug
    # order endpointsets and endpoint
    ls_endpointset = list( es_endpointset )
    ls_endpointset.sort()
    ls_endpoint = list( es_endpoint )
    ls_endpoint.sort()
   
    # for each layout     
    for s_layout in es_layout : 
        # for each drug == plot
        for s_drug in es_drug :
            # for each plot one time 
            plt.figure( figsize=t_figsize )  # set figure figsize=(11,8.5) figsize=(8.5,11) 
            ax = plt.subplot( 1,1,1 )  # generate just one figure and one subplot
            if ( tf_ylim != None ): 
                ax.set_ylim( tf_ylim )
            # set label 
            s_out = s_outputfile+'_'+s_drug
            plt.title( s_drug )
            plt.ylabel( s_metric+'\n' ) 
            # reset 
            llf_value = []
            ls_xlabel = []
            #i_color = 0  # set first color 
            # for each endpointset 
            for s_endpointset in ls_endpointset :
                # for each endpoint == box  
                for s_endpoint in ls_endpoint :
                    lf_value = []
                    # for each biology 
                    for s_biology in es_biology :
                        # get valu for each layout, drug, endpointset, endpoint, biology combination  
                        try : 
                            s_value = d5t_bselur[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][0]
                            if (s_value != 'None'): 
                                f_value = float( s_value )
                                lf_value.append( f_value )
                        except KeyError : 
                            pass # not ever each layout, drug, endpointset, endpoint, biology combination will exist ! 
                    # go to chatch them all 
                    if ( len(lf_value) > 0 ): 
                        llf_value.append( lf_value )
                        ls_xlabel.append( s_endpoint )
            # plot 
            if ( len(llf_value) != 0 ): 
                ax.boxplot( llf_value,0 )
                # set axes and grid
                axes = plt.gca()
                axes.yaxis.grid( True, linestyle='-', which='major', color='lightgrey', alpha=0.5 )
                axes.set_xticklabels( ls_xlabel, rotation=90, fontsize='small' )
                # output to file 
                # plt.tight_layout() # tight 
                plt.savefig( s_out+'_pltboxwhisker_'+s_metric+'.pdf', format='pdf' )  # file format: png, svg, pdf; bue 2014-01-10: dpi dots per inch can be set!
                #plt.show()
    # return
    print( "o pyd5tbselur2pltbox_metric : nop" )
    return()                    
 


# module self test  
if __name__=='__main__':
    # bseluowr test obj
    d7t_test = {} 
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', s_dose='1', s_well='well1', t_response=(1,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', s_dose='2', s_well='well2', t_response=(2,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well3', t_response=(2,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='endpoint2', s_layout='layout1', s_drug='drug1', s_dose='1', s_well='well4', t_response=(1,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='endpoint2', s_layout='layout1', s_drug='drug1', s_dose='2', s_well='well5', t_response=(3,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='endpoint2', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well6', t_response=(3,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='cellcount1', s_layout='layout1', s_drug='drug1', s_dose='1', s_well='well7', t_response=(2,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='cellcount1', s_layout='layout1', s_drug='drug1', s_dose='2', s_well='well8', t_response=(1,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='cellcount1', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well9', t_response=(1,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='cellcount2', s_layout='layout1', s_drug='drug1', s_dose='1', s_well='well10', t_response=(3,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='cellcount2', s_layout='layout1', s_drug='drug1', s_dose='2', s_well='well11', t_response=(2,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='cellcount2', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well12', t_response=(1,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='cellcount2', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well13', t_response=(1.1,'rawabc',), d7t_bseluowr=d7t_test )
    d7t_test = pyd7tbseluowr2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='cellcount2', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well14', t_response=(1.4,'rawabc',), d7t_bseluowr=d7t_test )

    # bselur test obj
    d5t_test = {}
    d5t_test = pyd5tbselur2populate( s_biology='bio1', s_endpointset='endset2', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', t_response=(1.0,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio2', s_endpointset='endset2', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', t_response=(1.1,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio3', s_endpointset='endset2', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', t_response=(1.3,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio4', s_endpointset='endset2', s_endpoint='endpoint2', s_layout='layout1', s_drug='drug1', t_response=(2.0,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio5', s_endpointset='endset2', s_endpoint='endpoint2', s_layout='layout1', s_drug='drug1', t_response=(2.2,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio6', s_endpointset='endset2', s_endpoint='endpoint2', s_layout='layout1', s_drug='drug1', t_response=(2.4,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio1', s_endpointset='endset1', s_endpoint='endpoint3', s_layout='layout1', s_drug='drug1', t_response=(3.0,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio2', s_endpointset='endset1', s_endpoint='endpoint3', s_layout='layout1', s_drug='drug1', t_response=(3.3,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio3', s_endpointset='endset1', s_endpoint='endpoint3', s_layout='layout1', s_drug='drug1', t_response=(3.6,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio4', s_endpointset='endset1', s_endpoint='endpoint4', s_layout='layout1', s_drug='drug1', t_response=(1.4,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio5', s_endpointset='endset1', s_endpoint='endpoint4', s_layout='layout1', s_drug='drug1', t_response=(1.5,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio6', s_endpointset='endset1', s_endpoint='endpoint4', s_layout='layout1', s_drug='drug1', t_response=(1.6,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio1', s_endpointset='endset3', s_endpoint='endpoint6', s_layout='layout1', s_drug='drug1', t_response=(2.4,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio2', s_endpointset='endset3', s_endpoint='endpoint6', s_layout='layout1', s_drug='drug1', t_response=(2.5,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio3', s_endpointset='endset3', s_endpoint='endpoint6', s_layout='layout1', s_drug='drug1', t_response=(2.6,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio4', s_endpointset='endset3', s_endpoint='endpoint5', s_layout='layout1', s_drug='drug1', t_response=(3.4,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio5', s_endpointset='endset3', s_endpoint='endpoint5', s_layout='layout1', s_drug='drug1', t_response=(3.5,'rawabc',), d5t_bselur=d5t_test )
    d5t_test = pyd5tbselur2populate( s_biology='bio6', s_endpointset='endset3', s_endpoint='endpoint5', s_layout='layout1', s_drug='drug1', t_response=(3.6,'rawabc',), d5t_bselur=d5t_test )


    # test run 
    print('\n*** selftest py d7t bseluowr 2 plt box cell count plot ***')
    pyd7tbseluowr2pltbox_cellcount( d7t_bseluowr=d7t_test, ts_cellcount=('cellcount1','cellcount2',), s_outputfile='selftest', tf_ylim=None, t_figsize=(8.5,8.5) )
 
    print('\n*** selftest py d7t bseluowr 2 plt box intensity plot ***')
    pyd7tbseluowr2pltbox_intensity( d7t_bseluowr=d7t_test, s_outputfile='selftest', tf_ylim=None, t_figsize=(8.5,8.5) )
 
    print('\n*** selftest py d7t bselur 2 plt box metric plot ***')
    pyd5tbselur2pltbox_metric( d5t_bselur=d5t_test, s_metric='metric', s_outputfile='selftest', tf_ylim=None, t_figsize=(8.5,8.5) )
