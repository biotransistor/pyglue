"""
# file : gltowell.py
# 
# history: 
#     author: bue
#     date: 2014-02-18, 2014-08-11, 2014-10-22
#     license: >= GPL3 
#     language: Python 3.4
#     description: sceening data analysis on the well level 
#
#     biology:endpoint:treatment > biology:endpointset:endpoint:layout:drug:dose:well:response > bseluowr
"""

# load libraries
import sys # error jump out
import copy # deep copy objects 
import csv # read and write files
import math # math   
import statistics # statistics python 3.4
import pyglue.glue as gl # 2txt and selftest
import numpy as np # slope and auc calucation 
from scipy import stats # slope calculation 
from Bio.Statistics.lowess import lowess # biopython stats ( bue 2014-10-24: implementation uses a lot ram,  evt use lowess implementation from python statsmodels library ) 


### populate modules ###
## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd7tbseluowr2populate( s_biology, s_endpointset, s_endpoint, s_layout, s_drug, s_dose, s_well, t_response, d7t_bseluowr={} ):
    """
    pyd7tbseluowr2populate - populate biology dictionary of endpointset dictionary of endpoint dictionary of layout dictionary of drug dictionary of dose dictionary of well dictionary of response tuple 
    input:
        s_biology : string of biology
        s_endpointset : string of endpointset
        s_endpoint : string of endpoint
        s_layout : string of plate layout 
        s_drug : string of drug
        s_dose : string of dose
        s_well : string of well
        t_response : tuple of response (f_measure, s_rawdatafile,)
        d7t_bseluowr : bseluowr dictionary to be populated
    output:  
        d7t_bseluowr : bseluowr dictionary updated
    """
    # input 
    print( "i pyd7tbseluowr2populate s_biology :", s_biology )
    print( "i pyd7tbseluowr2populate s_endpointset :", s_endpointset )
    print( "i pyd7tbseluowr2populate s_endpoint :", s_endpoint )
    print( "i pyd7tbseluowr2populate s_layout :", s_layout )
    print( "i pyd7tbseluowr2populate s_drug :", s_drug )
    print( "i pyd7tbseluowr2populate s_dose :", s_dose )
    print( "i pyd7tbseluowr2populate s_well :", s_well )
    print( "i pyd7tbseluowr2populate t_response :", t_response )
    print( "i pyd7tbseluowr2populate d7t_bseluowr : print suppressed", ) 
    #print("i pyd7tbseluowr2populate d7t_bseluowr :", d7t_bseluowr) 
    ## check if for this key entry alredy exist ##
    try: 
        catch = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well]
        sys.exit( "Error: entry alredy exist for " + s_biology + "_" + s_endpointset + "_" + s_endpoint + "_" + s_layout + "_" + s_drug + "_" + s_dose + "_" + s_well +"." )

    except KeyError: 
        ## get dictionary number 1 well ##
        try:  # good ending
            d_well = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose]
        except KeyError:  # bad ending
            d_well = {}
        # update dictionary nr 1 dose with the response tupe
        d_well.update( {s_well : t_response} )    
        #print( "d_well :", d_well )

        ## get dictionary nr 2 dose ##
        try:  
            d_dose = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug]
        except KeyError:
            d_dose = {}
        # update dictionary nr 2 dose with dictionary nr 1 well
        d_dose.update( {s_dose : d_well} )    
        #print( "d_dose :", d_dose )

        ## get dictionary nr 3 drug ##
        try:
            d_drug = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout]
        except KeyError:
            d_drug = {}
        # update dictionary nr 3 drug with dictionary nr 2 dose 
        d_drug.update( {s_drug : d_dose} )    
        #print( "d_drug :", d_drug )

        ## get dictionary nr 4 layout ##
        try: 
            d_layout = d7t_bseluowr[s_biology][s_endpointset][s_endpoint]
        except KeyError: 
            d_layout = {}
        # update dictionary nr 4 layout with dictionary nr 3 drug
        d_layout.update( {s_layout : d_drug} )    
        #print( "d_layout :", d_layou t)

        ## get dictionary 5 endpoint ##
        try:
            d_endpoint = d7t_bseluowr[s_biology][s_endpointset]
        except KeyError:
            d_endpoint = {}
        # update dictionary nr 5 endpoint with dictionary nr 4 layout
        d_endpoint.update( {s_endpoint : d_layout} )     
        #print( "d_endpoint :", d_endpoint )

        ## get dictionary 6 endpointset ##
        try:
            d_endpointset = d7t_bseluowr[s_biology]
        except KeyError:
            d_endpointset = {}
        # update dictionary nr 6 endpointset with dictionary nr 5 endpoint
        d_endpointset.update( {s_endpointset : d_endpoint} )    
        #print( "d_endpointset :", d_endpointset )

        ## update main dictionary 7 biology with dictionary nr 6 endpointset ##
        d7t_bseluowr.update( {s_biology : d_endpointset} )

    # output
    print( "o pyd7tbseluowr2populate d7t_bseluowr : print suppressed" )
    #print( "o pyd7tbseluowr2populate d7t_bseluowr :", d7t_bseluowr )
    return( d7t_bseluowr )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd6tbselowr2populate( s_biology, s_endpointset, s_endpoint, s_layout, s_dose, s_well, t_response, b_overwrite=False, d6t_bselowr={} ):
    """
    pyd6tbselowr2populate - populate biology dictionary of endpointset dictionary of endpoint dictionary of layout dictionary of dose dictionary of well dictionary of response tuple 
    input:
        s_biology : string of biology
        s_endpointset : string of endpointset
        s_endpoint : string of endpoint
        s_layout : string of plate layout 
        s_dose : string of dose
        s_well : string of well
        t_response : tuple of response (f_measure, s_rawdatafile,)
        d6t_bselowr : bselowr dictionary to be populated
    output:  
        d6t_bselowr : bselowr dictionary updated
    """
    # input 
    print( "i pyd6tbselowr2populate s_biology :", s_biology )
    print( "i pyd6tbselowr2populate s_endpointset :", s_endpointset )
    print( "i pyd6tbselowr2populate s_endpoint :", s_endpoint )
    print( "i pyd6tbselowr2populate s_layout :", s_layout )
    print( "i pyd6tbselowr2populate s_dose :", s_dose )
    print( "i pyd6tbselowr2populate s_well :", s_well )
    print( "i pyd6tbselowr2populate t_response :", t_response )
    print( "i pyd6tbselowr2populate b_overwrite :", b_overwrite )
    print( "i pyd6tbselowr2populate d6t_bselowr : print suppressed", ) 
    #print( "i pyd6tbselowr2populate d6t_bselowr :", d6t_bselowr ) 
    ## set overwrite status ##
    if ( b_overwrite ):
        # if overwrite true
        b_run = True
    else: 
        # if overwrite false check if for this key entry alredy exist
        try: 
            catch = d6t_bselowr[s_biology][s_endpointset][s_endpoint][s_layout][s_dose][s_well]
            sys.exit( "Error: entry alredy exist for " + s_biology + "_" + s_endpointset + "_" + s_endpoint + "_" + s_layout + "_" + s_dose + "_" + s_well +"." )
        except KeyError:
            b_run = True
 
    ## dictionary population ##
    if ( b_run ):
        ## get dictionary number 1 well ##
        try:  # good ending
            d_well = d6t_bselowr[s_biology][s_endpointset][s_endpoint][s_layout][s_dose]
        except KeyError:  # bad ending
            d_well = {}
        # update dictionary nr 1 dose with the response tupe
        d_well.update( {s_well : t_response} )    
        #print( "d_well :", d_well )

        ## get dictionary nr 2 dose ##
        try:  
            d_dose = d6t_bselowr[s_biology][s_endpointset][s_endpoint][s_layout]
        except KeyError:
            d_dose = {}
        # update dictionary nr 2 dose with dictionary nr 1 well
        d_dose.update( {s_dose : d_well} )    
        #print( "d_dose :", d_dose )

        ## get dictionary nr 4 layout ##
        try: 
            d_layout = d6t_bselowr[s_biology][s_endpointset][s_endpoint]
        except KeyError: 
            d_layout = {}
        # update dictionary nr 4 layout with dictionary nr 2 dose
        d_layout.update( {s_layout : d_dose} )    
        #print( "d_layout :", d_layout )

        ## get dictionary 5 endpoint ##
        try:
            d_endpoint = d6t_bselowr[s_biology][s_endpointset]
        except KeyError:
            d_endpoint = {}
        # update dictionary nr 5 endpoint with dictionary nr 4 layout
        d_endpoint.update( {s_endpoint : d_layout} )    
        #print( "d_endpoint :", d_endpoint )

        ## get dictionary 6 endpointset ##
        try:
            d_endpointset = d6t_bselowr[s_biology]
        except KeyError:
            d_endpointset = {}
        # update dictionary nr 6 endpointset with dictionary nr 5 endpoint
        d_endpointset.update( {s_endpointset : d_endpoint} )    
        #print( "d_endpointset :", d_endpointset )

        ## update main dictionary 7 biology with dictionary nr 6 endpointset ##
        d6t_bselowr.update( {s_biology : d_endpointset} )

    # output
    print( "o pyd6tbselowr2populate d6t_bselowr : print suppressed" )
    #print( "o pyd6tbselowr2populate d6t_bselowr :", d6t_bselowr )
    return( d6t_bselowr )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd6tbseluor2populate( s_biology, s_endpointset, s_endpoint, s_layout, s_drug, s_dose, t_response, d6t_bseluor={}):
    """
    pyd6tbseluor2populate - populate biology dictionary of endpointset dictionary of endpoint dictionary of layout dictionary of drug dictionary of dose dictionary of response tuple 
    input:
        s_biology : string of biology
        s_endpointset : string of endpointset
        s_endpoint : string of endpoint
        s_layout : string of plate layout 
        s_drug : string of drug
        s_dose : string of dose
        t_response : tuple of response (f_measure, s_rawdatafile,)
        d6t_bseluor : bseluor dictionary to be populated
    output:  
        d6t_bseluor : bseluor dictionary updated
    """
    # input 
    print( "i pyd6tbseluor2populate s_biology :", s_biology )
    print( "i pyd6tbseluor2populate s_endpointset :", s_endpointset )
    print( "i pyd6tbseluor2populate s_endpoint :", s_endpoint )
    print( "i pyd6tbseluor2populate s_layout :", s_layout )
    print( "i pyd6tbseluor2populate s_drug :", s_drug )
    print( "i pyd6tbseluor2populate s_dose :", s_dose )
    print( "i pyd6tbseluor2populate t_response :", t_response )
    print( "i pyd6tbseluor2populate d6t_bseluor : print suppressed" ) 
    #print( "i pyd6tbseluor2populate d6t_bseluor :", d6t_bseluor ) 
    ## check if for this key entry alredy exist ##
    try: 
        catch = d6t_bseluor[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose]
        sys.exit( "Error: entry alredy exist for " + s_biology + "_" + s_endpointset + "_" + s_endpoint + "_" + s_layout + "_" + s_drug + "_" + s_dose + "." )

    except KeyError: 
        ## get dictionary nr 2 dose ##
        try:  
            d_dose = d6t_bseluor[s_biology][s_endpointset][s_endpoint][s_layout][s_drug]
        except KeyError:
            d_dose = {}
        # update dictionary nr 2 dose with dictionary nr 1 well
        d_dose.update( {s_dose : t_response} )    
        #print( "d_dose :", d_dose )

        ## get dictionary nr 3 drug ##
        try:
            d_drug = d6t_bseluor[s_biology][s_endpointset][s_endpoint][s_layout]
        except KeyError:
            d_drug = {}
        # update dictionary nr 3 drug with dictionary nr 2 dose 
        d_drug.update( {s_drug : d_dose} )    
        #print( "d_drug :", d_drug )

        ## get dictionary nr 4 layout ##
        try: 
            d_layout = d6t_bseluor[s_biology][s_endpointset][s_endpoint]
        except KeyError: 
            d_layout = {}
        # update dictionary nr 4 layout with dictionary nr 3 drug
        d_layout.update( {s_layout : d_drug} )    
        #print( "d_layout :", d_layout )

        ## get dictionary 5 endpoint ##
        try:
            d_endpoint = d6t_bseluor[s_biology][s_endpointset]
        except KeyError:
            d_endpoint = {}
        # update dictionary nr 5 endpoint with dictionary nr 4 layout
        d_endpoint.update( {s_endpoint : d_layout} )    
        #print( "d_endpoint :", d_endpoint )

        ## get dictionary 6 endpointset ##
        try:
            d_endpointset = d6t_bseluor[s_biology]
        except KeyError:
            d_endpointset = {}
        # update dictionary nr 6 endpointset with dictionary nr 5 endpoint
        d_endpointset.update( {s_endpointset : d_endpoint} )    
        #print( "d_endpointset :", d_endpointset )

        ## update main dictionary 7 biology with dictionary nr 6 endpointset ##
        d6t_bseluor.update( {s_biology : d_endpointset} )

    # output
    print( "o pyd6tbseluor2populate d6t_bseluor : print suppressed" )
    #print( "o pyd6tbseluor2populate d6t_bseluor :", d6t_bseluor )
    return( d6t_bseluor )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd5tbselur2populate( s_biology, s_endpointset, s_endpoint, s_layout, s_drug, t_response, d5t_bselur={} ):
    """
    pyd5tbselur2populate - populate biology dictionary of endpointset dictionary of endpoint dictionary of layout dictionary of drug dictionary of response tuple 
    input:
        s_biology : string of biology
        s_endpointset : string of endpointset
        s_endpoint : string of endpoint
        s_layout : string of plate layout 
        s_drug : string of drug
        t_response : tuple of response 
        d5t_bselur : bselur dictionary to be populated
    output:  
        d5t_bselur : bselur dictionary updated
    """
    # input 
    print( "i pyd5tbselur2populate s_biology :", s_biology )
    print( "i pyd5tbselur2populate s_endpointset :", s_endpointset )
    print( "i pyd5tbselur2populate s_endpoint :", s_endpoint )
    print( "i pyd5tbselur2populate s_layout :", s_layout )
    print( "i pyd5tbselur2populate s_drug :", s_drug )
    print( "i pyd5tbselur2populate t_response :", t_response )
    print( "i pyd5tbselur2populate d5t_bselur : print suppressed" ) 
    #print( "i pyd5tbselur2populate d5t_bselur :", d5t_bselur ) 

    ## check if for this key entry alredy exist ##
    try: 
        catch = d5t_bselur[s_biology][s_endpointset][s_endpoint][s_layout][s_drug]
        sys.exit( "Error: entry alredy exist for " + s_biology + "_" + s_endpointset + "_" + s_endpoint + "_" + s_layout + "_" + s_drug + "." )

    except KeyError: 
        ## get dictionary nr 3 drug ##
        try: # good ending
            d_drug = d5t_bselur[s_biology][s_endpointset][s_endpoint][s_layout]
        except KeyError:
            d_drug = {} # bad ending
        # update dictionary nr 3 drug with dictionary nr 2 dose 
        d_drug.update( {s_drug : t_response} )    
        #print( "d_drug :", d_drug )

        ## get dictionary nr 4 layout ##
        try: 
            d_layout = d5t_bselur[s_biology][s_endpointset][s_endpoint]
        except KeyError: 
            d_layout = {}
        # update dictionary nr 4 layout with dictionary nr 3 drug
        d_layout.update( {s_layout : d_drug} )    
        #print( "d_layout :", d_layout )

        ## get dictionary 5 endpoint ##
        try:
            d_endpoint = d5t_bselur[s_biology][s_endpointset]
        except KeyError:
            d_endpoint = {}
        # update dictionary nr 5 endpoint with dictionary nr 4 layout
        d_endpoint.update( {s_endpoint : d_layout} )    
        #print( "d_endpoint :", d_endpoint )

        ## get dictionary 6 endpointset ##
        try:
            d_endpointset = d5t_bselur[s_biology]
        except KeyError:
            d_endpointset = {}
        # update dictionary nr 6 endpointset with dictionary nr 5 endpoint
        d_endpointset.update( {s_endpointset : d_endpoint} )    
        #print( "d_endpointset :", d_endpointset )

        ## update main dictionary 7 biology with dictionary nr 6 endpointset ##
        d5t_bselur.update( {s_biology : d_endpointset} )

    # output
    print( "o pyd5tbselur2populate d5t_bselur : print suppressed" )
    #print( "o pyd5tbselur2populate d5t_bselur :", d5t_bselur )
    return( d5t_bselur )


### extend module ###
## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd7tbseluowr2extend( d7t_bseluowr, d7t_bseluowrextend ):
    """
    pyd7tbseluowr2extend - populate d7t_bseluowr with the content of d7t_bseluowrextend 
    input:
        d7t_bseluowr : bseluowr dictionary 
        d7t_bseluowrextend : bseluowr dictionary to extend
    output: 
        d7t_bseluowrextended : extended bseluowr dictionary
    """
    # input 
    print( "i pyd7tbseluowr2extend d7t_bseluowr : print suppressed" )
    #print( "i pyd7tbseluowr2extend d7t_bseluowr :", d7t_bseluowr )
    print( "i pyd7tbseluowr2extend d7t_bseluowrextend : print suppressed" )
    #print( "i pyd7tbseluowr2extend d7t_bseluowrextend :", d7t_bseluowrextend )
    # empty output
    d7t_bseluowrextended = copy.deepcopy( d7t_bseluowr )
    # processing
    ts_biology = d7t_bseluowrextend.keys()  # get biology keys
    for s_biology in ts_biology : 
        ts_endpointset = d7t_bseluowrextend[s_biology].keys()  # get endpointset keys 
        for s_endpointset in ts_endpointset:  
            ts_endpoint = d7t_bseluowrextend[s_biology][s_endpointset].keys()  # get endpoint keys 
            for s_endpoint in ts_endpoint:  
                ts_layout = d7t_bseluowrextend[s_biology][s_endpointset][s_endpoint].keys()  # get layout keys
                for s_layout in ts_layout:  
                    ts_drug = d7t_bseluowrextend[s_biology][s_endpointset][s_endpoint][s_layout].keys() # get drug keys
                    for s_drug in ts_drug:
                        ts_dose = d7t_bseluowrextend[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys() # get dose keys
                        for s_dose in ts_dose:
                            ts_well = d7t_bseluowrextend[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys()  # get well keys
                            for s_well in ts_well: 
                                t_response = d7t_bseluowrextend[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well]
                                d7t_bseluowrextended = pyd7tbseluowr2populate( s_biology=s_biology, s_endpointset=s_endpointset, s_endpoint=s_endpoint, s_layout=s_layout, s_drug=s_drug, s_dose=s_dose, s_well=s_well, t_response=t_response, d7t_bseluowr=d7t_bseluowrextended )
    # output 
    print( "o pyd7tbseluowr2extend d7t_bseluowrextended : print suppressed" )
    #print( "o pyd7tbseluowr2extend d7t_bseluowrextended :", d7t_bseluowrextended )
    return( d7t_bseluowrextended )


### py obj 2 py object modules ###
## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
## mean median avergae 2 pyd6tbseluor 
def pyd7tbseluowr2pyd6tbseluor( d7t_bseluowr, d6t_bseluor={}, method=statistics.mean ): 
    """
    pyd7tbseluowr2pyd6tbseluor - condens well with same biology, endpointset, endpoint, layout, drug, dose by the specified method to one value
    input : 
        pyd7t_bseluowr : dictionary of biology of endpointset of endpoint of layout of drug of dose of well of response
        pyd6tbseluor : same method condensed dictionary of biology of endpointset of endpoint of layout of drug of dose
        method : average method (e.g. statistics.mean, statistics.median)
    output : 
        pyd6tbseluor : method condensed dictionary of biology of endpointset of endpoint of layout of drug of dose
    """
    print( "i pyd7tbseluowr2pyd6tbseluor d7t_bseluowr : print suppressed" )
    #print( "i pyd7tbseluowr2pyd6tbseluor d7t_bseluowr :", d7t_bseluowr )
    print( "i pyd7tbseluowr2pyd6tbseluor d6t_bseluor : print suppressed" )
    #print( "i pyd7tbseluowr2pyd6tbseluor d6t_bseluor :", d6t_bseluor )
    print( "i pyd7tbseluowr2pyd6tbseluor method :", method )
    # read out d6tbseluowr dictionary
    for s_biology in d7t_bseluowr.keys(): 
        for s_endpointset in d7t_bseluowr[s_biology].keys():
            for s_endpoint in d7t_bseluowr[s_biology][s_endpointset].keys():
                for s_layout in d7t_bseluowr[s_biology][s_endpointset][s_endpoint].keys():
                    for s_drug in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                        for s_dose in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys():
                            lf_measure = []
                            s_rawdatafile = None  # for t_response annotation
                            s_wells = None  # for t_response annotation
                            for s_well in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                # handle wells
                                if s_wells ==  None:
                                    s_wells = s_well
                                else:  
                                    s_wells = s_wells + '_' + s_well
                                # read out response measure                            
                                t_response = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well]
                                try :
                                    f_measure = float( t_response[0] )
                                    lf_measure.append( f_measure )
                                except ValueError:
                                    pass
                                # read out response rawdatafile annotation
                                if ( s_rawdatafile is None ):
                                    s_rawdatafile = t_response[1]
                                else: 
                                    if ( s_rawdatafile != t_response[1] ):
                                        sys.exit( "Error : " + s_biology + "_" + s_endpointset + "_" + s_endpoint + "_" + s_layout + "_" + s_drug + "_" + s_dose + " replicate well " + s_well + " shows inconsistent  annotation for the rawdata file" + s_rawdatafile )
                            # method average calculation
                            try : 
                                f_average = method( lf_measure )
                            except: 
                                f_average = 'None'
                            # populate pyd6tbseluor response 
                            t_resp = ( f_average, s_rawdatafile, s_wells, str(method) )
                            # populate pyd6tbseluor dictionary
                            d6t_bseluor = pyd6tbseluor2populate( s_biology=s_biology, s_endpointset=s_endpointset, s_endpoint=s_endpoint, s_layout=s_layout, s_drug=s_drug, s_dose=s_dose, t_response=t_resp, d6t_bseluor=d6t_bseluor )
    # output
    print( "o pyd7tbseluowr2pyd6tbseluor d6t_bseluor : print suppressed" )
    #print( "o pyd7tbseluowr2pyd6tbseluor d6t_bseluor :", d6t_bseluor )
    return( d6t_bseluor )


## sigma condens 2 pyd5tbselur 
## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
### 2 sigma
def pyd7tbseluowr2pyd7tbseluowrrrr( d7t_bseluowr_raw, d7t_bseluowr_refnorm, ts_cellcount=('cellcount',), d7t_bseluowrrrr={} ): 
    """
    pyd7tbseluowr2pyd7tbseluowrrrr - code to condense refnorm response values, refnorm cellcount values, and real cellcountvalues into one bseluowrrrr dictionary of dictionary obj
    input: 
        d7t_bseluowr_raw : dictionary of dictionary of biology, endpointset, endpoint, layout, drug, dose, well, raw data response object
        d7t_bseluowr_refnorm : dictionary of dictionary biology, endpointset, endpoint, layout, drug, dose, well, reference normalized data response object
        ts_cellcount : tuple of string of cell_count endpoint labels
        d7t_bseluowrrrr : dictionary of dictionary of biology, endpointset, endpoint, layout, drug, dose, well, refnorm cellcount, raw cellcount, refnorm intensity, raw intensity data object
    output:
        d7t_bseluowrrrr : uodated dictionary of dictionary of biology, endpointset, endpoint, layout, drug, dose, well, refnorm cellcount, raw cellcount, refnorm intensity, raw intensity data object
    note: 
    rrrr = refnorm cellcount value, raw cellcount value, refnorm intensity value, raw intensity value,
    bseluowr = biology, endpointset, endpoint, layout, drug, dose, well, response 
    bseluowrrrr = biology, endpointset, endpoint, layout, drug, dose, well, refnorm cellcount response, raw cellcount response, refnorm intensity response, raw intensity response
    """
    print( "i pyd7tbseluowr2pyd7tbseluowrrrr d7t_bseluowr_raw : print suppresses" )
    #print( "i pyd7tbseluowr2pyd7tbseluowrrrr d7t_bseluowr_raw :",  d7t_bseluowr_raw )
    print( "i pyd7tbseluowr2pyd7tbseluowrrrr d7t_bseluowr_refnorm : print suppressed" )
    #print( "i pyd7tbseluowr2pyd7tbseluowrrrr d7t_bseluowr_refnorm : ", d7t_bseluowr_refnorm )
    print( "i pyd7tbseluowr2pyd7tbseluowrrrr ts_cellcount ", ts_cellcount )
    print( "i pyd7tbseluowr2pyd7tbseluowrrrr d7t_bseluowrrrr : print suppressed" )
    #print( "i pyd7tbseluowr2pyd7tbseluowrrrr d7t_bseluowrrrr : ", d7t_bseluowrrrr )
    # extract d7t_bseluowr_refnorm and d7t_bseluowr_raw, update d7t_bseluowrrrr
    for s_biology in d7t_bseluowr_refnorm.keys():
        for s_endpointset in d7t_bseluowr_refnorm[s_biology].keys():
            # break endpointset appart 
            b_cellcount = False
            s_cellcount = None
            ls_endpoint = []
            for s_end in d7t_bseluowr_refnorm[s_biology][s_endpointset].keys():
                if ( s_end in  ts_cellcount ):
                    # handle cellcount endpoint
                    if ( b_cellcount ):
                        sys.exit( "Error: d7t_bseluowr_refnorm endpointset "+str(s_endpointset)+" contains more then one cellcount endpoint specified in ts_cellcount "+ str(ts_cellcount)+", explicitely "+str(s_cellcount)+" "+str(s_end)+"." ) 
                    else:  
                        b_cellcount = True
                        s_cellcount = s_end
                else: 
                    # handle other endpoints 
                    ls_endpoint.append( s_end )
            # run endpoint set 
            if ( not b_cellcount ):
                sys.exit("Error: d7t_bseluowr_refnorm endpointset "+str(s_endpointset)+" contains no cellcount endpoint specified ts_cellcount "+str(ts_cellcount)+", only endpoints "+str(ls_endpoint)+".") 
            for s_endpoint in ls_endpoint : 
                    for s_layout in d7t_bseluowr_refnorm[s_biology][s_endpointset][s_endpoint].keys():
                        for s_drug in d7t_bseluowr_refnorm[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                            for s_dose in d7t_bseluowr_refnorm[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys():
                                for s_well in d7t_bseluowr_refnorm[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                    s_refcellc = d7t_bseluowr_refnorm[s_biology][s_endpointset][s_cellcount][s_layout][s_drug][s_dose][s_well][0]  # relative cell count endpoint  
                                    s_rawcellc = d7t_bseluowr_raw[s_biology][s_endpointset][s_cellcount][s_layout][s_drug][s_dose][s_well][0]  # absolte cell count endpoint 
                                    s_refint  = d7t_bseluowr_refnorm[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][0]  # relative real endpoint 
                                    s_rawint = d7t_bseluowr_raw[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][0]  # absolte real endpoint 
                                    t_response = (s_refcellc, s_rawcellc, s_refint, s_rawint)
                                    d7t_bseluowrrrr = pyd7tbseluowr2populate( s_biology, s_endpointset, s_endpoint, s_layout, s_drug, s_dose, s_well, t_response, d7t_bseluowr=d7t_bseluowrrrr )
    # output
    print( "o pyd7tbseluowr2pyd7tbseluowrrrr d7t_bseluowrrrr : print suppressed" )
    #print( "o pyd7tbseluowr2pyd7tbseluowrrrr d7t_bseluowrrrr :", d7t_bseluowrrrr )
    return( d7t_bseluowrrrr )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd7tbseluowrrrr2pyd5tbselurfa( d7t_bseluowrrrr, d5t_bselur_fa={}, method=statistics.mean ): 
   """
   pyd7tbseluowrrrr2pyd5tbselurfa - code to intrapolate fa50 or fa100 (fraction alive value) for raw cellcount, ref intensity and raw intensity 
   input : 
       d7t_bseluowrrrr : dictionary of dictionary of  biology, endpointset, endpoint, layout, drug, dose, well, refnorm cellcount, raw cellcount, refnorm intensity, raw intensity data object
       d5t_bselur_fa : fa 50 100 dictionar of dictionary of biology, endpointset, endpoint, layout, drug, fraction alive data object  
       method : average method (e.g. statistics.mean, statistics.median)

   output :
       d5t_bselur_fa : updated fa 50 100 dictionar of dictionary of  biology, endpointset, endpoint, layout, drug, fraction alive data object  
   note :
   function runs from dmso to small to big doses. 
   chekes mean or median of every dose if <= fa50 reached    
   when exact fa50 reached take vales. 
   when < fa50 reached intapolate back to fa50 (linreg at y lin x log scale) and take values.  
   if max dose rech and never reach < fa50, fa is 100, endpointvalues for the higest dose are taken.   
   """
   print( "i pyd7tbseluowrrrr2pyd5tbselurfa d7t_bseluowrrrr : print suppressed" )
   #print( "i pyd7tbseluowrrrr2pyd5tbselurfa d7t_bseluowrrrr :", d7t_bseluowrrrr )
   print( "i pyd7tbseluowrrrr2pyd5tbselurfa d5t_bselur_fa : print suppressed" )
   #print( "i pyd7tbseluowrrrr2pyd5tbselurfa d5t_bselur_fa :", d5t_bselur_fa )
   print( "i pyd7tbseluowrrrr2pyd5tbselurfa method :", method )
   # processing 
   for s_biology in d7t_bseluowrrrr.keys(): 
       for s_endpointset in d7t_bseluowrrrr[s_biology].keys(): 
           for s_endpoint in d7t_bseluowrrrr[s_biology][s_endpointset].keys(): 
               for s_layout in d7t_bseluowrrrr[s_biology][s_endpointset][s_endpoint].keys():
                   for s_drug in d7t_bseluowrrrr[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                       b_ok = True
                       b_fa50 = False
                       t_response = None
                       # handle dose 
                       ls_dose = list( d7t_bseluowrrrr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys() )
                       ls_dose.sort( key=float )
                       ts_dose = tuple( ls_dose )
                       # run dose
                       for s_dose in ts_dose:
                           if ( not b_fa50 ): 
                               # run well 
                               ls_yrefcellc = [] 
                               ls_yrawcellc = [] 
                               ls_yrefint = [] 
                               ls_yrawint = [] 
                               for s_well in d7t_bseluowrrrr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                   # get values 
                                   s_yrefcellc = d7t_bseluowrrrr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][0] 
                                   s_yrawcellc = d7t_bseluowrrrr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][1] 
                                   s_yrefint = d7t_bseluowrrrr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][2] 
                                   s_yrawint = d7t_bseluowrrrr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][3] 
                                   # hand values
                                   if ( 'None' not in ( s_yrefcellc, s_yrawcellc, s_yrefint, s_yrawint  ) ): 
                                       ls_yrefcellc.append( s_yrefcellc )
                                       ls_yrawcellc.append( s_yrawcellc )
                                       ls_yrefint.append( s_yrefint )
                                       ls_yrawint.append( s_yrawint )
                                    # else nop
                               # calculation average  
                               if ( len( ls_yrawcellc ) > 0): 
                                   lf_yrefcellc = [ float( n ) for n in ls_yrefcellc ] 
                                   lf_yrawcellc = [ float( n ) for n in ls_yrawcellc ] 
                                   lf_yrefint = [ float( n ) for n in ls_yrefint ] 
                                   lf_yrawint = [ float( n ) for n in ls_yrawint ] 
                                   f_yrefcellcave = method( lf_yrefcellc ) 
                                   f_yrawcellcave = method( lf_yrawcellc ) 
                                   f_yrefintave = method( lf_yrefint ) 
                                   f_yrawintave = method( lf_yrawint ) 
                                   # calculate fa50
                                   if ( f_yrefcellcave == 0.5 ):
                                       # exact fa50 
                                       f_yrefcellc50b = 0.5 
                                       f_yrawcellc50b = f_yrawcellcave 
                                       f_yrefint50b = f_yrefintave 
                                       f_yrawint50b = f_yrawintave 
                                       f_xlindose50b = float( s_dose )
                                       f_xlogdose50b = math.log( f_xlindose50b ) 
                                       b_fa50 = True
                                       #print( "= fa50 s_biology, s_endpointset,s_endpoint, s_layout,s_drug ,fa50 :", s_biology, s_endpointset,s_endpoint, s_layout,s_drug, b_fa50 )
                                       #print( "x_dose_lin, x_dose_log :", f_xlindose50b, f_xlogdose50b ) 
                                       #print( "y_ref_cellcount, y_raw_cellcount :", f_yrefcellc50b, f_yrawcellc50b )
                                       #print( "y_ref_intensity, y_raw_intensity :", f_yrefint50b, f_yrawint50b )
                                   elif ( f_yrefcellcave > 0.5 ): 
                                       # more then fa50 (more like dmso)
                                       f_yrefcellc50a = f_yrefcellcave 
                                       f_yrawcellc50a = f_yrawcellcave
                                       f_yrefint50a = f_yrefintave 
                                       f_yrawint50a = f_yrawintave 
                                       f_xlindose50a = float( s_dose )
                                       # f_xlogdose50a = math.log( f_xlindose50a ) # bue 20141006: because of dsmo non real dose 0 later calculatedn when needed 
                                   else : 
                                       # f_xrefcellave < 0.5 
                                       # less then fa50 (less like dmso, more like a common drug)
                                       try:
                                           # first real dose ok dosed (fa > 50)
                                           f_xlogdose50a = math.log( f_xlindose50a ) # bue 20141006: because of dsmo dose 0 now calculated, if first real doese below fa50 >>> overdosed 
                                           #print( "> fa50 s_biology, s_endpointset,s_endpoint, s_layout,s_drug ,fa50 :", s_biology, s_endpointset,s_endpoint, s_layout,s_drug, b_fa50 )
                                           #print( "x_dose_lin, x_dose_log :", f_xlindose50a, f_xlogdose50a ) 
                                           #print( "y_ref_cellcount, y_raw_cellcount :", f_yrefcellc50a, f_yrawcellc50a )
                                           #print( "y_ref_intensity, y_raw_intensity :", f_yrefint50a, f_yrawint50a )
                                           # get  
                                           f_yrefcellc50c = f_yrefcellcave
                                           f_yrawcellc50c = f_yrawcellcave
                                           f_yrefint50c = f_yrefintave
                                           f_yrawint50c = f_yrawintave
                                           f_xlindose50c = float( s_dose )
                                           f_xlogdose50c = math.log( f_xlindose50c )
                                           #print( "< fa50 s_biology, s_endpointset,s_endpoint, s_layout,s_drug ,fa50 :", s_biology, s_endpointset,s_endpoint, s_layout,s_drug, b_fa50 )
                                           #print( "x_dose_lin, x_dose_log :", f_xlindose50c, f_xlogdose50c ) 
                                           #print( "y_ref_cellcount, y_raw_cellcount :", f_yrefcellc50c, f_yrawcellc50c )
                                           #print( "y_ref_intensity, y_raw_intensity :", f_yrefint50c, f_yrawint50c )
                                           # lin reg 50b cellc ref
                                           #f_yrefcellc50b = (((f_yrefcellc50c - f_yrefcellc50a)/(f_xlogdose50c - f_xlogdose50a)) * (f_xlogdose50b - f_xlogdose50a)) + f_yrefcellc50a
                                           f_yrefcellc50b = 0.5
                                           # lin reg dose by xrefcel
                                           f_xlogdose50b = (((f_xlogdose50c - f_xlogdose50a) / (f_yrefcellc50c - f_yrefcellc50a)) * (f_yrefcellc50b - f_yrefcellc50a)) + f_xlogdose50a
                                           f_xlindose50b = math.exp( f_xlogdose50b )
                                           # lin reg 50b cellc raw
                                           f_yrawcellc50b = (((f_yrawcellc50c - f_yrawcellc50a) / (f_xlogdose50c - f_xlogdose50a)) * (f_xlogdose50b - f_xlogdose50a)) + f_yrawcellc50a
                                           # lin reg 50b intensity ref
                                           f_yrefint50b = (((f_yrefint50c - f_yrefint50a) / (f_xlogdose50c - f_xlogdose50a)) * (f_xlogdose50b - f_xlogdose50a)) + f_yrefint50a
                                           # lin reg 50b intensity raw
                                           f_yrawint50b = (((f_yrawint50c - f_yrawint50a) / (f_xlogdose50c - f_xlogdose50a)) * (f_xlogdose50b - f_xlogdose50a)) + f_yrawint50a
                                           #print( "= fa50 s_biology, s_endpointset,s_endpoint, s_layout,s_drug ,fa50 :", s_biology, s_endpointset,s_endpoint, s_layout,s_drug, b_fa50 )
                                           #print( "x_dose_lin, x_dose_log :", f_xlindose50b, f_xlogdose50b ) 
                                           #print( "y_ref_cellcount, y_raw_cellcount :", f_yrefcellc50b, f_yrawcellc50b )
                                           #print( "y_ref_intensity, y_raw_intensity :", f_yrefint50b, f_yrawint50b )
                                       except ValueError:
                                           # first real dose overdosed (fa < 50), drug get kicked
                                           b_ok = False
                                       # return
                                       b_fa50 = True
                               # noting to calculate 
                               else:
                                   # followup dose overdosed, only 'None' values with out reaching a real value less then fa50, drug get kicked 
                                   b_ok = False 
                       # non killing drug
                       if ( not b_fa50 ): 
                           f_xlindose50b = float( s_dose )
                           f_xlogdose50b = math.log( f_xlindose50b )
                           f_yrefcellc50b = 1
                           f_yrawcellc50b = f_yrawcellcave
                           f_yrefint50b = f_yrefintave 
                           f_yrawint50b = f_yrawintave 
                           #print( ">> fa50 s_biology, s_endpointset,s_endpoint, s_layout,s_drug ,fa50 :", s_biology, s_endpointset,s_endpoint, s_layout,s_drug, b_fa50 )
                           #print( "x_dose_lin, x_dose_log :", f_xlindose50b, f_xlogdose50b ) 
                           #print( "y_ref_cellcount, y_raw_cellcount :", f_yrefcellc50b, f_yrawcellc50b )
                           #print( "y_ref_intensity, y_raw_intensity :", f_yrefint50b, f_yrawint50b )
                       # populate fa dictionary of dictionary 
                       if ( b_ok ): 
                           t_response = ( f_xlindose50b, f_yrefcellc50b,f_yrawcellc50b, f_yrefint50b,f_yrawint50b, b_fa50 )
                       else :
                           t_response = ( 'None', 'None','None', 'None','None', 'None' )
                       d5t_bselur_fa = pyd5tbselur2populate( s_biology, s_endpointset, s_endpoint, s_layout, s_drug, t_response, d5t_bselur=d5t_bselur_fa )
   # output 
   print( "o pyd7tbseluowrrrr2pyd5tbselurfa d5t_bselur_fa : suppressed" )
   #print( "o pyd7tbseluowrrrr2pyd5tbselurfa d5t_bselur_fa :", d5t_bselur_fa )
   return( d5t_bselur_fa )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd5tbselurfa2pyd5tbselursigma( d5t_bselur_fa, d7t_bseluowr_refnorm, s_referencedose='0', f_sigma=3, d5t_bselur_sigma={} ):
   """
   pyd5tbselurfa2pyd5tbselursigma - code to calculate sigma 3 output fractin alive and referenze norm objects  
   input :
       d5t_bselur_fa : dictionary of dictionary of biology, endpointset, endpoint, list, drug, fraction alive response
       d7t_bseluowr_refnorm : dictionary of dictionary of biology, endpointset, endpoint, list, drug, dose, well, refnorm resonse
       s_referencedose : dmso refernce dose
       f_sigma  : set the n - sigam treshold by f_sigma times standard deviation
   output : 
       d5t_bselur_sigma : dictonary of dictionary of biology, endpointset, endpoint, layout, drug, of sigma response
   note : 
   3 sigma work only properly on about normal distributed data.  
   for each biology, endpointset, endpoint, layout, drug dose 0 (dmso) combiation calculate standard deviation (sigma) and the mean. 
   check for each biology, endpointset, endpoint, layout, drug related d5t_bselur_fa value if value is > or < then +-3 sigma standard deviation. set value -1 0 +1.
   """
   print( "i pyd5tbselurfa2pyd5tbselursigma d5t_bselur_fa : print suppressed" )
   print( "i pyd5tbselurfa2pyd5tbselursigma d5t_bselur_fa :", d5t_bselur_fa )
   print( "i pyd5tbselurfa2pyd5tbselursigma d7t_bseluowr_refnorm : suppressed" )
   print( "i pyd5tbselurfa2pyd5tbselursigma d7t_bseluowr_refnorm :", d7t_bseluowr_refnorm )
   print( "i pyd5tbselurfa2pyd5tbselursigma s_referencedose :", s_referencedose )
   print( "i pyd5tbselurfa2pyd5tbselursigma f_sigma :", f_sigma )
   print( "i pyd5tbselurfa2pyd5tbselursigma d5t_bselur_sigma : print suppressed" )
   print("i pyd5tbselurfa2pyd5tbselursigma d5t_bselur_sigma :", d5t_bselur_sigma )

   for s_biology in d5t_bselur_fa.keys(): 
       for s_endpointset in d5t_bselur_fa[s_biology].keys():
           for s_endpoint in d5t_bselur_fa[s_biology][s_endpointset].keys():
               for s_layout in d5t_bselur_fa[s_biology][s_endpointset][s_endpoint].keys():
                   for s_drug in d5t_bselur_fa[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                       # get response 
                       #t_response = ( f_xlindose50b, f_yrefcellc50b,f_yrawcellc50b, f_yrefint50b,f_yrawint50b, b_fa50 )
                       t_response = d5t_bselur_fa[s_biology][s_endpointset][s_endpoint][s_layout][s_drug]
                       l_resp = list( t_response )
                       #f_xlindose50b = t_response[0]
                       #f_xlogdose50b
                       #f_yrefcellc50b = t_response[1]
                       #f_yrawcellc50b = t_response[2] 
                       s_yrefint50b = t_response[3] 
                       #f_yrawint50b = t_response[4]
                       #b_fa50 = t_response[5]
                       # check for overdosed drug
                       if ( s_yrefint50b == 'None' ):
                           l_resp.append( 'None' )
                       else: 
                           f_yrefint50b = float( s_yrefint50b )
                           # get standard deviation
                           lf_mu  = [] 
                           for s_well in d7t_bseluowr_refnorm[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_referencedose].keys():
                               s_measure = d7t_bseluowr_refnorm[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_referencedose][s_well][0]
                               if ( s_measure != 'None'): 
                                   f_measure = float( s_measure )
                                   lf_mu.append( f_measure )
                           f_mu = statistics.mean( lf_mu )
                           f_stdev =  statistics.stdev( lf_mu )
                           # get 3 sigma border 
                           f_border = f_sigma * f_stdev
                           # check response value to 3 sigma border realtion
                           if ( f_yrefint50b  > (f_mu + f_border) ):
                               l_resp.append( +1 )
                           elif ( f_yrefint50b  < (f_mu - f_border) ):
                               l_resp.append( -1 )
                           else :
                               l_resp.append( 0 )
                       # populate 
                       t_resp = tuple( l_resp )
                       d5t_bselur_sigma = pyd5tbselur2populate( s_biology, s_endpointset, s_endpoint, s_layout, s_drug, t_response=t_resp, d5t_bselur=d5t_bselur_sigma )
   print( "o pyd5tbselurfa2pyd5tbselursigma d5t_bselur_sigma : print suppressed" )
   #print( "o pyd5tbselurfa2pyd5tbselursigma d5t_bselur_sigma :", d5t_bselur_sigma )
   return( d5t_bselur_sigma )


## metrics
# fa50 ok (check put sigma 3)
# slope ok
# auc ok 
# E0, Einf, Emax, E50
# IC50

# slope 
## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd7tbseluowr2pyd5tbselurslope( d7t_bseluowr_ref, ts_cellcount, d5t_bselur_slope={}, method=statistics.mean ): 
   """
   pyd7tbseluowr2pyd5tbselurslope - code to calculate the logphase slope from y:relative_intensity x:log_dose drug response curves 
   input : 
       d7t_bseluowr_ref : somehow reference normalized dictionary of dictionary object of  biology, endpointset, endpoint, layout, drug, dose, well, response data
       ts_cellcount : tuple of string of cell_count endpoint labels
       d5t_bselur_slope : slope dictionar of dictionary of biology, endpointset, endpoint, layout, drug, slope data object  
       method : average method (e.g. statistics.mean, statistics.median)
   output :
       d5t_bselur_slope : updated slope dictionar of dictionary of  biology, endpointset, endpoint, layout, drug, slope data object  
   note :
   function runs from dmso to small to big doses. 
   chekes mean or median of every dose if <= fa50 reached    
   """
   print( "i pyd7tbseluowr2pyd5tbselurslope d7t_bseluowr_ref : print supressed" )
   #print( "i pyd7tbseluowr2pyd5tbselurslope d7t_bseluowr_ref :", d7t_bseluowr_ref )
   print( "i pyd7tbseluowr2pyd5tbselurslope ts_cellcount :", ts_cellcount )
   print( "i pyd7tbseluowr2pyd5tbselurslope d5t_bselur_slope : print spressed" )
   #print( "i pyd7tbseluowr2pyd5tbselurslope d5t_bselur_slope :", d5t_bselur_slope )
   print( "i pyd7tbseluowr2pyd5tbselurslope method :", method )

   # processing 
   # for each biology  
   for s_biology in d7t_bseluowr_ref.keys():
       # for each endpointset  
       for s_endpointset in d7t_bseluowr_ref[s_biology].keys():
           # get cellcount endpoint from this endpointset
           ls_endpoint =  list( d7t_bseluowr_ref[s_biology][s_endpointset].keys() )
           s_cellcount = None
           for s_endpoint in ls_endpoint: 
               if ( s_endpoint in ts_cellcount ): 
                   if ( s_cellcount == None ): 
                       s_cellcount = s_endpoint
                       ls_endpoint.pop( ls_endpoint.index( s_cellcount ) )
                   else: 
                       sys.exit( 'Error: endpointset '+s_endpointset+' contains more then one cellcount endpoint specified in the ts_cellcount '+str( ts_cellcount )+'.' )
           # for each real endpoint in this endpointset
           for s_endpoint in ls_endpoint: 
               for s_layout in d7t_bseluowr_ref[s_biology][s_endpointset][s_endpoint].keys():
                   for s_drug in d7t_bseluowr_ref[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                       b_kill = False  # fracton alive below 50 percent 
                       t_response = None
                       # empyty  
                       lf_allyrefint = [] 
                       lf_allxlogdose = []
                       # handle dose 
                       ls_dose = list( d7t_bseluowr_ref[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys() )
                       ls_dose.sort( key=float )
                       ts_dose = tuple( ls_dose )
                       # run dose
                       for s_dose in ts_dose:
                           # if yet non killing drug 
                           if ( not b_kill ):
                               b_store = False 
                               # calculatre f_yrefcellcave 
                               ls_well = []
                               lf_aveyrefcellc = []
                               for s_well in d7t_bseluowr_ref[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                   s_yrefcellc = d7t_bseluowr_ref[s_biology][s_endpointset][s_cellcount][s_layout][s_drug][s_dose][s_well][0] 
                                   if ( s_yrefcellc != 'None' ):
                                       ls_well.append( s_well ) 
                                       lf_aveyrefcellc.append( float(s_yrefcellc) )
                               # if any well provide enough cells 
                               if ( len( ls_well ) != 0 ):
                                   f_aveyrefcellc = method( lf_aveyrefcellc )
                                   # reset value list and ok dose counter if dose not reaches cellcount value < 0.5 fraction alive (but not = 0.5 fraction alive))  
                                   if ( f_aveyrefcellc > 0.5 ): 
                                       i_okdose = 0
                                       lf_yrefint = [] 
                                       lf_xlogdose = []
                                   # set run flag to false if dose reaches cellcount value < 0.5 fraction alive  
                                   if ( f_aveyrefcellc < 0.5 ):
                                       b_kill = True
                                   # run well
                                   for s_well in ls_well: 
                                       # get values (only real endpoints not celcount)
                                       s_yrefint = d7t_bseluowr_ref[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][0] 
                                       f_xlindose = float( s_dose )
                                       # store values
                                       if ( ('None' not in ( s_yrefint, )) and (f_xlindose > 0) ): 
                                           # killing drug
                                           lf_yrefint.append( float( s_yrefint ) )
                                           lf_xlogdose.append( math.log( f_xlindose ) )
                                           # non killing drug  
                                           lf_allyrefint.append( float( s_yrefint ) )  
                                           lf_allxlogdose.append( math.log( f_xlindose ) )
                                           b_store = True
                                   # update ok dose counter
                                   if ( b_store ):
                                       i_okdose += 1
                       # if non killing drug
                       if ( b_kill == False ): 
                           lf_yrefint = lf_allyrefint
                           lf_xlogdose = lf_allxlogdose
                           i_okdose = len( set( lf_allxlogdose ) )
                       # caclucate slope
                       if ( i_okdose >= 2 ):
                           l_response = list( stats.linregress( x=lf_xlogdose, y=lf_yrefint ) )  # f_slope, f_intercept, f_rvalue, f_pvalue, f_stderr
                           l_response.append( b_kill )  # killing or non killing drug
                           t_response = tuple( l_response )
                           # populate fa dictionary of dictionary 
                           d5t_bselur_slope = pyd5tbselur2populate( s_biology, s_endpointset, s_endpoint, s_layout, s_drug, t_response, d5t_bselur=d5t_bselur_slope )
                       # else overdosed killing drug 
   # output 
   print( "o pyd7tbseluowr2pyd5tbselurslope d5t_bselur_slope : print suppressed" )
   #print( "o pyd7tbseluowr2pyd5tbselurslope d5t_bselur_slope :", d5t_bselur_slope )
   return( d5t_bselur_slope )


# area under the curve 
## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd7tbseluowr2pyd5tbselurauc( d7t_bseluowr_ref, ts_cellcount, d5t_bselur_auc={}, method=statistics.mean ): 
   """
   pyd7tbseluowr2pyd5tbselurauc - code to calculate the logphase area under the curve from y:relative_intensity x:log_dose drug response curves 
   input : 
       d7t_bseluowr_ref : somehow reference normalized dictionary of dictionary object of  biology, endpointset, endpoint, layout, drug, dose, well, response data
       ts_cellcount : tuple of string of cell_count endpoint labels
       d5t_bselur_auc : auc dictionar of dictionary of biology, endpointset, endpoint, layout, drug, auc data object  
       method : average method (e.g. statistics.mean, statistics.median)
   output :
       d5t_bselur_auc : updated auc dictionar of dictionary of  biology, endpointset, endpoint, layout, drug, auc data object  
   note :
   function runs from dmso to small to big doses. 
   chekes mean or median of every dose if <= fa50 reached    
   """
   print( "i pyd7tbseluowr2pyd5tbselurauc d7t_bseluowr_ref : print supressed" )
   #print( "i pyd7tbseluowr2pyd5tbselurauc d7t_bseluowr_ref :", d7t_bseluowr_ref )
   print( "i pyd7tbseluowr2pyd5tbselurauc ts_cellcount :", ts_cellcount )
   print( "i pyd7tbseluowr2pyd5tbselurauc d5t_bselur_auc : print spressed" )
   #print( "i pyd7tbseluowr2pyd5tbselurauc d5t_bselur_auc :", d5t_bselur_auc )
   print( "i pyd7tbseluowr2pyd5tbselurauc method :", method )

   # processing 
   # for each biology  
   for s_biology in d7t_bseluowr_ref.keys():
       # for each endpointset  
       for s_endpointset in d7t_bseluowr_ref[s_biology].keys():
           # get cellcount endpoint from this endpointset
           ls_endpoint =  list( d7t_bseluowr_ref[s_biology][s_endpointset].keys() )
           s_cellcount = None
           for s_endpoint in ls_endpoint: 
               if ( s_endpoint in ts_cellcount ): 
                   if ( s_cellcount == None ): 
                       s_cellcount = s_endpoint
                       ls_endpoint.pop( ls_endpoint.index( s_cellcount ) )
                   else: 
                       sys.exit( 'Error: endpointset '+s_endpointset+' contains more then one cellcount endpoint specified in the ts_cellcount '+str(ts_cellcount)+'.' )
           # for each real endpoint in this endpointset
           for s_endpoint in ls_endpoint: 
               for s_layout in d7t_bseluowr_ref[s_biology][s_endpointset][s_endpoint].keys():
                   for s_drug in d7t_bseluowr_ref[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                       b_kill = False  # fracton alive below 50 percent 
                       t_response = None
                       # empyty  
                       lf_allyrefint = [] 
                       lf_allxlogdose = []
                       # handle dose 
                       ls_dose = list( d7t_bseluowr_ref[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys() )
                       ls_dose.sort( key=float )
                       ts_dose = tuple( ls_dose )
                       # run dose
                       for s_dose in ts_dose:
                           if ( not b_kill ):
                               b_store = False 
                               # calculatre f_yrefcellcave
                               ls_well = []
                               lf_aveyrefcellc = []
                               for s_well in d7t_bseluowr_ref[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                   s_yrefcellc = d7t_bseluowr_ref[s_biology][s_endpointset][s_cellcount][s_layout][s_drug][s_dose][s_well][0] 
                                   if ( s_yrefcellc != 'None' ):
                                       ls_well.append( s_well )
                                       lf_aveyrefcellc.append( float( s_yrefcellc ) )
                               # if any well provide enough cells 
                               if ( len( ls_well ) != 0 ):
                                   f_aveyrefcellc = method( lf_aveyrefcellc )
                                   # reset value list and ok dose counter if dose not reaches cellcount value < 0.5 fraction alive (but not = 0.5 fraction alive))  
                                   if ( f_aveyrefcellc > 0.5 ): 
                                       i_okdose = 0
                                       lf_yrefint = [] 
                                       lf_xlogdose = [] 
                                   # set run flag to false if dose reaches cellcount value < 0.5 fraction alive  
                                   if ( f_aveyrefcellc < 0.5 ):
                                       b_kill = True
                                   # run well
                                   for s_well in ls_well: 
                                       # get values (only real endpoints not celcount)
                                       s_yrefint = d7t_bseluowr_ref[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][0] 
                                       f_xlindose = float( s_dose )
                                       # store values
                                       if ( ('None' not in ( s_yrefint, )) and (f_xlindose > 0) ):
                                           # killing drug  
                                           lf_yrefint.append( float( s_yrefint ) )
                                           lf_xlogdose.append( math.log( f_xlindose ) )
                                           # non killing drug
                                           lf_allyrefint.append( float( s_yrefint ) )
                                           lf_allxlogdose.append( math.log( f_xlindose ) )
                                           b_store = True
                                   # update ok dose counter
                                   if ( b_store ):
                                       i_okdose += 1
                       # if non killing drug
                       if ( b_kill == False ): 
                           lf_yrefint = lf_allyrefint
                           lf_xlogdose = lf_allxlogdose
                           i_okdose = len( set( lf_allxlogdose ) )
                       # caclucate auc
                       if ( i_okdose >= 2 ):
                           f_auc = np.trapz( x=lf_xlogdose, y=lf_yrefint )
                           t_response = ( f_auc, b_kill )  # auc value, killing or non killing drug boolean 
                           # populate fa dictionary of dictionary 
                           d5t_bselur_auc = pyd5tbselur2populate( s_biology, s_endpointset, s_endpoint, s_layout, s_drug, t_response, d5t_bselur=d5t_bselur_auc )
                       # else overdosed killing drug 
   # output 
   print( "o pyd7tbseluowr2pyd5tbselurauc d5t_bselur_auc : print suppressed" )
   #print( "o pyd7tbseluowr2pyd5tbselurauc d5t_bselur_auc :", d5t_bselur_auc )
   return( d5t_bselur_auc )


### 2 normalization ###
## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
## norm threshold 
def pyd7tbseluowr2threshold( f_threshold, ts_thresholdendpoint, d7t_bseluowr ):
    """
    pyd7tbseluowr2threshold - replace all values of a entier endpointset, related to a specified threshold endpoint comination out of this endpoint set to None
    input: 
        f_threshold : float filter threshold where a sample below f_threshold set by s_threshold get kicked 
        ts_thresholdendpoint : tuple of string filter endpoint where a threshold value will be set ( e.g. cell_count ) 
        d7t_bseluowr : dictionary to cut
    output: 
        d7t_cut : by threshold cutted dictionary
    note: 
    s_thresholdendpoint have to be an element of the ts_endpointset. there is error checking for this requirement. 
    """
    # input
    print( "i pyd7tbseluowr2threshold f_threshold :", f_threshold )
    print( "i pyd7tbseluowr2threshold ts_thresholdendpoint :", ts_thresholdendpoint )
    print( "i pyd7tbseluowr2threshold d7t_bseluowr : print suppressed" )
    #print( "i pyd7tbseluowr2threshold d7t_bseluowr :", d7t_bseluowr )
    # set flag 
    ls_thresholdendpoint = list( ts_thresholdendpoint )
    #print( 'ls_thresholdendpoint:', ls_thresholdendpoint )
    # empty output 
    d7t_cut = copy.deepcopy( d7t_bseluowr )
    # processing
    for s_biology in d7t_bseluowr.keys():  # get biology keys 
        for s_endpointset in d7t_bseluowr[s_biology].keys(): # get enpointset key
            for s_thresholdendpoint in ts_thresholdendpoint: 
                # if endpointset contains s_thresholdendpoint 
                try:
                    for s_layout in d7t_bseluowr[s_biology][s_endpointset][s_thresholdendpoint].keys():  # get layout keys
                        # pop  flag 
                        try: 
                            ls_thresholdendpoint.pop( ls_thresholdendpoint.index( s_thresholdendpoint ) )
                            #print( 'ls_thresholdendpoint:', ls_thresholdendpoint )
                        except ValueError:
                            pass
                        # continuer
                        for s_drug in d7t_bseluowr[s_biology][s_endpointset][s_thresholdendpoint][s_layout].keys(): # get drug keys
                            for s_dose in d7t_bseluowr[s_biology][s_endpointset][s_thresholdendpoint][s_layout][s_drug].keys(): # get dose keys
                                for s_well in d7t_bseluowr[s_biology][s_endpointset][s_thresholdendpoint][s_layout][s_drug][s_dose].keys():  # get well keys: 
                                    # if response below threshold
                                    f_response = float( d7t_bseluowr[s_biology][s_endpointset][s_thresholdendpoint][s_layout][s_drug][s_dose][s_well][0] )  # get response
                                    if ( f_response < f_threshold ) :
                                        # cut each realted value
                                        #print( '\nthe cut:' )
                                        for s_endpoint in d7t_bseluowr[s_biology][s_endpointset].keys():  # get endpoint key of this endpointset
                                            l_response = list( d7t_cut[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well] )
                                            #print( 's_biology s_endpointset s_thresholdendpoint s_endpoint s_layout s_drug s_dose s_well l_response :', s_biology,s_endpointset,s_thresholdendpoint,s_endpoint,s_layout,s_drug,s_dose,s_well,l_response )
                                            l_response[0] = 'None'  # set response below threshold to None
                                            d7t_cut[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well] = tuple( l_response )
                # else 
                except KeyError:
                    pass 
    # check if a s_thresholdendpoint ever was found in the bseluowrs object 
    if ( ls_thresholdendpoint != [] ):
        sys.exit( "Error: input d7t_bseluowr object is missing threshold endpoint "+str(ls_thresholdendpoint)+" from ts_thresholdendpoint set "+str(ts_thresholdendpoint) ) 
    # output
    print( "o pyd7tbseluowr2threshold d7t_cut : print suppressed" )
    #print( "o pyd7tbseluowr2threshold d7t_cut :", d7t_cut )
    return( d7t_cut )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
## norm antibody per cell, sliding window 
def pyd7tbseluowr2igwnorm( ts_cellcount, d7t_bseluowr, i_window=11, method=statistics.mean, s_dist='div' ):
    """
    pyd7tbseluowr2igwnorm - imunoglobulin (antibody) sliding window moving average normalize d7t_bseluowr
    input :
        ts_cellcount : tuple of string which contains all cellcount endpoints 
        d7t_bseluowr : bseluowr dictionary to be normalized
        i_window : window span. should be an odd number, will be rounded up to odd number when even. default is x11
        method : average method (e.g. statistics.mean, statistics.median)
        s_dist : distance method ( possible are sub or div ) 
    output :
        d7t_norm : slidieng window moving averge normalized belddwr dictionary
    """
    # input
    print( "i pyd7tbseluowr2igwnorm ts_cellcount :", ts_cellcount )
    print( "i pyd7tbseluowr2igwnorm d7t_bseluowr : print suppressed" )
    #print( "i pyd7tbseluowr2igwnorm d7t_bseluowr :", d7t_bseluowr )
    print( "i pyd7tbseluowr2igwnorm i_window :", i_window )
    print( "i pyd7tbseluowr2igwnorm method :", method )
    print( "i pyd7tbseluowr2igwnorm s_dist :", s_dist )
    # empty output 
    d7t_norm = copy.deepcopy( d7t_bseluowr )
    # populate list of tuple with intensity value and cell count values [(intenity, cellcount, biology, endpoint, layout, dose, well),]
    for s_biology in d7t_bseluowr.keys():
        for s_endpointset in d7t_bseluowr[s_biology].keys():
            # get endpointset and  cellcount endpoint 
            ls_endpoint = []
            s_cellcount = None
            for s_endpoint in d7t_bseluowr[s_biology][s_endpointset].keys():
                if ( s_endpoint not in ts_cellcount ):
                    # get endpoints of endpointset with cellcount endpoint kicked
                    ls_endpoint.append( s_endpoint )
                else: 
                    if ( s_cellcount == None ):
                       # cellcount endpoint of endpointset
                        s_cellcount = s_endpoint
                    else: 
                        # error check more then one cellcount endpoint in endpointset
                        sys.exit( "Error: endpoint set "+s_endpointset+" contains more then one cellcount endpoint, defined in ts_cellcount "+str(ts_cellcount) )
            if ( s_cellcount == None ):
                # error check no cellcount endpoint in endpointset
                sys.exit( "Error: endpoint set "+s_endpointset+" contains no cellcount endpoint, defined in ts_cellcount "+str(ts_cellcount) )
            # continuer
            for s_endpoint in ls_endpoint:  # cell count endpoint kicked 
                for s_layout in d7t_bseluowr[s_biology][s_endpointset][s_endpoint].keys():
                    lt_entry = []
                    for s_drug in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout].keys(): 
                        for s_dose in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys():
                            for s_well in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                #print("s_biology, s_endpointset, s_endpoint, s_layout, s_dose, s_well :", s_biology, s_endpointset, s_endpoint, s_layout, s_dose, s_well)
                                #try:
                                f_intensity = float( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][0] )  # y axis
                                f_cellcount = float( d7t_bseluowr[s_biology][s_endpointset][s_cellcount][s_layout][s_drug][s_dose][s_well][0] )  # x axis
                                t_entry = ( f_intensity, f_cellcount, s_biology, s_endpointset, s_endpoint, s_layout, s_drug, s_dose, s_well )
                                lt_entry.append( t_entry )
                                #except KeyError: 
                                #    pass  # nop maybe not each s_bilogy, s_endpointset, s_endpoint, s_layout, s_dose, s_well combination exist

                    # normalize per plate list which contains all measurement for one biology endpoint layout combination once  
                    # sort list of tuple ( cellcount raising, intensiy falling )
                    lt_entryy = sorted( lt_entry, key=lambda entry : entry[0], reverse=True )  # sorted(lt_entry, key=itemgetter(0), reverse=True) y axis intensity
                    lt_entry = sorted( lt_entryy, key=lambda entry : entry[1], reverse=False )  # sorted(lt_entryy, key=itemgetter(1), reverse=False) x axis cellcount
                    # normalize list of tuple over windows
                    i_wing = math.floor( i_window/2 )
                    for i_index in range( len(lt_entry) ):
                        # get keys out of tuple
                        #s_biology = lt_entry[i_index][2]
                        #s_endpointset = lt_entry[i_index][3] 
                        #s_endpoint = lt_entry[i_index][4] 
                        #s_layout = lt_entry[i_index][5] 
                        s_drug = lt_entry[i_index][6]  # get drug
                        s_dose = lt_entry[i_index][7]  # get dose
                        s_well = lt_entry[i_index][8]  # get well
                        # get window begin 
                        i_wingcut = 0
                        i_begin = i_index - i_wing
                        while ( i_begin < 0 ): 
                            i_wingcut += 1
                            i_begin = i_index - ( i_wing - i_wingcut )
                        # get window end
                        i_wingcut = 0
                        i_end = i_index + i_wing
                        while ( i_end >= len(lt_entry) ): 
                            i_wingcut += 1
                            i_end = i_index + ( i_wing - i_wingcut )
                        # get data in window
                        lf_intensity = []  # y axis
                        #lf_cellcount = []  # x axis 
                        for i_span in range( i_begin, (i_end+1) ):
                            lf_intensity.append( lt_entry[i_span][0] )  # y axis
                            #lf_cellcount.append( lt_entry[i_span][1] )  # x axis 
                        # get normalized value 
                        f_meanintensity = method( lf_intensity )
                        #f_meancellcount = method( lf_cellcount )
                        # get response from input bseluowr object
                        l_insert =  list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well] ) 
                        f_intensity = float( l_insert[0] )
                        # normalize
                        # sub 
                        if ( s_dist== 'sub' ):  
                            if ( f_intensity != 'None' ): 
                                l_insert[0]  = f_intensity - f_meanintensity 
                            else: 
                                l_insert[0] = 'None'
                            # div
                        elif ( s_dist == 'div' ): 
                            if ( (f_intensity != 'None') and (f_meanintensity != 0) ):        
                                l_insert[0]  = f_intensity / f_meanintensity
                            else: 
                                l_insert[0] = 'None'
                        # unknow
                        else: 
                            sys.exit( 'Error: unknow s_dist distance methode '+str(s_dist)+'. knowen are sub and div.' )
                        # put response into output bseluow dictiobary
                        d7t_norm[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well] = tuple( l_insert )
    # output
    print( "o pyd7tbseluowr2igwnorm d7t_norm : print suppressed" )
    #print( "o pyd7tbseluowr2igwnorm d7t_norm :", d7t_norm )
    return( d7t_norm )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
## norm lowess
def pyd7tbseluowr2lowessnorm( ts_cellcount, d7t_bseluowr, f_f=2./3., i_iter=3 ):
    """
    pyd7tbseluowr2lowessnorm - biopython lowless normalization adaption for d7t_bseluowr ojects, normalization per drug layout plate base
    input : 
        ts_cellcount : tuple of string which contains all cellcount endpoints 
        d7t_bseluowr : bseluowr dictionary to be normalized
        f_f : smoothing span. span given as proprton of the whole range. 1 spans over the whole range and will be the mean value of the whole scatterplot. a larger value for f will result in a smoother curve. biopython default is 2/3.
        i_iter : number of robustifying iterations. The function will run faster with a smaller number of iterations. biopython deafult is 3.
    output : 
        d7t_norm : normalized bseluowr dictionary
    note: 
    """
    # input
    print( "i pyd7tbseluowr2lowessnorm ts_cellcount :", ts_cellcount )
    print( "i pyd7tbseluowr2lowessnorm d7t_bseluowr : print suppressed" )
    #print( "i pyd7tbseluowr2lowessnorm d7t_bseluowr :", d7t_bseluowr )
    print( "i pyd7tbseluowr2lowessnorm f_f :", f_f )
    print( "i pyd7tbseluowr2lowessnorm i_iter :", i_iter )

    # write tracker file headrow  
    s_outgetfile = 'lowesstracker_rdb.txt'
    ts_label = ('Sample','EndpointSet','Endpoint','Layout','Compound','Dose_nM','Well','Measure','RawDataFile')
    # open file handle
    with open( s_outgetfile, 'w', newline='' ) as f:
        writer = csv.writer( f, delimiter='\t', quoting=csv.QUOTE_NONE )
        # handle label row
        #if (ts_label != None):
        # manipulate ts_label
        #ls_label = ['None' if ( n == None ) else n for n in ts_label]
        #ts_label = tuple( ls_label )
        # write ts_label
        writer.writerow( ts_label )
        print( "o pyd7tbseluowr2lowessnorm line :", ts_label )

    # empty result dictionary
    #d7t_norm = copy.deepcopy( d7t_bseluowr )

    # populate y with intensity value and x with cell count values
    lf_y = []
    lf_x = []
    # for each biology 
    ls_biology = list( d7t_bseluowr.keys() )
    ls_biology.sort()
    ts_biology = tuple(ls_biology)
    for s_biology in ts_biology:
        # for each endpointset 
        ls_endpointset = list(  d7t_bseluowr[s_biology].keys() )
        ls_endpointset.sort()
        ts_endpointset = tuple( ls_endpointset )
        for s_endpointset in ts_endpointset:
            ls_realendpoint = []
            s_cellcount = None
            # for each endpoint 
            ls_endpoint = list( d7t_bseluowr[s_biology][s_endpointset].keys() )
            ls_endpoint.sort()
            ts_endpoint = tuple( ls_endpoint )
            for s_endpoint in ts_endpoint:
                if ( s_endpoint not in ts_cellcount ):
                    # get endpoints of endpointset with cellcount endpoint kicked
                    ls_realendpoint.append( s_endpoint )
                else: 
                    if (s_cellcount == None):
                        # cellcount endpoint of endpointset
                        s_cellcount = s_endpoint
                    else: 
                        # error check more then one cellcount endpoint in endpointset
                        sys.exit( "Error: endpoint set "+s_endpointset+" contains more then one cellcount endpoint, defined in ts_cellcount "+str(ts_cellcount) )
            if ( s_cellcount == None ) :
                # error check no cellcount endpoint in endpointset
                sys.exit( "Error: endpoint set "+s_endpointset+" contains no cellcount endpoint, defined in ts_cellcount "+str(ts_cellcount) )
            # continuer
            for s_endpoint in ls_realendpoint:  # cell count endpoint kicked 
                ls_layout = list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint].keys() )
                ls_layout.sort()
                ts_layout = tuple( ls_layout ) 
                for s_layout in ts_layout :
                    ls_drugzoom = []
                    ls_dosezoom = []
                    ls_wellzoom = []
                    for s_drug in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                        for s_dose in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys():
                            for s_well in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                #try: 
                                f_intensity = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][0]  # y axis
                                f_cellcount = d7t_bseluowr[s_biology][s_endpointset][s_cellcount][s_layout][s_drug][s_dose][s_well][0]  # x axis
                                print( "input ... s_biology, s_endpointset, s_endpoint, s_layout, s_drug, s_dose, s_well, f_intensity, f_cellcount :", s_biology, s_endpointset, s_endpoint, s_layout, s_drug, s_dose, s_well, f_intensity, f_cellcount )
                                if (( f_intensity != 'None' ) and ( f_cellcount != 'None' )):
                                    f_intensity = float( f_intensity )  # y axis 
                                    f_cellcount = float( f_cellcount )  # x axis 
                                    ls_drugzoom.append( s_drug )
                                    ls_dosezoom.append( s_dose ) 
                                    ls_wellzoom.append( s_well )
                                    lf_y.append( f_intensity )
                                    lf_x.append( f_cellcount )
                                # else:
                                    # f_intensity = None
                                    # f_cellcount = None
                    # per biology, endpoint, layout combination calculate y axis lowess values
                    print( "processing ...", s_biology, s_endpointset, s_endpoint, s_layout )
                    af_x = np.array( lf_x, np.float )
                    af_y = np.array( lf_y, np.float ) 
                    print( "i af_x :", af_x )
                    print( "i af_y :", af_y )
                    print( "i f_f :", f_f )
                    print( "i i_iter :", i_iter )
                    af_yresult = lowess( x=af_x, y=af_y, f=f_f, iter=i_iter )  # biopython
                    # poplate result dict with lowess values
                    for i_index in range( len(ls_wellzoom) ):
                        l_response = list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][ls_drugzoom[i_index]][ls_dosezoom[i_index]][ls_wellzoom[i_index]] ) 
                        l_response[0] = af_yresult[i_index]
                        #d7t_norm[s_biology][s_endpointset][s_endpoint][s_layout][ls_drugzoom[i_index]][ls_dosezoom[i_index]][ls_wellzoom[i_index]] = tuple( l_response )

                        # write tracker file line 
                        ts_out = tuple( [s_biology, s_endpointset, s_endpoint, s_layout, ls_drugzoom[i_index], ls_dosezoom[i_index], ls_wellzoom[i_index]] + l_response ) 
                        # open file handle
                        with open( s_outgetfile, 'a', newline='' ) as f:
                            writer = csv.writer( f, delimiter='\t', quoting=csv.QUOTE_NONE )
                            # write result to file
                            writer.writerow( ts_out )
                            print( "o pyd7tbseluowr2lowessnorm line :", ts_out )

    # output
    #print( "o pyd7tbseluowr2lowessnorm d7t_norm : print suppressed" )
    #print( "o pyd7tbseluowr2lowessnorm d7t_norm :", d7t_norm )
    #return( d7t_norm )
    print( "o pyd7tbseluowr2lowessnorm  s_outgetfile :",  s_outgetfile)
    return( s_outgetfile )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
## norm z score 
def pyd7tbseluowr2zscorenorm( d7t_bseluowr, s_referencedose='0', ts_cellcount = (), stdev_allwell=False ):
    """
    pyd7tbseluowr2zscorenorm - z score normalization
    input :
        d7t_bseluowr : bseluowr dictionary to be normalized
        s_referencedose : default is 0 which this specifies the biology, endpointset, endpoint, layout, drug related control wells (e.g. dmso)
        ts_cellcount : tuple of cellcount endpoints (to be excluded for z normalization).  
        stdev_allwell : False will taking only the controll wells into accunt to calculate the stanard deviation. This is the common way to calculate the z score. But because in our case the variance in the control wells is not that big, taking only the control wells for stanard deviation calculation will lead to artificial high z scores in the non control wells. True will take for standard deviation calculation all biology, endpoint, drug related wells into account, which will smoothen the z score in the extrems.
    output : 
        d7t_norm : normalized bseluowr dictionary
    """
    # input 
    print( "i pyd7tbseluowr2zscorenorm d7t_bseluowr : print suppressed" )
    #print( "i pyd7tbseluowr2zscorenorm d7t_bseluowr :", d7t_bseluowr )
    print( "i pyd7tbseluowr2zscorenorm s_referencedose :", s_referencedose )
    print( "i pyd7tbseluowr2zscorenorm stdev_allwell :", stdev_allwell )
    # prozessing
    # empty output dictionary
    d7t_norm = copy.deepcopy( d7t_bseluowr )
    # populate dictionary of biology of endpoint of layout of dose of well of response, drug get for this case kicked
    d6t_bselowr = {}
    for s_biology in d7t_bseluowr.keys():
        for s_endpointset in d7t_bseluowr[s_biology].keys():
            for s_endpoint in d7t_bseluowr[s_biology][s_endpointset].keys():
                for s_layout in d7t_bseluowr[s_biology][s_endpointset][s_endpoint].keys(): 
                    for s_drug in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                        for s_dose in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys(): 
                            for s_well in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys(): 
                                t_response = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well]
                                d6t_bselowr = pyd6tbselowr2populate( s_biology=s_biology, s_endpointset=s_endpointset, s_endpoint=s_endpoint, s_layout=s_layout, s_dose=s_dose, s_well=s_well, t_response=t_response, b_overwrite=True, d6t_bselowr=d6t_bselowr ) # overwrite = True because drug dmso well are member of each drug of a layout plate
    # empty between result dictionary
    d6t_between = copy.deepcopy( d6t_bselowr )
    # processing 
    for s_biology in d6t_bselowr.keys():  # get biology keys
        for s_endpointset in d6t_bselowr[s_biology].keys():  # get endpointset keys
            for s_endpoint in  d6t_bselowr[s_biology][s_endpointset].keys():  # get endpoint keys
                for s_layout in d6t_bselowr[s_biology][s_endpointset][s_endpoint].keys():  # get layout keys
                    # baseline calculations
                    lf_baseline = []  # baseline stays the same for any biology:endpoint:layout combination
                    for s_well in d6t_bselowr[s_biology][s_endpointset][s_endpoint][s_layout][s_referencedose].keys():  # get dmso well keys 
                        s_entry = d6t_bselowr[s_biology][s_endpointset][s_endpoint][s_layout][s_referencedose][s_well][0]
                        if ( s_entry != 'None' ):
                            f_entry = float( s_entry )
                            lf_baseline.append( f_entry )  # get dmso value for this endpoint
                        print( "baseline fetch I: s_biology, s_endpoinset, s_endpoint, s_layout, s_referencedose, s_well lf_baseline :", s_biology, s_endpointset, s_endpoint, s_layout, s_referencedose, s_well, lf_baseline )
                    f_basemean = statistics.mean( lf_baseline )  # get mean average dmso value for this endpoint. this basemean value is used i the z_ctrlwell and z_allwell normalization
                    # baseline standard deviation
                    if ( stdev_allwell ):  # if all well, not only the dmso well, define the standard diviation any biology:endpointset:endpoint:layout combination 
                        lf_baseline = []  # reset baseline list 
                        for s_dose in d6t_bselowr[s_biology][s_endpointset][s_endpoint][s_layout].keys():  # get all dose keys 
                            for s_well in d6t_bselowr[s_biology][s_endpointset][s_endpoint][s_layout][s_dose].keys():  # get all well keys 
                                s_entry = d6t_bselowr[s_biology][s_endpointset][s_endpoint][s_layout][s_dose][s_well][0]
                                if ( s_entry != 'None' ): 
                                    f_entry = float( s_entry )
                                    lf_baseline.append( f_entry )  # get dmso value for this endpoint but all wells
                                print( "baseline fetch II: s_biology, s_endpointset, s_endpoint, s_layout, s_dose, s_well lf_baseline :", s_biology, s_endpointset, s_endpoint, s_layout, s_dose, s_well, lf_baseline )
                    # standard deviation calclation 
                    f_basestdev = statistics.stdev( lf_baseline )  # calculate the f_basestdev from the z_ctrlwell or z_allwell corresponding lf_baseline 
                    print( "baseline calculation : f_basemean, f_basestdev:", f_basemean, f_basestdev )
                    # normalization calculations
                    for s_dose in d6t_bselowr[s_biology][s_endpointset][s_endpoint][s_layout].keys():  # get dose keys 
                        for s_well in d6t_bselowr[s_biology][s_endpointset][s_endpoint][s_layout][s_dose].keys():  # get well keys 
                            l_entry = list( d6t_bselowr[s_biology][s_endpointset][s_endpoint][s_layout][s_dose][s_well] )  # get response tuple
                            s_measure = l_entry[0]
                            if ( (s_measure != 'None') and (f_basestdev != 0) ):
                                f_measure = float(s_measure)
                                f_norm = ( f_measure - f_basemean ) / f_basestdev
                            else: 
                                f_norm = 'None'
                            print( "baseline normalization : s_biology, s_endpoint, s_layout, s_dose, s_well, f_measure, f_norm :", s_biology, s_endpoint, s_layout, s_dose, s_well, f_measure, f_norm )
                            # populate inbetween dictionary 
                            l_entry[0] = f_norm
                            d6t_between[s_biology][s_endpointset][s_endpoint][s_layout][s_dose][s_well] = tuple( l_entry )
    # poplate output norm dict with lowess values
    for s_biology in d7t_norm.keys():
        for s_endpointset in d7t_bseluowr[s_biology].keys():
            for s_endpoint in d7t_bseluowr[s_biology][s_endpointset].keys():
                if ( s_endpoint not in ts_cellcount ):  # jump over cellcount endpoints 
                    for s_layout in d7t_norm[s_biology][s_endpointset][s_endpoint].keys(): 
                        for s_drug in d7t_norm[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                            for s_dose in d7t_norm[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys(): 
                                for s_well in d7t_norm[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                    t_response = d6t_between[s_biology][s_endpointset][s_endpoint][s_layout][s_dose][s_well]
                                    d7t_norm[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well] = t_response
    # output
    print( "o pyd7tbseluowr2zscorenorm d7t_norm : print suppressed" ) 
    #print( "o pyd7tbseluowr2zscorenorm d7t_norm :", d7t_norm ) 
    return( d7t_norm )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
# norm relative (dmso)
def pyd7tbseluowr2relativenorm( d7t_bseluowr, s_referencedose='0', method=statistics.mean ):
    """
    pyd7tbseluowr2relativenorm - relative reference level (baseline level, dmso level) normalization
        d7t_bseluowr : bseluowr dictionary to be normalized
        s_referencedose : reference dose which will become the 1 value of the normalization. default is 0, which this specifies the biology, endpoint, drug related controll wells (e.g. dmso)
        method : reverence dose averageing method (e.g. statistics.mean, statistics.median) 
    output : 
        d7t_norm : normalized bseluowr dictionary
    note: 
    in the begining the medina averaging methode was used. however I think to chose mean consistentely everywhere is more proper.   
    """
    # input 
    print( "i pyd7tbseluowr2relativenorm d7t_bseluowr : print suppressed" )
    #print( "i pyd7tbseluowr2relativenorm d7t_bseluowr :", d7t_bseluowr )
    print( "i pyd7tbseluowr2relativenorm s_referencedose :", s_referencedose )
    # empty output 
    d7t_norm = copy.deepcopy( d7t_bseluowr )
    # processing 
    for s_biology in d7t_bseluowr.keys():  # get biology keys
        for s_endpointset in d7t_bseluowr[s_biology].keys():  # get endpointset keys 
            for s_endpoint in d7t_bseluowr[s_biology][s_endpointset].keys():  # get endpoint keys 
                for s_layout in d7t_bseluowr[s_biology][s_endpointset][s_endpoint].keys():  # get layout keys
                    for s_drug in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout].keys():  # get drug keys 
                        # baseline calculations based on reference dose 
                        lf_baseline = []  # baseline stays the same for any biology:(endpointset):endpoint:(layout):drug cobination of the same plate 
                        for s_well in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_referencedose].keys():  # get dmso well keys
                            s_entry = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_referencedose][s_well][0]
                            if ( s_entry != 'None' ):
                                f_entry = float( s_entry )
                                lf_baseline.append( float( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_referencedose][s_well][0]) )  # get dmso value for this endpoint
                            print( "baseline fetch : s_biology, s_endpointset, s_endpoint, s_layout, s_drug, s_referencedose, s_well lf_baseline :", s_biology, s_endpointset, s_endpoint, s_layout, s_drug, s_referencedose, s_well, lf_baseline )
                        f_baseline = method( lf_baseline )  # get median average dmso value (or nowadays mean) for this endpoint 
                        print( "baseline calculation : lf_baseline, f_baseline:", lf_baseline, f_baseline )
                        # normalization calculations
                        for s_dose in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys():  # get dose keys 
                            for s_well in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():  # get well keys 
                                l_entry = list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well] )  # get response tuple
                                s_measure = l_entry[0]
                                if ( (s_measure != 'None') and (f_baseline != 0) ):
                                    f_measure = float( s_measure )
                                    f_norm = f_measure / f_baseline
                                else:
                                    f_measure = 'None'
                                    f_norm = 'None'
                                print( "baseline normalization : s_biology, s_endpointset, s_endpoint, s_layout, s_drug, s_dose, s_well, f_measure, f_norm :", s_biology, s_endpointset, s_endpoint, s_drug, s_dose, s_well, f_measure, f_norm ) 
                                # populate dictionary 
                                l_entry[0] = f_norm
                                d7t_norm[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well] = tuple( l_entry )
    # output
    print( "o pyd7tbseluowr2relativenorm d7t_norm : print suppressd" )
    #print( "o pyd7tbseluowr2relativenorm d7t_norm :", d7t_norm )
    return( d7t_norm )



### 2 txt ###
# pyltraw2txtrdb 
# utilise gl.pylt2txtrdb

## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd7tbseluowr2txtrdb( d7t_bseluowr, ts_label=('Sample','EndpointSet','Endpoint','Layout','Compound','Dose_nM','Well','Measure','RawDataFile'), s_outputfile='d7tbseluowr' ):
    """
    print out python d7t bseluowr objects to relational data base table like files
    input:
        :d7t_bseluowr : input dictionary 
        (e.g. bio dict of endpointset, dict of endpoint, dict of layout, dict of drug, dict of dose, dict of well, dict of tuple of float measure, string rawdata_file) 
        ts_label : column label row 
        s_outputfile : output file name without extension
    output: 
        s_outgetfile : output filename with extension 
    """
    # input
    print( "i pyd7tbseluowr2txtrdb d7t_bseluowr : print suppressd" )
    print( "i pyd7tbseluowr2txtrdb d7t_bseluowr :", d7t_bseluowr )
    print( "i pyd7tbseluowr2txtrdb ts_label :", ts_label )
    print( "i pyd7tbseluowr2txtrdb s_outputfile :", s_outputfile )
    # set output variable 
    s_outgetfile = s_outputfile+'_rdb.txt'
    # open file handle
    with open( s_outgetfile, 'w', newline='' ) as f:
        writer = csv.writer( f, delimiter='\t', quoting=csv.QUOTE_NONE )
        # handle label row 
        if (ts_label != None):
            # manipulate ts_label 
            ls_label = ['None' if ( n == None ) else n for n in ts_label]
            ts_label = tuple( ls_label )
            # write ts_label 
            writer.writerow( ts_label )
            print( "o pyd7tbseluowr2txtrdb line :", ts_label )
        # get keys
        ls_biology = list( d7t_bseluowr.keys() )  
        ls_biology.sort()
        for s_biology  in ls_biology:
            ls_endpointset = list( d7t_bseluowr[s_biology].keys() )
            ls_endpointset.sort()
            for s_endpointset  in ls_endpointset:
                ls_endpoint = list( d7t_bseluowr[s_biology][s_endpointset].keys() )
                ls_endpoint.sort()
                for s_endpoint in ls_endpoint:
                    ls_layout = list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint].keys() )
                    ls_layout.sort()
                    for s_layout in ls_layout:  
                        ls_drug = list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout].keys() )
                        ls_drug.sort()
                        for s_drug in ls_drug: 
                            ls_dose = list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys() )
                            ls_dose.sort()
                            for s_dose  in ls_dose:
                                ls_well = list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys() )
                                ls_well.sort()
                                for s_well in ls_well:
                                    # read out response tuple
                                    l_out = [s_biology, s_endpointset, s_endpoint, s_layout, s_drug, s_dose, s_well,] 
                                    l_out.extend( list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well] ) )
                                    # manipulate line 
                                    l_out = ['None' if (n == None) else n for n in l_out]
                                    # write line
                                    writer.writerow( l_out )
                                    print( "o pyd7tbseluowr2txtrdb line :", l_out )
    # close file handle
    f.close()
    # output
    print( "o pyd7tbseluowr2txtrdb s_outgetfile :", s_outgetfile )    
    return( s_outgetfile )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd6tbseluor2txtrdb( d6t_bseluor, ts_label=('Sample','EndpointSet','Endpoint','Layout','Compound','Dose_nM','Average','RawDataFile','Wells','Method'), s_outputfile='d6tbseluor' ):
    """
    print out python d6t bseluor objects to relational data base table like files
    input:
        :d6t_bseluor : input dictionary 
        (e.g. bio dict of endpointset, dict of endpoint, dict of layout, dict of drug, dict of dose, dict of tuple of float averaged measure, string rawdata_file, string wells, string average method) 
        ts_label : column label row 
        s_outputfile : output file name without extension
    output: 
        s_outgetfile : output filename with extension 
    """
    # input
    print( "i pyd6tbseluor2txtrdb d6t_bseluor : print suppressed" )
    #print( "i pyd6tbseluor2txtrdb d6t_bseluor :", d6t_bseluor )
    print( "i pyd6tbseluor2txtrdb ts_label :", ts_label )
    print( "i pyd6tbseluor2txtrdb s_outputfile :", s_outputfile )
    # set output variable 
    s_outgetfile = s_outputfile+'_rdb.txt'
    # open file handle
    with open( s_outgetfile, 'w', newline='' ) as f:
        writer = csv.writer( f, delimiter='\t', quoting=csv.QUOTE_NONE )
        # handle label row 
        if ( ts_label != None ):
            # manipulate ts_label
            ls_label = ['None' if (n == None) else n for n in ts_label]
            ts_label = tuple( ls_label )
            # write ts_label 
            writer.writerow( ts_label )
            print( "o pyd6tbseluor2txtrdb line :", ts_label )
        # get keys
        ls_biology = list( d6t_bseluor.keys() )  
        ls_biology.sort()
        for s_biology  in ls_biology:
            ls_endpointset = list( d6t_bseluor[s_biology].keys() )
            ls_endpointset.sort()
            for s_endpointset  in ls_endpointset:
                ls_endpoint = list( d6t_bseluor[s_biology][s_endpointset].keys() )
                ls_endpoint.sort()
                for s_endpoint in ls_endpoint:
                    ls_layout = list( d6t_bseluor[s_biology][s_endpointset][s_endpoint].keys() )
                    ls_layout.sort()
                    for s_layout in ls_layout:  
                        ls_drug = list( d6t_bseluor[s_biology][s_endpointset][s_endpoint][s_layout].keys() )
                        ls_drug.sort()
                        for s_drug in ls_drug: 
                            ls_dose = list( d6t_bseluor[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys() )
                            ls_dose.sort()
                            for s_dose  in ls_dose:
                                # read out response tuple
                                l_out = [s_biology, s_endpointset, s_endpoint, s_layout, s_drug, s_dose,] 
                                l_out.extend( list( d6t_bseluor[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose] ) )
                                # manipulate line 
                                l_out = ['None' if (n == None) else n for n in l_out]
                                # outout write line
                                writer.writerow( l_out )
                                print( "o pyd6tbseluor2txtrdb line :", l_out )
    # close file handle
    f.close()
    # output
    print( "o pyd6tbseluor2txtrdb s_outgetfile :", s_outgetfile )
    return( s_outgetfile )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def pyd5tbselur2txtrdb( d5t_bselur, ts_label=('Sample','EndpointSet','Endpoint','Layout','Compound','Measure','RawDataFile','Wells','Method'), s_outputfile='d5tbselur' ):
    """
    print out python d5t bselur objects to relational data base table like files
    input:
        d5t_bselur : input dictionary  
        (e.g. dictinary of dictionary of  bio, endpointset,  endpoint, layout, drug, dose, tuple of float calculated value, string rawdata_file, string wells, string calcuation method) 
        ts_label : column label row 
        s_outputfile : output file name without extension
    output: 
        s_outgetfile : output file name with extension
    """
    # input
    print( "i pyd5tbselur2txtrdb d5t_bselur : print suppressed" )
    #print( "i pyd5tbselur2txtrdb d5t_bselur :", d5t_bselur )
    print( "i pyd5tbselur2txtrdb ts_label :", ts_label )
    print( "i pyd5tbselur2txtrdb s_outputfile :", s_outputfile )
    # set output variable 
    s_outgetfile = s_outputfile+'_rdb.txt'
    # open file handle
    with open( s_outgetfile, 'w', newline='' ) as f:
        writer = csv.writer( f, delimiter='\t', quoting=csv.QUOTE_NONE )
        # handle label row 
        if (ts_label != None): 
            # manipulate ts_label 
            ls_label = ['None' if (n == None) else n for n in ts_label]
            ts_label = tuple( ls_label )
            # write ts_label
            writer.writerow( ts_label )
            print( "o pyd5tbselur2txtrdb line :", ts_label )
        # get keys
        ls_biology = list(d5t_bselur.keys())  
        ls_biology.sort()
        for s_biology  in ls_biology:
            ls_endpointset = list( d5t_bselur[s_biology].keys() )
            ls_endpointset.sort()
            for s_endpointset  in ls_endpointset:
                ls_endpoint = list( d5t_bselur[s_biology][s_endpointset].keys() )
                ls_endpoint.sort()
                for s_endpoint in ls_endpoint:
                    ls_layout = list( d5t_bselur[s_biology][s_endpointset][s_endpoint].keys() )
                    ls_layout.sort()
                    for s_layout in ls_layout:  
                        ls_drug = list( d5t_bselur[s_biology][s_endpointset][s_endpoint][s_layout].keys() )
                        ls_drug.sort()
                        for s_drug in ls_drug: 
                            # read out response tuple
                            l_out = [s_biology, s_endpointset, s_endpoint, s_layout, s_drug,] 
                            l_out.extend( list( d5t_bselur[s_biology][s_endpointset][s_endpoint][s_layout][s_drug] ) )
                            # manipulate line 
                            l_out = ['None' if (n == None) else n for n in l_out]
                            # outout write line
                            writer.writerow( l_out )
                            print( "o pyd5tbselur2txtrdb line :", l_out )
    # close file handle
    f.close()
    # output
    print( "o pyd5tbselur2txtrdb s_outgetfile :", s_outgetfile )
    return( s_outgetfile )


### txt 2 ### 
## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def txtraw2pyltraw ( s_biology, s_endpointset, ts_endpoint, s_layoutfile, s_rawdatafile, tivtsvivs_layoutvalue=('WellIndex','Well00','Well','Compound','ConcentrationRank','CompoundConcentration_nM',), tivtsvivs_rawdatavalue=('All #  in All',), ivs_layoutkey='Well', ivs_rawdatakey='Description', ts_headrow=('Sample','EndpointSet', 'Endpoint','Layout','WellIndex','Well00','Well','Compound','Dose_rank','Dose_nM','Measure','RawDataFile',), lt_raw=[] ):  # ts_headrow = None
    """
    txtraw2pyltraw - generate python tl raw objects from Scan^R raw output files
    input:
        s_biology : string cell line name
        ts_endpointset : strings to labeling ts_endpoint
        ts_endpoint : tupe of strings to labeling the endpoints, same ordered as tivts_rawdatavalue 
        s_layoutfile : string layout file filename
        s_rawdatafile : string Scan^R raw data file filename
        tivtsvivs_layoutvalue : tuple of or string or interger pointing to values in the s_layoutfile
        tivts_rawdatavalue : tuple of or string or interger pointing to the values in the s_rawdatafile
        ivs_layoutkey : string or integer pointing to the well coordinate column in s_layoutfile
        ivs_rawdatakey : string or interger pointing to the well coordinat column in s_rawdatafile 
        ts_headrow : tuple with column lables
        lt_raw : output list of tuples to be populated
    output:
        lt_raw : populated output list of tuples
    """
    # input 
    print("i txtraw2pyltraw s_biology :", s_biology)
    print("i txtraw2pyltraw s_endpointset:", s_endpointset)
    print("i txtraw2pyltraw ts_endpoint:", ts_endpoint)
    print("i txtraw2pyltraw s_layoutfile :", s_layoutfile)
    print("i txtraw2pyltraw s_rawdatafile :", s_rawdatafile)
    print("i txtraw2pyltraw tivtsvivs_layoutvalue :", tivtsvivs_layoutvalue)
    print("i txtraw2pyltraw tivtsvivs_rawdatavalue :", tivtsvivs_rawdatavalue)
    print("i txtraw2pyltraw ivs_layoutkey :", ivs_layoutkey)
    print("i txtraw2pyltraw ivs_rawdatakey :", ivs_rawdatakey)
    print("i txtraw2pyltraw ts_headrow :", ts_headrow)
    print("i txtraw2pyltraw lt_raw :", lt_raw)  

    # handle raw data file 
    (dt_raw, ts_raw_xaxis, ts_raw_value, ts_raw_key, s_raw_primarykey, ts_ykey, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_rawdatafile, tivtsvivs_value=tivtsvivs_rawdatavalue, tivtsvivs_key=ivs_rawdatakey, tivtsvivs_primarykey=ivs_rawdatakey, ivs_labelrow=0, b_dtl=False)
    # pop key 
    dt_raw = gl.pydt2pop(dt_matrix=dt_raw, i_pop=0)
    # handle layout file
    (dt_layout, ts_layout_xaxis, ts_layout_value, ts_layout_key, s_layout_primarykey, ts_yaxis, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_layoutfile, tivtsvivs_value=tivtsvivs_layoutvalue, tivtsvivs_key=ivs_layoutkey, tivtsvivs_primarykey=ivs_layoutkey, ivs_labelrow=0, b_dtl=False)
    # pop key
    dt_layout = gl.pydt2pop(dt_matrix=dt_layout, i_pop=0)
    # build list tuple output
    # head row 
    if (ts_headrow != None):
        # manipulate ts_headrow 
        ls_headrow = ['None' if (n == None) else n for n in ts_headrow]
        ts_headrow = tuple(ls_headrow)
        # put ts_headrow 
        lt_raw.append(ts_headrow) 
    # data row    
    for s_ykey in ts_ykey: 
        for i_x in range(len(ts_endpoint)): 
            # key biology part line
            l_line = [s_biology,] 
            # key endpoint part line
            l_line.append(s_endpointset)
            l_line.append(ts_endpoint[i_x])
            # layout file and value part line 
            s_layoutfile = s_layoutfile.replace('.txt','')
            l_layout = [s_layoutfile,] 
            l_layout.extend(list(dt_layout[s_ykey]))
            l_line.extend(l_layout)
            # raw value and file part line
            s_raw = dt_raw[s_ykey][i_x]
            l_raw = [s_raw, s_rawdatafile]
            l_line.extend(l_raw)
            # output line
            t_line = tuple(l_line)
            lt_raw.append(t_line) 
    # output
    print("o txtrawdata2ltraw lt_raw :", lt_raw)
    return(lt_raw)


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def txtrdbraw2pyd7tbseluowr( s_txtrdb_rawglue_file, ts_drugzero, ts_drugexclude=(None,), ts_txtrdbkey=('Sample','EndpointSet','Endpoint','Layout','Compound','Dose_nM','Well',), ts_txtrdbvalue=('Measure','RawDataFile',), d7t_bseluowr={} ):
    """
    txtrdbraw2pyd7tbseluowr - generate python d7t objects from txt rdb raw files, most probably generated with txtraw2pyltraw
    input: 
        s_txtrdb_rawglue_file : string pointing to the txt rdb raw data file, most probably generated with txtraw2pyltraw
        ts_drugzero :  tuple of strings contains no drug treatments (e.g. DMSO or DMSO, Media)
        ts_drugexclude :  tuple of string contains non real, non zero drug (e.g. Media)
        ts_txtrdbkey : this will endup as the d7t_bseluowr dictionary keys
        ts_txtrdbvalue : this will endup as the respose tuple 
        d7t_bseluowr : output dictionary to update
    output: 
        d7t_bseluowr : output dictionary 
    note: 
    Dose_nM could be replaced by Dose_rank
    Well could be replaced by Well00 or WellIndex
    """
    # input
    print( "i txtrdbraw2pyd7tbseluowr s_txtrdb_rawglue_file :", s_txtrdb_rawglue_file )
    print( "i txtrdbraw2pyd7tbseluowr ts_drugzero :", ts_drugzero )
    print( "i txtrdbraw2pyd7tbseluowr ts_drugexclude :", ts_drugexclude )
    print( "i txtrdbraw2pyd7tbseluowr ts_txtrdbkey :", ts_txtrdbkey )
    print( "i txtrdbraw2pyd7tbseluowr ts_txtrdbvalue :", ts_txtrdbvalue )
    print( "i txtrdbraw2pyd7tbseluowr d7t_bseluowr : print suppressed" )
    #print( "i txtrdbraw2pyd7tbseluowr d7t_bseluowr :", d7t_bseluowr )
    # processing
    # handle txt rdb raw data file 
    ( dt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count ) = gl.txtrdb2pydt( s_matrix=s_txtrdb_rawglue_file, tivtsvivs_value=ts_txtrdbvalue, tivtsvivs_key=ts_txtrdbkey, tivtsvivs_primarykey=ts_txtrdbkey, ivs_labelrow=0, b_dtl=False )
    # coodinates 
    i_biology = ts_xaxis.index( ts_txtrdbkey[0] )
    i_endpointset = ts_xaxis.index( ts_txtrdbkey[1] )
    i_endpoint = ts_xaxis.index( ts_txtrdbkey[2] )
    i_layout = ts_xaxis.index( ts_txtrdbkey[3] )
    i_drug = ts_xaxis.index( ts_txtrdbkey[4] )
    i_dose = ts_xaxis.index( ts_txtrdbkey[5] )
    i_well = ts_xaxis.index( ts_txtrdbkey[6] )
    # get layout drug set as key, drug set as set 
    de_drugset = {}
    for s_yaxis in ts_yaxis:
        # get line
        ts_line = dt_matrix[s_yaxis]
        s_drugset = ts_line[i_layout]
        s_drug = ts_line[i_drug]
        # for non zero drugs
        if ( (s_drug not in ts_drugzero) and (s_drug not in ts_drugexclude) ):
            # get related drug set out of drug layout dictionary
            try: 
                e_drugset = de_drugset[s_drugset]
            except KeyError: 
                e_drugset = set()
            # update drug set
            e_drugset.add(s_drug)  # s_drug
            # update dictionary
            de_drugset.update( {s_drugset:e_drugset} )
        else:
           pass
    # get input tuple
    for s_yaxis in ts_yaxis: 
        ts_line = dt_matrix[s_yaxis] 
        # get ts_response 
        ls_response = []
        for s_txtrdbvalue in ts_txtrdbvalue: 
            i_txtrdbvalue = ts_xaxis.index( s_txtrdbvalue )
            ls_response.append( ts_line[i_txtrdbvalue] )
        t_response = tuple( ls_response )
        # populate output dictionary
        if ( ts_line[i_drug] in ts_drugzero ): 
            # zero drug : for all drugs which this zero values belong to 
            for s_drug in de_drugset[ts_line[i_layout]]:
                d7t_bseluowr = pyd7tbseluowr2populate( s_biology=ts_line[i_biology], s_endpointset=ts_line[i_endpointset], s_endpoint=ts_line[i_endpoint], s_layout=ts_line[i_layout], s_drug=s_drug, s_dose='0', s_well=ts_line[i_well], t_response=t_response, d7t_bseluowr=d7t_bseluowr )
        else:
            # real drug 
            if ( ts_line[i_drug] not in ts_drugexclude ):  # kick non zero non real drug like media  
                d7t_bseluowr = pyd7tbseluowr2populate( s_biology=ts_line[i_biology], s_endpointset=ts_line[i_endpointset], s_endpoint=ts_line[i_endpoint], s_layout=ts_line[i_layout], s_drug=ts_line[i_drug], s_dose=ts_line[i_dose], s_well=ts_line[i_well], t_response=t_response, d7t_bseluowr=d7t_bseluowr )
    # output
    print( "o txtrdbraw2pyd7tbseluowr d7t_bseluowr : print suppressed" )
    #print( "o txtrdbraw2pyd7tbseluowr d7t_bseluowr :", d7t_bseluowr )
    return( d7t_bseluowr )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def txtrdbbseluowr2pyd7tbseluowr( s_txtrdbfile, ts_txtrdbkey=('Sample','EndpointSet','Endpoint','Layout','Compound','Dose_nM','Well',), ts_txtrdbvalue=('Measure','RawDataFile',), d7t_bseluowr={} ):
    """
    txtrdbbseluowr2pyd7tbseluowr - generate python d7t objects from txt rdb bseluowr files
    input:
        s_txtrdbfile : string pointing to the txt rdb bseluowr compatible data file
        ts_txtrdbkey = this will endup in the d7t_bseluowr dictionary keys
        ts_txtrdbvalue = this will endup in the respose tuple
        d7t_bseluowr : output dictionary to update
    output:
        d7t_bseluowr : output dictionary
    note:
    Dose_nM could be replaced by Dose_rank
    Well could be replaced by well00 or WellIndex
    """
    # input
    print( "i txtrdbbseluowr2pyd7tbseluowr s_txtrdbfile :", s_txtrdbfile )
    print( "i txtrdbbseluowr2pyd7tbseluowr ts_txtrdbkey :", ts_txtrdbkey )
    print( "i txtrdbbseluowr2pyd7tbseluowr ts_txtrdbvalue :", ts_txtrdbvalue )
    print( "i txtrdbbseluowr2pyd7tbseluowr d7t_bseluowr : print suppessed" )
    #print( "i txtrdbbseluowr2pyd7tbseluowr d7t_bseluowr :", d7t_bseluowr )
    # processing
    # handle txt rdb bseluowr data file
    ( dt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_txtrdbfile, tivtsvivs_value=ts_txtrdbvalue, tivtsvivs_key=ts_txtrdbkey, tivtsvivs_primarykey=ts_txtrdbkey, ivs_labelrow=0, b_dtl=False )
    # coodinates
    i_biology = ts_xaxis.index( ts_txtrdbkey[0] )
    i_endpointset = ts_xaxis.index( ts_txtrdbkey[1] )
    i_endpoint = ts_xaxis.index( ts_txtrdbkey[2] )
    i_layout = ts_xaxis.index( ts_txtrdbkey[3] )
    i_drug = ts_xaxis.index( ts_txtrdbkey[4] )
    i_dose = ts_xaxis.index( ts_txtrdbkey[5] )
    i_well = ts_xaxis.index( ts_txtrdbkey[6] )
    # get input tuple
    for s_yaxis in ts_yaxis:
        ts_line = dt_matrix[s_yaxis]
        # get ts_response 
        ls_response = []
        for s_txtrdbvalue in ts_txtrdbvalue: 
            i_txtrdbvalue = ts_xaxis.index( s_txtrdbvalue )
            ls_response.append( ts_line[i_txtrdbvalue] )
        t_response = tuple( ls_response )
        # populate output dictionary
        d7t_bseluowr = pyd7tbseluowr2populate( s_biology=ts_line[i_biology], s_endpointset=ts_line[i_endpointset], s_endpoint=ts_line[i_endpoint], s_layout=ts_line[i_layout], s_drug=ts_line[i_drug], s_dose=ts_line[i_dose], s_well=ts_line[i_well], t_response=t_response, d7t_bseluowr=d7t_bseluowr )
    # output
    print( "o txtrdbbseluowr2pyd7tbseluowr d7t_bseluowr : print suppressed" )
    #print( "o txtrdbbseluowr2pyd7tbseluowr d7t_bseluowr :", d7t_bseluowr )
    return( d7t_bseluowr )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def txtrdbbseluor2pyd6tbseluor( s_txtrdbfile, ts_txtrdbkey=('Sample','EndpointSet','Endpoint','Layout','Compound','Dose_nM',), ts_txtrdbvalue=('Average','RawDataFile','Wells','Method'), d6t_bseluor={} ):
    """
    txtrdbbseluor2pyd6tbseluor - generate python d6t objects from txt rdb bseluor files
    input:
        s_txtrdbfile : string pointing to the txt rdb bseluor compatible data file
        ts_txtrdbkey = this will endup in the d6t_bseluor dictionary keys
        ts_txtrdbvalue = this will endup in the respose tuple
        d6t_bseluor : output dictionary to update
    output:
        d6t_bseluor : output dictionary
    note:
    Dose_nM could be replaced by Dose_rank
    Well could be replaced by Well00 or WellIndex
    """
    # input
    print( "i txtrdbbseluor2pyd6tbseluor s_txtrdbfile :", s_txtrdbfile )
    print( "i txtrdbbseluor2pyd6tbseluor ts_txtrdbkey :", ts_txtrdbkey )
    print( "i txtrdbbseluor2pyd6tbseluor ts_txtrdbvalue :", ts_txtrdbvalue )
    print( "i txtrdbbseluor2pyd6tbseluor d6t_bseluor : print suppressed" )
    #print( "i txtrdbbseluor2pyd6tbseluor d6t_bseluor :", d6t_bseluor )
    # processing
    # handle txt rdb bseluor data file
    ( dt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_txtrdbfile, tivtsvivs_value=ts_txtrdbvalue, tivtsvivs_key=ts_txtrdbkey, tivtsvivs_primarykey=ts_txtrdbkey, ivs_labelrow=0, b_dtl=False )
    # coodinates
    i_biology = ts_xaxis.index( ts_txtrdbkey[0] )
    i_endpointset = ts_xaxis.index( ts_txtrdbkey[1] )
    i_endpoint = ts_xaxis.index( ts_txtrdbkey[2] )
    i_layout = ts_xaxis.index( ts_txtrdbkey[3] )
    i_drug = ts_xaxis.index( ts_txtrdbkey[4] )
    i_dose = ts_xaxis.index( ts_txtrdbkey[5] )
    # get input tuple
    for s_yaxis in ts_yaxis:
        ts_line = dt_matrix[s_yaxis]
        # get ts_response 
        ls_response = []
        for s_txtrdbvalue in ts_txtrdbvalue: 
            i_txtrdbvalue = ts_xaxis.index( s_txtrdbvalue )
            ls_response.append( ts_line[i_txtrdbvalue] )
        t_response = tuple( ls_response )
        # populate output dictionary
        d6t_bseluor = pyd6tbseluor2populate( s_biology=ts_line[i_biology], s_endpointset=ts_line[i_endpointset], s_endpoint=ts_line[i_endpoint], s_layout=ts_line[i_layout], s_drug=ts_line[i_drug], s_dose=ts_line[i_dose], t_response=t_response, d6t_bseluor=d6t_bseluor )
    # output
    print( "o txtrdbbseluor2pyd6tbseluor d6t_bseluor : print suppressed" )
    #print( "o txtrdbbseluor2pyd6tbseluor d6t_bseluor :", d6t_bseluor )
    return( d6t_bseluor )


## ok d7 bseluowr (Biology, endpointSet, Endpoint, Layout, drUg, dOse, Well, Responde) ##
def txtrdbbselur2pyd5tbselur( s_txtrdbfile, ts_txtrdbkey=('Sample','EndpointSet','Endpoint','Layout','Compound',), ts_txtrdbvalue=('Measure',), d5t_bselur={} ):
    """
    txtrdbbselur2pyd5tbselur - generate python d5t objects from txt rdb bselur files
    input:
        s_txtrdbfile : string pointing to the txt rdb bselur compatible data file
        ts_txtrdbkey = this will endup in the d5t_bselur dictionary keys
        ts_txtrdbvalue = this will endup in the respose tuple
        d5t_bselur : output dictionary to update
    output:
        d5t_bselur : output dictionary
    note:
    Dose_nM could be replaced by Dose_rank
    Well could be replaced by Well00 or WellIndex
    """
    # input
    print( "i txtrdbbselur2pyd5tbselur s_txtrdbfile :", s_txtrdbfile )
    print( "i txtrdbbselur2pyd5tbselur ts_txtrdbkey :", ts_txtrdbkey )
    print( "i txtrdbbselur2pyd5tbselur ts_txtrdbvalue :", ts_txtrdbvalue )
    print( "i txtrdbbselur2pyd5tbselur d5t_bselur : print suppressed" )
    #print( "i txtrdbbselur2pyd5tbselur d5t_bselur :", d5t_bselur )
    # processing
    # handle txt rdb bselur data file
    ( dt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_txtrdbfile, tivtsvivs_value=ts_txtrdbvalue, tivtsvivs_key=ts_txtrdbkey, tivtsvivs_primarykey=ts_txtrdbkey, ivs_labelrow=0, b_dtl=False )
    # coodinates
    i_biology = ts_xaxis.index( ts_txtrdbkey[0] )
    i_endpointset = ts_xaxis.index( ts_txtrdbkey[1] )
    i_endpoint = ts_xaxis.index( ts_txtrdbkey[2] )
    i_layout = ts_xaxis.index( ts_txtrdbkey[3] )
    i_drug = ts_xaxis.index( ts_txtrdbkey[4] )
    # get input tuple
    for s_yaxis in ts_yaxis:
        ts_line = dt_matrix[s_yaxis]
        # get ts_response 
        ls_response = []
        for s_txtrdbvalue in ts_txtrdbvalue: 
            i_txtrdbvalue = ts_xaxis.index( s_txtrdbvalue )
            ls_response.append( ts_line[i_txtrdbvalue] )
        t_response = tuple( ls_response )
        # populate output dictionary
        d5t_bselur = pyd5tbselur2populate( s_biology=ts_line[i_biology], s_endpointset=ts_line[i_endpointset], s_endpoint=ts_line[i_endpoint], s_layout=ts_line[i_layout], s_drug=ts_line[i_drug], t_response=t_response, d5t_bselur=d5t_bselur )
    # output
    print( "o txtrdbbselur2pyd5tbselur d5t_bselur : print suppressed" )
    #print( "o txtrdbbselur2pyd5tbselur d5t_bselur :", d5t_bselur )
    return( d5t_bselur )



### module self test  ###
if (__name__=='__main__'):

    ## 2 pyd7tbseluowr
    # populate
    print("\n*** selftest d7t bseluowr 2 populate ***")
    # cellcount 1
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='1',t_response=(992,'raw.txt'), d7t_bseluowr={})
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='2',t_response=(1008,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='3',t_response=(1008,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='4',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='5',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='6',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='7',t_response=(1040,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='8',t_response=(1040,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='9',t_response=(1056,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='1',s_well='11',t_response=(1000,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='1',s_well='12',t_response=(1001,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='1',s_well='13',t_response=(1002,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='2',s_well='14',t_response=(1047,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='2',s_well='15',t_response=(1048,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='2',s_well='16',t_response=(1049,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='4',s_well='17',t_response=(1023,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='4',s_well='18',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='cellcount1',s_layout='layout1',s_drug='drug1',s_dose='4',s_well='19',t_response=(1025,'raw.txt'), d7t_bseluowr=d_bseluowr1)
   # endpoint 1
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='1',t_response=(992,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='2',t_response=(1008,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='3',t_response=(1008,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='4',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='5',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='6',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='7',t_response=(1040,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='8',t_response=(1040,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='0',s_well='9',t_response=(1056,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='1',s_well='11',t_response=(1031,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='1',s_well='12',t_response=(1032,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='1',s_well='13',t_response=(1033,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='2',s_well='14',t_response=(1055,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='2',s_well='15',t_response=(1056,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='2',s_well='16',t_response=(1057,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='4',s_well='17',t_response=(1151,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='4',s_well='18',t_response=(1152,'raw.txt'), d7t_bseluowr=d_bseluowr1)
    d_bseluowr1 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset1',s_endpoint='endpoint1',s_layout='layout1',s_drug='drug1',s_dose='4',s_well='19',t_response=(1153,'raw.txt'), d7t_bseluowr=d_bseluowr1)

    # cellcount2
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='1',t_response=(976,'raw.txt'), d7t_bseluowr={})
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='2',t_response=(1000,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='3',t_response=(1000,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='4',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='5',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='6',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='7',t_response=(1048,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='8',t_response=(1048,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='9',t_response=(1072,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='1',s_well='30',t_response=(767,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='1',s_well='31',t_response=(768,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='1',s_well='32',t_response=(769,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='2',s_well='33',t_response=(31,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='2',s_well='34',t_response=(32,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='2',s_well='35',t_response=(33,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='4',s_well='36',t_response=(15,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='4',s_well='37',t_response=(16,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='cellcount2',s_layout='layout1',s_drug='drug2',s_dose='4',s_well='38',t_response=(17,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    # endpoint2
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='1',t_response=(92,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='2',t_response=(1008,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='3',t_response=(1008,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='4',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='5',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='6',t_response=(1024,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='7',t_response=(1040,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='8',t_response=(1040,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='0',s_well='9',t_response=(1056,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='1',s_well='30',t_response=(1015,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='1',s_well='31',t_response=(1016,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='1',s_well='32',t_response=(1017,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='2',s_well='33',t_response=(991,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='2',s_well='34',t_response=(992,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='2',s_well='35',t_response=(993,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='4',s_well='36',t_response=(895,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='4',s_well='37',t_response=(896,'raw.txt'), d7t_bseluowr=d_bseluowr2)
    d_bseluowr2 = pyd7tbseluowr2populate(s_biology='bio1',s_endpointset='eset2',s_endpoint='endpoint2',s_layout='layout1',s_drug='drug2',s_dose='4',s_well='38',t_response=(897,'raw.txt'), d7t_bseluowr=d_bseluowr2)

    # extend 
    print("\n*** selftest : py d7t bselUOWr 2 extend and py d7t bselUOWr 2 populate ***")
    d_bseluowrx = pyd7tbseluowr2extend(d7t_bseluowr=d_bseluowr1, d7t_bseluowrextend=d_bseluowr2)


    ### normalizations ### 
    print("\n*** selftest : py d7t bselUOWr 2 threshold ***")
    d_bseluowr_cut = pyd7tbseluowr2threshold(f_threshold=128, ts_thresholdendpoint=('cellcount1','cellcount2',), d7t_bseluowr=d_bseluowrx)

    print("\n*** selftest : py d7t bselUOWr 2 ig w norman ***")
    d_bseluowr_ignorm = pyd7tbseluowr2igwnorm(ts_cellcount=('cellcount1','cellcount2',), d7t_bseluowr=d_bseluowrx, i_window=11)

    print("\n*** selftest py d7t bselUOWr 2 lowess norm ***")
    d_bseluowr_lowess = pyd7tbseluowr2lowessnorm(ts_cellcount=('cellcount1','cellcount2',), d7t_bseluowr=d_bseluowrx, f_f=2./3., i_iter=3)

    print("\n*** selftest py d7t bselUOWr 2 z score norm and py d6t bselOWr 2 populate ***")
    d_bseluowr_znorm = pyd7tbseluowr2zscorenorm(d7t_bseluowr=d_bseluowrx, s_referencedose='0', ts_cellcount = ('cellcount1','cellcount2',), stdev_allwell=False)
    d_bseluowr_zjnorm = pyd7tbseluowr2zscorenorm(d7t_bseluowr=d_bseluowrx, s_referencedose='0', ts_cellcount = ('cellcount1','cellcount2',), stdev_allwell=True)

    print("\n*** selftest py d7t bselUOWr 2 relative dmso reference norm ***") 
    d_bseluowr_relativenorm = pyd7tbseluowr2relativenorm(d7t_bseluowr=d_bseluowrx, s_referencedose='0')


    ### metrics ### 
    # 2 py d6t bseluor averageing
    print("\n*** selftest : py d7t bselUOWr 2 pyd6tbselUOr and py d6t bselUOr 2 populate ***")
    d_bseluor_ave = pyd7tbseluowr2pyd6tbseluor(d7t_bseluowr=d_bseluowrx, d6t_bseluor={}, method=statistics.mean)

    # 2 pyd7t bseluow rrrr (sigma)
    print("\n*** selftest : py d7t bselUOWr 2 py d7t bselUOWrrrr and  py d7t bselUOWr 2 populate ***")
    d_bseluowrrrr = pyd7tbseluowr2pyd7tbseluowrrrr(d7t_bseluowr_raw=d_bseluowrx, d7t_bseluowr_refnorm=d_bseluowr_relativenorm, ts_cellcount=('cellcount1','cellcount2',), d7t_bseluowrrrr={})
    # 2 py d5t bselur fa (sigma)
    print("\n*** selftest : py d7t bselUOWrrrr 2 py d5t bselUr fa and  py d7t bselUr 2 populate ***")
    d5t_bselur_fa = pyd7tbseluowrrrr2pyd5tbselurfa(d7t_bseluowrrrr=d_bseluowrrrr, d5t_bselur_fa={}) 

    # 2 py d5t bselur sigma
    print("\n*** selftest : py d5t bselUr fa 2 py d5t bselUr sigma and py d7t bselUr 2 populate ***")
    d5t_bselur_sigma = pyd5tbselurfa2pyd5tbselursigma(d5t_bselur_fa=d5t_bselur_fa, d7t_bseluowr_refnorm=d_bseluowr_relativenorm, s_referencedose='0', f_sigma=3, d5t_bselur_sigma={})

    # 2 slope
    print("\n*** selftest : py d7t bselUOWrrrr 2 py d5t bselUr slope and  py d7t bselUr 2 populate ***")
    d5t_bselur_slope = pyd7tbseluowr2pyd5tbselurslope(d7t_bseluowr_ref=d_bseluowr_relativenorm, ts_cellcount=('cellcount1','cellcount2',), d5t_bselur_slope={}, method=statistics.mean) 

    # 2 auc
    print("\n*** selftest : py d7t bselUOWrrrr 2 py d5t bselUr auc and  py d7t bselUr 2 populate ***")
    d5t_bselur_auc = pyd7tbseluowr2pyd5tbselurauc(d7t_bseluowr_ref=d_bseluowr_relativenorm, ts_cellcount=('cellcount1','cellcount2',), d5t_bselur_auc={}, method=statistics.mean) 


    ### 2 txt ###
    print("\n*** selftest : py d7t bselUOWr 2 txt rdb ***")
    pyd7tbseluowr2txtrdb(d7t_bseluowr=d_bseluowrx, ts_label=('sample','endpointset','endpoint','layout','compound','dose_nM','well','measure','rawdatafile'), s_outputfile='selftest_pyd7tbseluowr2txtrdb')

    print("\n*** selftest : py d7t bselUOr 2 txt rdb ***")
    pyd6tbseluor2txtrdb(d6t_bseluor=d_bseluor_ave, ts_label=('sample','endpointset','endpoint','layout','compound','dose_nM','average','rawdatafile','wells','method'), s_outputfile='selftest_pyd6tbseluor2txtrdb')

    print("\n*** selftest : py d5t bselUr 2 txt rdb ***")
    #pyd5tbselur2txtrdb(d5t_bselur=d5t_bselur_fa, ts_label=('sample','endpointset','endpoint','layout','compound','dose_nM','cellcount_ref','cellcount_raw','intensity_ref','intensity_raw','kill'), s_outputfile='selftest_pyd5tbselur2txtrdb')
    pyd5tbselur2txtrdb(d5t_bselur=d5t_bselur_sigma, ts_label=('sample','endpointset','endpoint','layout','compound','dose_nM','cellcount_ref','cellcount_raw','intensity_ref','intensity_raw','kill','sigma'), s_outputfile='selftest_pyd5tbselur2txtrdb')


    ### txt 2 ###
    # txt real raw 2 lt raw 
    print("\n*** selftest : txt RAW 2 py ltRAW and gl.pylt2txtrdb ***")
    ltraw_txtraw = txtraw2pyltraw (s_biology='bio0', s_endpointset='eset0', ts_endpoint=('end0','end1','end2'), s_layoutfile='layout_v0.txt', s_rawdatafile='scanrraw_well_rdb.txt', tivtsvivs_layoutvalue=('WellIndex','Well00','Well','Master plate quadrant','Master plate number','Compound','CAS Number','ConcentrationRank','CompoundConcentration_nM'), tivtsvivs_rawdatavalue=('END1/All/Mean Intensity Alexa 647/Mean','END2/All/Mean Intensity Alexa 488/Mean','END3/All/Mean Intensity Alexa 568/Mean'), ivs_layoutkey='Well', ivs_rawdatakey='Description', ts_headrow=('sample','endpointset', 'endpoint','layout','wellindex','well00','well','master_plate_quadrant','master_plate_number','compound','cas_number','dose_rank','dose_nM','measure','rawdatafile'), lt_raw=[])
    # next step 
    gl.pylt2txtrdb(s_outputfile='selftest_pyltraw2txtrdbraw', lt_matrix=ltraw_txtraw, t_xaxis=None, s_mode='w')

    # txt lt raw 2 bseluowr rdb raw 
    print("\n*** selftest : txt rdb RAW 2 py d7t bselUOWr and py d7t bselUOWr 2 populate ***")
    d_bseluor_txtrawrdb = txtrdbraw2pyd7tbseluowr(s_txtrdb_rawglue_file='selftest_pyltraw2txtrdbraw_glue.txt', ts_drugzero=('DMSO'), ts_drugexclude=('Media',), ts_txtrdbkey=('sample','endpointset','endpoint','layout','compound','dose_nM','well',), ts_txtrdbvalue=('measure','rawdatafile',), d7t_bseluowr={})

    # txt rdb 2 bseluowr 
    print("\n*** selftest : txt rdb bselUOWr 2 py d7t bselUOWr and py d7t bselUOWr 2 populate ***")
    d_bseluowr_txt = txtrdbbseluowr2pyd7tbseluowr(s_txtrdbfile='selftest_pyd7tbseluowr2txtrdb_rdb.txt', ts_txtrdbkey=('sample','endpointset','endpoint','layout','compound','dose_nM','well',), ts_txtrdbvalue=('measure','rawdatafile',), d7t_bseluowr={})

    print("\n*** selftest : txt rdb bselUOr 2 py d6t bselUOr and py d6t bselUOr 2 populate ***")
    d_bseluor_txt = txtrdbbseluor2pyd6tbseluor(s_txtrdbfile='selftest_pyd6tbseluor2txtrdb_rdb.txt', ts_txtrdbkey=('sample','endpointset','endpoint','layout','compound','dose_nM',), ts_txtrdbvalue=('average','rawdatafile','wells','method'), d6t_bseluor={})

    print("\n*** selftest : txt rdb bselUr 2 py d5t bselUr and py d5t bselUr 2 populate ***")
    d_bselur_txt = txtrdbbselur2pyd5tbselur(s_txtrdbfile='selftest_pyd5tbselur2txtrdb_rdb.txt', ts_txtrdbkey=('sample','endpointset','endpoint','layout','compound',), ts_txtrdbvalue=('dose_nM','cellcount_ref','cellcount_raw','intensity_ref','intensity_raw','kill',), d5t_bselur={})
