"""
# file : gltocircos.py
# 
# history: 
#     author: bue
#     date: 2014-01-03 
#     license: >= GPL3 
#     language: Python 3.3.3
#     description: This library will provide  glue interface naming convention compatible modules for circos based data analyis.
#
#  version:
#     0.0-1 2014-01-28 txtrdbxlsxcircos2xlsxkaryotype xlsx python circos filter for the xlsx2circos pipeline
#
"""

# load libraries
#import pyglue.glue as gl  # bue 20140219: old glue library version 
import pygluedev.glue as gl  # bue 20140408: newglue library version 


# glue interface naming convention compatible functions 
#########
# circos #
##########
#def txtrdbgene2circostrack(s_inputfile, s_ucscgenome, s_chromosome, s_start, s_end, s_value, s_option=None, s_outputfile):
#    """
#    """
#    # chr start end value [options]
#    b_ok = False
#    txtrdb2pydf()
#xs    return(b_ok)


###############
# xlsx circos #
###############
def txtrdbxlsxcircos2xlsxkaryotype(s_chromosomeinput='xlsxchromosome.txt', s_bandinput='xlsxchromosomeband.txt', s_idchromosome='#id_chromosome', s_idband='#id_band'): 
    """
    #
    """
    # input 
    print("i txtrdbxlsxcircos2xlsxkaryotype s_chromosomeinput : ", s_chromosomeinput)
    print("i txtrdbxlsxcircos2xlsxkaryotype s_bandinput : ", s_bandinput)
    print("i txtrdbxlsxcircos2xlsxkaryotype s_idchromosome : ", s_idchromosome)
    print("i txtrdbxlsxcircos2xlsxkaryotype s_idband : ", s_idband)

    # empty output variable
    b_ok = False
    b_karyo_ok = False
    b_label_ok = False
    # input chromosome
    #(dt_chromosome, t_chromosome_xaxis, t_chromosome_xaxis_value, t_chromosome_xaxis_key, s_chromosome_xaxis_primarykey, t_chromosome_yaxis, dt_chromosome_count) = gl.txtrdb2pydtvlt(s_matrix=s_chromosomeinput, tivts_value=('#label_chromosome','#color',), tivts_key=('#id_chromosome',), tivts_primarykey=('#id_chromosome',))
    (dt_chromosome, t_chromosome_xaxis, t_chromosome_xaxis_value, t_chromosome_xaxis_key, s_chromosome_xaxis_primarykey, t_chromosome_yaxis, dt_chromosome_count) = gl.txtrdb2pydtvlt(s_matrix=s_chromosomeinput, tivts_value=(s_idchromosome,), tivts_key=(s_idchromosome,), tivts_primarykey=(s_idchromosome,))
    print("\nt_chromosome_yaxis :", t_chromosome_yaxis)
    print("t_chromosome_xaxis :", t_chromosome_xaxis)
    print("dt_chromosome :\n", dt_chromosome)
    # input band
    #(dt_band, t_band_xaxis, t_band_xaxis_value, t_band_xaxis_key, s_band_xaxis_primarykey, t_band_yaxis, dt_band_count) = gl.txtrdb2pydtvlt(s_matrix=s_bandinput, tivts_value=('#id_chromosome','#id_band','#label_band','#color',), tivts_key=('#id_chromosome','#id_band',), tivts_primarykey=('#id_chromosome', '#id_band',))
    (dt_band, t_band_xaxis, t_band_xaxis_value, t_band_xaxis_key, s_band_xaxis_primarykey, t_band_yaxis, dt_band_count) = gl.txtrdb2pydtvlt(s_matrix=s_bandinput, tivts_value=(s_idchromosome,s_idband,s_idband,'#color',), tivts_key=(s_idchromosome,s_idband,), tivts_primarykey=(s_idchromosome, s_idband,))
    print("\nt_band_yaxis :", t_band_yaxis)
    print("t_band_xaxis :", t_band_xaxis)
    print("dt_band :\n", dt_band)
    # band per chomosome
    (dt_chrband, t_chrband_xaxis, t_chrband_xaxis_value, t_chrband_xaxis_key, s_chrband_xaxis_primarykey, t_chrband_yaxis, dt_chrband_count) = gl.txtrdb2pydtvlt(s_matrix=s_bandinput, tivts_value=(s_idband,), tivts_key=(s_idchromosome,), tivts_primarykey=(s_idchromosome,))
    print("\nt_chrband_yaxis :", t_chromosome_yaxis)
    print("t_chrband_xaxis :", t_chromosome_xaxis)
    print("dt_chrband :\n", dt_chrband)
    print("dt_chrband_count :\n", dt_chrband_count,"\n")
    # build kareotype list of tuple
    lt_karyotype = []
    lt_karyotype.append(('#chr_or_band','#id_one','#id_two','#label','#start','#stop','#color'))

    # process chromosome
    for chr in t_chromosome_yaxis:
        #print("chr :",chr, dt_chromosome[chr])
        l_karyotype = []
        l_karyotype.append("chr")
        l_karyotype.append("-")
        l_karyotype.append(dt_chromosome[chr][0])  # chromosome id
        l_karyotype.append(dt_chromosome[chr][1])  # chromosome label
        l_karyotype.append(0)  # start
        l_karyotype.append(dt_chrband_count[chr][1])  # stop
        l_karyotype.append("grey")  # color
        #l_karyotype.append(dt_chromosome[chr][2])  # color
        # out
        t_karyotype = tuple(l_karyotype)
        lt_karyotype.append(t_karyotype)
        print("t_karyotype :",t_karyotype)

    # process band
    chrnow = None
    for band in t_band_yaxis:
        chrprocessed = dt_band[band][2]  # chromosome id
        #print("band :", band, "chrnow :", chrnow, "chrprocessed :", chrprocessed)
        if (chrprocessed != chrnow):
            count  = 0
            chrnow = chrprocessed
        l_karyotype = []
        l_karyotype.append('band') 
        l_karyotype.append(chrprocessed)  # chromosome id
        l_karyotype.append(dt_band[band][3])  # band id 
        l_karyotype.append(dt_band[band][4])  # band label
        l_karyotype.append(count)  # start
        count = count + 1
        l_karyotype.append(count)  # end
        l_karyotype.append(dt_band[band][5])  # color
        # out
        t_karyotype = tuple(l_karyotype)
        lt_karyotype.append(t_karyotype)
        print("t_karyotype :",t_karyotype)

    # build label data track list of tuple
    lt_label = []
    lt_label.append(('#chromosome_id','#start','#stop','#chrband_label'))
    # process 
    for t_y in lt_karyotype: 
        if (t_y[0] == 'band'):
            print("t_y :", t_y)
            l_label = []
            l_label.append(t_y[1]) # chromosome id
            l_label.append(t_y[4]) # start
            l_label.append(t_y[5]) # stop
            l_label.append(t_y[3]) # band label
            # out
            t_label = tuple(l_label)
            lt_label.append(t_label)
            print("t_label :",t_label)

    # output
    b_karyo_ok = gl.pydtvlt2txtrdb(s_outputfile='xlsxkaryotype', dtvlt_matrix=lt_karyotype)
    b_label_ok = gl.pydtvlt2txtrdb(s_outputfile='xlsxlabeltrack', dtvlt_matrix=lt_label)
    b_ok = b_karyo_ok and b_label_ok
    print("o txtrdbxlsxcircos2xlsxkaryotype b_ok", b_ok)
    return(b_ok)

 
def txtrdbxlsxcircos2xlsxlink(s_karyotypeinput='xlsxkaryotype_rdb.txt', s_outputfile='xlsxlink', s_linkinput='xlsxlink.txt', s_idbandsource='#id_band_source', s_idbandsink='#id_band_sink',  s_output='xlsxlink'): 
    """
    # 
    """
    # input
    print("i txtrdbxlsxcircos2xlsxlink s_karyotypeinput", s_karyotypeinput)
    print("i txtrdbxlsxcircos2xlsxlink s_outputfile", s_outputfile)
    print("i txtrdbxlsxcircos2xlsxlink s_linkinput", s_linkinput)
    print("i txtrdbxlsxcircos2xlsxlink s_idbandsource", s_idbandsource)
    print("i txtrdbxlsxcircos2xlsxlink s_idbandsink", s_idbandsink)
    print("i txtrdbxlsxcircos2xlsxlink s_output", s_output)

    # set empty output variables
    b_ok = False
    # input karyotype
    (dd_karyo, dd_y_karyo) = gl.txtrdb2pydd(s_matrix=s_karyotypeinput, tivtsvivs_value=(1,4,5), tivtsvivs_key=(0,2,3), ivs_primarykey=2) # tivts_value=(4,5,6)
    #print("\ndd_karyo", dd_karyo)
    #print("ddt_y_karyo", dd_y_karyo,"\n") 
    # input link
    #(lt_link, t_link_xaxis, t_link_xaxis_value, t_link_xaxis_key, s_link_xaxis_primarykey, t_link_yaxis, dt_link_count) = gl.txtrdb2pydtvlt(s_matrix=s_linkinput, tivts_value=('#id_band_source','#id_band_sink','#options',), tivts_key=None, tivts_primarykey=None)
    (lt_link, t_link_xaxis, t_link_xaxis_value, t_link_xaxis_key, s_link_xaxis_primarykey, t_link_yaxis, dt_link_count) = gl.txtrdb2pydtvlt(s_matrix=s_linkinput, tivts_value=(s_idbandsource, s_idbandsink,'#options',), tivts_key=None, tivts_primarykey=None)
    #print("\nt_link_yaxis :", t_link_yaxis)
    #print("t_link_xaxis :", t_link_xaxis)
    #print("lt_link :\n", lt_link,"\n")

    # build link list of tuple
    lt_outlink = []
    lt_outlink.append(('#chromosome_source_id','#start_source','#stop_source','#chromosome_sink_id','#start_sink','#stop_sink','#option'))
    # process 
    for t_y in lt_link: 
        print("t_y :", t_y)
        #print("dd_karyo[t_y[0]] :", dd_karyo[t_y[0]])
        l_outlink = []
        # band_source
        l_outlink.append(dd_karyo[t_y[0]]['#id_one'])
        l_outlink.append(dd_karyo[t_y[0]]['#start'])
        l_outlink.append(dd_karyo[t_y[0]]['#stop'])
        # band_sink
        l_outlink.append(dd_karyo[t_y[1]]['#id_one'])
        l_outlink.append(dd_karyo[t_y[1]]['#start'])
        l_outlink.append(dd_karyo[t_y[1]]['#stop'])
        # options
        l_outlink.append(t_y[2])
        # out
        t_outlink = tuple(l_outlink)
        lt_outlink.append(t_outlink)
        print("t_outlink :",t_outlink)

    # output
    #print("\nlt_outlink :\n", lt_outlink)
    b_ok = gl.pydtvlt2txtrdb(s_outputfile=s_output, dtvlt_matrix=lt_outlink)
    print("o txtrdbxlsxcircos2xlsxlink b_ok", b_ok)
    return(b_ok)


# module self test  
if __name__=='__main__':
    # txt rdb xlsx circos 2 xlsx kareotype 
    print('*** selftest txt rdb xlsx circos 2 xlsx kareotype***')
    b_ok = txtrdbxlsxcircos2xlsxkaryotype()
    #txt rdb xlsx circos 2 xlsx link self test
    print('***  selftest txt rdb xlsx circos 2 xlsx link***')
    b_ok = txtrdbxlsxcircos2xlsxlink()
