"""
# file : heattree.py
# 
# history: 
#     author: bue
#     date: 2014-01-03 
#     license: >= GPL3 
#     language: Python 3.3.3
#     description: heat map and phylogenetic tree cluster analysis modules
#
"""

# load libraries
import sys
import numpy as np
import scipy as sp
import scipy.spatial as spspat
import scipy.cluster as spclu
import matplotlib as mpl
import matplotlib.pyplot as plt
from  pyanalysis.gltowell import txtrdbbselur2pyd5tbselur # pyd7tbseluowr2pltheattree_sigma


# heat tree functions
def heattree( llf_matrix, ls_rowlabel, ls_columnlabel, s_rowmethod=None, s_columnmethod=None, s_metric='euclidean', s_colorgradient=None, f_vmin=None, f_vmax=None, s_title=None, s_outputfile='prj_norm', t_figsize=(8.5,8.5), b_display=False ):
    """ 
    heattree - to generate a heatmap dendrogram plot
    input: 
        llf_matrix : list of list of float matrix
        ls_rowlabel : python list contains row names in the same order as stored in llf_matrix
        ls_columnlabel : python list contains column names  in the same order as stord in llf_matrix
        s_rowmethod : python string cluster methode for row, for detils goto s_columnmethod
        s_columnmethod: python string cluster methode for columns 
            None : not clustering !
            'single' : nearest point algorithm
            'complete' : farthest point algorithm (Hees Algorithm) : default R language gplots.heatmap2 > hclust pkg
            'average' : UPGMA algorithm
            'weighted' : WPGMA algorithm
            'centroid' : UPGMC algorithm
            'median' : WPGMC algorithm
            'ward' : Ward variance minimization algorithm
            http://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html#scipy.cluster.hierarchy.linkage
        s_metric : python string dictance metric
            'euclidean' : Euclidean distance : default R language gplots.heatmap2 > dist pkg 
            'minkowski' : Minkowski distance 
            'cityblock' : city block or Manhattan distance
            'seuclidean' : standardized Euclidean distance
            'sqeuclidean' : squared Euclidean distance
            'cosine' : cosine distance
            'correlation' : correlation distance 
            'hamming' : normalized Hamming distance
            'jaccard' : Jaccard distance
            'chebyshev' : Chebyshev distance
            'canberra' : Jaccard distance
            'braycurtis' : Jaccard distance
            'mahalanobis' : Mahalanobis distance
            'yule' : Yule distance 
            'matching' : matching distance
            'dice' : Dice distance
            'kulsinski' : Kulsinski distance 
            'rogerstanimoto' : Rogers-Tanimoto distance
            'russellrao' : Russell-Rao distance 
            'sokalmichener' : Sokal-Michener distance
            'sokalsneath' : Sokal-Sneath distance 
            'wminkowski' : weighted Minkowski distance
            'f' : distance between all pairs of vectors in X using the user supplied 2-arity function 
            http://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html
        s_colorgradient : options 'rainbow', 'blue_white_read', None == 'blue_white_read'! 
            http://matplotlib.org/api/colors_api.html 
            http://matplotlib.org/examples/color/colormaps_reference.html#color-colormaps-reference
        f_vmin : number to set the min cutoff for coloring. default None will automaticaly set the cutoff.
        f_vmax : number to set the max cutoff for coloring. default None will automaticaly set the cutoff.
        s_title : plot title. default None prints colourkey above the spectrum box 
        s_outputfile : python string output filen name, without .png extension
        t_figsize : tuple of flots, contains paper y length and x whide in inch. should be quadratic!
        b_display : python boolean. False == no screen output, True == X11 output 

    output: 
        s_outputfile.png : png heatmap dendrogram plot
        npat_matrix : numpy array hierarchical clustered transformed npa_matrix input array
        ls_rowlabelt : python list hierarchical clustered transformed ls_rowlabel input list 
        ls_columnlabelt : python list hierarchical clustered transformed ls_columnlabel input list 
    """
    # input 
    print( "i heattree llf_matrix : suppresed" )
    #print( "i heattree llf_matrix :\n", llf_matrix )
    print( "i heattree ls_rowlabel :", ls_rowlabel )
    print( "i heattree ls_columnlabel :", ls_columnlabel )
    print( "i heattree s_rowmethod :", s_rowmethod )
    print( "i heattree s_columnmethod :", s_columnmethod )
    print( "i heattree s_metric :", s_metric )
    print( "i heattree s_colorgradient :", s_colorgradient )
    print( "i heattree f_vmin :", f_vmin )
    print( "i heattree f_vmax :", f_vmax )
    print( "i heattree s_title :", s_title )
    print( "i heattree s_outputfile :", s_outputfile )
    print( "i heattree t_figsize :", t_figsize )
    print( "i heattree b_display :", b_display )

    ## color 
    # translate input color gradient for heatmap output
    if s_colorgradient == 'rainbow': # rainbow
        cmap=plt.cm.rainbow
    #elif s_colorgradient == 'blue_white_red': # blue white red
    #cmap=plt.cm.bwr
    #elif s_colorgradient == 'blue_white_red': # blue white red
    #cmap=plt.cm.bwr
    else:
        cmap=plt.cm.bwr

    # convert lli list of list of floats to npa numpy array 
    npa_matrix = np.array( llf_matrix )

    # scale max and min colors symetric so that 0 is white/black
    if (f_vmax == None) and (f_vmin == None): 
        f_vmin = npa_matrix.min()
        f_vmax = npa_matrix.max()
        f_vmax = max( [f_vmax, abs(f_vmin)] )
        f_vmin = f_vmax*-1
    # scales colors asymetric
    elif (f_vmax == None):
        f_vmax = npa_matrix.max()
    elif (f_vmin == None):
        f_vmin = npa_matrix.min()

    # adjust the max and min to scale these colors, bue 2014-10-10: (vmin/2, vmax/2) ?
    cnorm = mpl.colors.Normalize(f_vmin, f_vmax)  

    ## font size
    if (len(ls_rowlabel) > 256) or (len(ls_columnlabel) > 256):
        f_rgap = +0.32
        f_cgap = -0.96
        plt.rcParams['font.size'] = 1
    elif (len(ls_rowlabel) > 128) or (len(ls_columnlabel) > 128):
        f_rgap = +0.32
        f_cgap = -0.96
        plt.rcParams['font.size'] = 2
    elif (len(ls_rowlabel) > 128) or (len(ls_columnlabel) > 96):
        f_rgap = +0.32
        f_cgap = -0.96
        plt.rcParams['font.size'] = 3
    elif (len(ls_rowlabel) > 64) or (len(ls_columnlabel) > 64):
        f_rgap = +0.32
        f_cgap = -0.96
        plt.rcParams['font.size'] = 4
    elif (len(ls_rowlabel) > 32) or (len(ls_columnlabel) > 32):
        f_rgap = +0.32
        f_cgap = -0.96
        plt.rcParams['font.size'] = 5
    elif (len(ls_rowlabel) > 16) or (len(ls_columnlabel) > 16):
        f_rgap = +0.16
        f_cgap = -0.96
        plt.rcParams['font.size'] = 8
    else:
        f_rgap = -0.4
        f_cgap = -0.8
        plt.rcParams['font.size'] = 9

    ## layout
    # paper size
    fig = plt.figure( figsize=t_figsize ) # could use m,n to scale here
    # element spacer
    space = 0.01
    # placement of y dendrogram on the left of the heatmap in relation to the paper size
    [ydendro_l, ydendro_b, ydendro_w, ydendro_h] = [0.03, 0.15, 0.2, 0.6]  
    # placement of x dendrogram on the top of the heatmap in relation to the paper size
    xdendro_l = ydendro_l + ydendro_w + space
    xdendro_b = ydendro_b + ydendro_h + space
    xdendro_w = 0.6
    xdendro_h = 0.2
    # placement of heatmap in realtion to paper size
    hmap_l = ydendro_l + ydendro_w + space
    hmap_b = ydendro_b 
    hmap_w = xdendro_w
    hmap_h = ydendro_h
    # placement of the color legend in relation to paper size
    clegend_l = ydendro_l 
    clegend_b = xdendro_b + (6*space)
    clegend_w = ydendro_w - (2*space)
    clegend_h = xdendro_h - ((6+1)*space)

    ## processing and ploting                                                        
    # row cluster and plot left y dendrogram
    if s_rowmethod != None:
        ydendro = fig.add_axes([ydendro_l, ydendro_b, ydendro_w, ydendro_h], frame_on=True)  # dendogram frame_on: True or False
        d1 = spspat.distance.pdist(npa_matrix)
        D1 = spspat.distance.squareform(d1)  # full matrix
        Y1 = spclu.hierarchy.linkage(D1, method=s_rowmethod, metric=s_metric)  # cluster method default 'single'; cluster metric default 'euclidean'
        Z1 = spclu.hierarchy.dendrogram(Y1, orientation='right') # plot dendrogram
        ydendro.set_xticks([])  # hides ticks
        ydendro.set_yticks([])  # hides ticks
        #ind1 = spclu.hierarchy.fcluster(Y1,0.7*max(Y1[:,2]),'distance')  # this is the default behavior of dendrogram
    #else:
        #ind1 = ['NA']*len(ls_rowlabel)  # used for exporting the flat cluster data

    # column clust and plot top x dendrogram
    if s_columnmethod != None:
        xdendro = fig.add_axes([xdendro_l, xdendro_b, xdendro_w, xdendro_h], frame_on=True)  # dendogram frame_on: True or False
        d2 = spspat.distance.pdist(npa_matrix.T)
        D2 = spspat.distance.squareform(d2) # full matrix
        Y2 = spclu.hierarchy.linkage(D2, method=s_columnmethod, metric=s_metric)  # process cluster method default 'single'; cluster metric default 'euclidean'
        Z2 = spclu.hierarchy.dendrogram(Y2)  # plot dendrogram
        xdendro.set_xticks([])  # hides ticks
        xdendro.set_yticks([])  # hides ticks
        #ind2 = spclu.hierarchy.fcluster(Y2,0.7*max(Y2[:,2]),'distance') # this is the default behavior of dendrogram
    #else:
        #ind2 = ['NA']*len(ls_columnlabel)  # used for exporting the flat cluster data

    # plot map values
    l_y_index=[]  # define empty variable
    l_x_index=[]  # define empty variable
    hmap = fig.add_axes([hmap_l, hmap_b, hmap_w, hmap_h]) 
    npat_matrix = npa_matrix  # transformed map 
    # row 
    if s_rowmethod != None:
        l_y_index = Z1['leaves']  # apply y dendrograms clustering to the map
        npat_matrix = npat_matrix[l_y_index,:]   
        #ind1 = ind1[l_y_index]  # reorder flat cluster
    # column
    if s_columnmethod != None:
        l_x_index = Z2['leaves']  # apply x dendograms clustering to the map
        npat_matrix = npat_matrix[:,l_x_index]
        #ind2 = ind2[l_x_index]  # reorder the flat cluster 
    # map 
    #print( "npat_matrix cmap norm :", npat_matrix, cmap, cnorm )
    hmap.matshow(npat_matrix, aspect='auto', origin='lower', cmap=cmap, norm=cnorm)
    hmap.set_xticks([]) # hides x-ticks
    hmap.set_yticks([]) # hide y-ticks

    # plot label
    ls_rowlabelt = []
    ls_columnlabelt = []
    # row 
    for i in range(npa_matrix.shape[0]):
        if s_rowmethod != None:
            if len(ls_rowlabel) < 1024:  # do not visualize row labels when more than 1024 rows
                hmap.text(npa_matrix.shape[1]+f_rgap, i, ls_rowlabel[l_y_index[i]])
            ls_rowlabelt.append(ls_rowlabel[l_y_index[i]])  # f_rgap + 0.3
        else:
            ### When not clustering rows
            if len(ls_rowlabel) < 1024: # do not visualize column labels when more than 1024 columns
                hmap.text(npa_matrix.shape[1]+f_rgap, i, ls_rowlabel[i])  # f_rgap + 0.3
            ls_rowlabelt.append(ls_rowlabel[i])
    # column
    for i in range(npa_matrix.shape[1]):
        if s_columnmethod != None:
            if len(ls_columnlabel) < 1024:  # do not visualize row labels when more than 1024 rows
                hmap.text(i, f_cgap, ls_columnlabel[l_x_index[i]], rotation='vertical', verticalalignment="top")  # f_cgap -1.2, -0.6 
            ls_columnlabelt.append(ls_columnlabel[l_x_index[i]])
        else:
            ### When not clustering rows
            if len(ls_columnlabel) < 1024:  # do not visualize row labels when more than 1024 rows
                hmap.text(i, f_cgap, ls_columnlabel[i], rotation='vertical', verticalalignment="top") # f_cgap -1.2, -0.6 
            ls_columnlabelt.append(ls_columnlabel[i])

    # plot color legend
    clegend = fig.add_axes([clegend_l, clegend_b, clegend_w, clegend_h], frame_on = False)  # axes for colorbar
    mpl.colorbar.ColorbarBase(clegend, cmap=cmap, norm=cnorm, orientation='horizontal')
    clegend.set_title("colorkey")
    # plot title
    if (s_title != None): 
        plt.title(s_title)

    ## output
    plt.savefig(s_outputfile+'_pltheattree.pdf', format='pdf')  # file format: png, svg, pdf; bue 2014-01-10: dpi dots per inch can be set!   
    if b_display: 
        plt.show()  # display
    print("o heattree npat_matrix :\n", npat_matrix)
    print("o heattree ls_rowlabelt", ls_rowlabelt)
    print("o heattree ls_columnlabelt", ls_columnlabelt)
    print("o heattree l_y_index", l_y_index)
    print("o heattree l_x_index", l_x_index)
    return( npat_matrix, ls_rowlabelt, ls_columnlabelt, l_y_index, l_x_index )


## 3 sigma 
def pyd7tbseluowr2pltheattree_sigma( d5t_bselur_sigma, i_xsigma=6, i_xkill=5, s_outputfile='prj_norm', t_figsize=(8.5,8.5) ):  # s_title 
    """
    py d7t bseluowr 2 plt heattree_sigma - code to display sigma 3 output as a heat map
    input:
        d5t_bselur_sigma : dictionary of dictionary of biology, endpointset, endpoint, layout, drug, resonse tuple ( dose_nM, cellcount_ref, cellcount_raw, intensity_ref, intensity_raw, kill, sigma )
        i_xsigma : sigma position in response tuple
        i_xkill : kill position in response tuple
        s_outputfile : output filen name string. without extension.
        t_figsize : tuple of flots, contains paper y length and x whide in inch. should be quadratic!
    output: 
        lli_matrix : heattree input list of list of float matrix
        ls_rowlabel (endpoints) : heattree input list contains row names in the same order as stored in llf_matrix 
        ls_column (drugs) : heattree input list contains column names  in the same order as stord in llf_matrix 
    note: 
    d5t_bselur_sigma input most probably generated by gltowell's
    pyd7tbseluowr2pyd7tbseluowrrrr | pyd7tbseluowrrrr2pyd5tbselurfa | pyd5tbselurfa2pyd5tbselursigma
    """ 
    print( "i pyd7tbseluowr2pltheattree_sigma d5t_bselur_sigma : suppressed" )
    #print( "i pyd7tbseluowr2pltheattree_sigma d5t_bselur_sigma :", d5t_bselur_sigma )
    print( "i pyd7tbseluowr2pltheattree_sigma i_xsigma :", i_xsigma )
    print( "i pyd7tbseluowr2pltheattree_sigma i_xkill :", i_xkill )
    print( "i pyd7tbseluowr2pltheattree_sigma s_outputfile :", s_outputfile )
    print( "i pyd7tbseluowr2pltheattree_sigma t_figsize :", t_figsize )
    # empty set 
    es_biology = set()
    es_endpointset = set()
    es_endpoint = set()
    es_layout = set()
    # es_drug = set()
    # populate sets
    for s_biology in d5t_bselur_sigma.keys():
        es_biology.update( [s_biology,] )  # handle biology
        for s_endpointset in d5t_bselur_sigma[s_biology].keys():
            es_endpointset.update( [s_endpointset,] )  # handle endpoint
            for s_endpoint in d5t_bselur_sigma[s_biology][s_endpointset].keys():
                es_endpoint.update( [s_endpoint,] )  # handle endpoint
                for s_layout in d5t_bselur_sigma[s_biology][s_endpointset][s_endpoint].keys():
                    es_layout.update( [s_layout,] )  # handle layout
                    #for s_drug in d5t_bselur_sigma[s_biology][s_endpointset][s_endpoint][s_layout].keys():
                        #es_drug.update( [s_drug,] )  # handle drug
    # sort sets 
    #ls_drug = list(es_drug)
    #ls_drug.sort()
    # for each drug plate layout 
    for s_layout in es_layout: 
        # for each biolog layout combination == plot == xy matrix 
        for s_biology in d5t_bselur_sigma.keys():
            lli_matrix = []
            ls_rowlabel = []
            # for each endpointset 
            ls_endpointset = list( d5t_bselur_sigma[s_biology].keys() )
            ls_endpointset.sort()
            for s_endpointset  in ls_endpointset:
                b_getalive = True
                # for each endpoint == y matrix row 
                ls_endpoint = list( d5t_bselur_sigma[s_biology][s_endpointset].keys() )
                ls_endpoint.sort()
                for s_endpoint in ls_endpoint:
                    li_alive = []
                    li_sigma = []                        
                    # for each drug of s_layout == x matrix column 
                    ls_drug = list( d5t_bselur_sigma[s_biology][s_endpointset][s_endpoint][s_layout].keys() )
                    ls_drug.sort()
                    for s_drug in ls_drug: 
                        print( "s_biology s_endpointset s_endpoint s_layout s_drug :", s_biology, s_endpointset, s_endpoint, s_layout, s_drug )
                        try: 
                            t_resp = d5t_bselur_sigma[s_biology][s_endpointset][s_endpoint][s_layout][s_drug]
                            #print( "t_resp :", t_resp )
                            # get 3 sigma and alive value
                            s_sigma = t_resp[i_xsigma]
                            if ( s_sigma == 'None' ): 
                                # overdosed drug
                                i_sigma = 0 
                                i_alive = 0  # kill 'None' 
                            else: 
                                # real dosed drug
                                i_sigma = int( t_resp[i_xsigma] )
                                s_kill = t_resp[i_xkill]
                                i_alive = None
                                if ( s_kill == 'True' ):  
                                    i_alive = -1  # killing drug, nothing alive 
                                elif ( s_kill == 'False' ): 
                                    i_alive = 1  # non killing drug, a lot alive
                                else: 
                                    sys.exit( "Error: s_kill stores invalid value "+str(s_kill)+". valid values are 'True' and 'False' and 'None'." )
                            # go to catch alive value
                            li_alive.append( i_alive )
                            # go to catch sigma 3 values
                            li_sigma.append( i_sigma )
                        except KeyError: 
                            sys.exit( "Error: the d5t_bselur_sigma layout "+s_layout+" input data does not resemble a matrix. Not all drug treatments "+str(ls_drug)+" are coverd by measurements of all endpoint "+str(ls_rowlabel)+"." )
                    # got to catch them all 
                    if ( b_getalive ): 
                        ls_rowlabel.append( 'alive' )
                        lli_matrix.append( li_alive )
                        b_getalive = False
                    ls_rowlabel.append( s_endpoint )
                    lli_matrix.append( li_sigma )
            # plot
            s_title = s_biology+'_'+s_layout
            s_out = s_outputfile + '_'+s_title
            heattree( llf_matrix=lli_matrix, ls_rowlabel=ls_rowlabel, ls_columnlabel=ls_drug, s_rowmethod=None, s_columnmethod=None, s_metric='euclidean', s_colorgradient='rainbow', f_vmin=-1, f_vmax=+1, s_title=s_title, s_outputfile=s_out, t_figsize=t_figsize, b_display=False )
    # return  
    print( "o pyd7tbseluowr2pltheattree_sigma lli_matrix :", lli_matrix )
    print( "o pyd7tbseluowr2pltheattree_sigma ls_rowlabel ( endpoints ) :", ls_rowlabel )
    print( "o pyd7tbseluowr2pltheattree_sigma ls_columnlabel ( drugs ) :", ls_drug )
    return( lli_matrix, ls_rowlabel, ls_drug )


## xcluster input
def txtraw2txtxcluster(s_matrix, tivtsvivs_value, tivtsvivs_key, tivtsvivs_primarykey, ivs_labelrow=0, s_outputfile='inputxcluser'): 
    """
    input: 
    output: 
        d_xcluster
    referneces: 
    http://www.stanford.edu/group/sherlocklab/cluster.html
    http://jtreeview.sourceforge.net/
    """
    # empty output
    d_xcluster = {}
     # get data and populate dictionary
    l_row000 = ['UID','NAME','GWEIGHT',]
    l_row000.extend(tivtsvivs_value)
    d_xcluster.update({'000': tuple(row000)})
    l_row001 = ['EWEIGHT','','',]
    l_row001.extend(len(tivtsvivs_value)*[1])
    d_xcluster.update({'001': tuple(row001)})
    (dt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = txtrdb2pydt(s_matrix, tivtsvivs_value, tivtsvivs_key, tivtsvivs_primarykey, ivs_labelrow=None, b_dtl=False) 
    d_xcluster.update(d_xcluster)
    # write file  
    pydt2txtrdb(s_outputfile=s_outputfile, dt_matrix=d_xcluster, t_xaxis=None, s_mode='w')
    return(d_xcluster)



# module self test  
if ( __name__=='__main__' ):
    ## heattree
    print( '\n*** selftest heattree unit ***' )
    # test input
    llf_matrix = [[-18, -17, -16, -15, -14, -13, -12, -11, -10],
    [-9, -8, -7, -6, -5, -4, -3, -2, -1],
    [-0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1],
    [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9],
    [1, 2, 3, 4, 5, 6, 7, 8, 9],
    [10, 11, 12, 13, 14, 15, 16, 17, 18],
    [19, 20, 21, 22, 23, 24, 25, 26, 27], 
    [28, 29, 30, 31, 32, 33, 34, 35, 36], 
    [37, 38, 39, 40, 41, 42, 43, 44, 45]]
    ls_rowlabel = ['a','bb','ccc','dddd','e','ff','ggg','hhhh','i']
    ls_columnlabel = ['A','BB','CCC','DDDD','E','FF','GGG','HHHH','I']
    # test run 
    ( npat, ls_rowlabelt, ls_columnlabelt, l_y_index, l_x_index ) = heattree( llf_matrix, ls_rowlabel, ls_columnlabel, s_rowmethod='complete', s_columnmethod='complete', s_outputfile='selftest', b_display = False )

    ## heattree_sigma
    print( '\n*** selftest heattree sigma ***' )
    s_txtrdbfile = 'selftest_pyd5tbselur2txtrdb_rdb.txt' 
    d5t_bselur_sigma = txtrdbbselur2pyd5tbselur( s_txtrdbfile=s_txtrdbfile, ts_txtrdbkey=('sample','endpointset','endpoint','layout','compound',), ts_txtrdbvalue=('dose_nM','cellcount_ref','cellcount_raw','intensity_ref','intensity_raw','kill','sigma'), d5t_bselur={} )
    d5t_bselur_sigma = pyd7tbseluowr2pltheattree_sigma( d5t_bselur_sigma=d5t_bselur_sigma, i_xsigma=6, i_xkill=5, s_outputfile='prj_norm', t_figsize=(8.5,8.5) ) 
