"""
# file : gltoscatter.py
# 
# history: 
#     author: bue
#     date: 2014-03-11 
#     license: >= GPL3 
#     language: Python 3.3.3
#     description: box and whisker plots
#
"""

# load libraries
import sys 
import matplotlib.pyplot as plt
import pyglue.glue as gl
from pyanalysis.glueto import pyd7tbseluowr2populate # test case


# modules
def pyd7tbseluowr2pltscatter_intensitycellcount( d7t_bseluowr, ts_cellcount=None, s_yscale='log', s_xscale='linear', tf_ylim=None, tf_xlim=None, t_figsize=(8.5,8.5), s_outfile='prj_norm' ):
    """
    plot intensity versus cellcount scatter plots out of a d7t_bseluowr object 
    input: 
        d7t_bseluowr : python dictionary of dictionary of biology, endpoint, layout, drug, dose, well, response tuple ([measure],[rawdatafile])
        ts_cellcount : tuple of strings of cell count endpoint
        s_yscale : set y scale linear or log 
        s_xscale : set x scale linear or log
        tf_ylim : tuple of float of y axis limits like (0,8). None = autoscale 
        tf_xlim : tuple of float of x axis limits. None = autoscale 
        t_figsize : tuple of flots, contains paper y length and x whide in inch 
        s_outfile : string with outputfile name, without extension
    output: 
        png file with plot 
    """
    print( "i pyd7tbseluowr2pltscatter_intensitycellcount d7t_bseluowr : print suppressed" )
    #print( "i pyd7tbseluowr2pltscatter_intensitycellcount d7t_bseluowr :\n", d7t_bseluowr )
    print( "i pyd7tbseluowr2pltscatter_intensitycellcount ts_cellcount :", ts_cellcount )
    print( "i pyd7tbseluowr2pltscatter_intensitycellcount s_yscale :", s_yscale )
    print( "i pyd7tbseluowr2pltscatter_intensitycellcount s_xscale :", s_xscale )
    print( "i pyd7tbseluowr2pltscatter_intensitycellcount tf_ylim :", tf_ylim )
    print( "i pyd7tbseluowr2pltscatter_intensitycellcount tf_xlim :", tf_xlim )
    print( "i pyd7tbseluowr2pltscatter_intensitycellcount t_figsize :", t_figsize )
    print( "i pyd7tbseluowr2pltscatter_intensitycellcount s_outfile :", s_outfile )
    # constants 
    ls_color = ['#6F3D86','#352879','#6C5EB5','#70A4B2','#588D43','#9AD284','#B8C76F','#6F4F25','#9A6759','#68372B','#433900','#959595','#6C6C6C','#444444',] 
    ls_marker = ['.','v','*', 'o','^','s', '8','<','p',  'h','>','d',  'H',',','D','+','x','|','_',]
    # for each biology
    for s_biology in d7t_bseluowr.keys():
        # for each endpointset
        for s_endpointset in d7t_bseluowr[s_biology].keys():
            # get s_cellline  and ts_endpoint 
            es_endpoint = set()
            s_cellcount = None
            for s_endpoint in d7t_bseluowr[s_biology][s_endpointset].keys(): 
                if ( s_endpoint in ts_cellcount ): 
                    if ( s_cellcount == None ): 
                        s_cellcount = s_endpoint
                    else: 
                        sys.exit( 'Error: endpointset '+s_endpointset+' contains more then one endpoint specified in ts_cellcount '+str(ts_cellcount) )
                else: 
                    es_endpoint.update( [s_endpoint,] ) 
            # if cellcount in endpointset 
            if ( s_cellcount != None ): 
                # each non cellcount endpoint of the endpointset
                for s_endpoint in  es_endpoint: 
                    # for each layout
                    for s_layout in d7t_bseluowr[s_biology][s_endpointset][s_endpoint].keys():
                        # set basics 
                        s_title = s_biology+' : '+s_endpointset+' : '+s_cellcount+' : '+s_layout
                        s_out = s_outfile+'_'+s_biology+'_'+s_endpointset+'_'+s_cellcount+'_'+s_layout
                        i_farbe = 0
                        i_zeichen = 0
                        plt.figure( figsize=t_figsize )  # set figure figsize=(11,8.5) figsize=(8.5,11) 
                        ax = plt.subplot( 1,1,1 )  # generate just one figure and one subplot
                        plt.ylabel( '[intensity]',  color='k' )  # set label
                        plt.xlabel( '[cell_number]\n\n'+s_title, color='k' )  # set drug label
                        #plt.title(s_title)
                        ls_drug = list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout].keys() )
                        ls_drug.sort()
                        for s_drug in ls_drug:
                            lf_y = []
                            lf_x = []
                            for s_dose in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys():
                                for s_well in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                    # got value 
                                    try:
                                        # y value 
                                        s_y = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][0]
                                        b_ok = True
                                        try: 
                                            f_y = float( s_y ) 
                                        except ValueError:  #TypeError
                                            b_ok = False
                                        # x value
                                        s_x = d7t_bseluowr[s_biology][s_endpointset][s_cellcount][s_layout][s_drug][s_dose][s_well][0]
                                        try:
                                            f_x = float( s_x ) 
                                        except ValueError: #TypeError 
                                            b_ok = False
                                        # put value in to list 
                                        if b_ok: 
                                            lf_y.append( f_y )
                                            lf_x.append( f_x )
                                            #ls_farbe.append( ls_color[i_farbe] )
                                            #ls_zeichen.append( ls_marker[i_zeichen] )   
                                    # gat no value
                                    except KeyError: 
                                        pass  # nop
                            # set color and mark 
                            if (i_farbe >= 13): 
                                i_farbe = 0
                                i_zeichen += 1
                            else: 
                                i_farbe += 1
                            # plot datapoints 
                            ax.scatter( lf_x, lf_y, color=ls_color[i_farbe], marker=ls_marker[i_zeichen], label=s_drug )  # xy ! 
                        # for each biology : endpoint combinatin  plot file 
                        ax.autoscale( enable=True, axis='y' )
                        ax.autoscale( enable=True, axis='x' )
                        if ( tf_ylim != None ):
                            ax.autoscale( enable=False, axis='y' )
                            ax.set_ylim( tf_ylim[0],tf_ylim[1] )
                        if ( tf_xlim != None ):
                            ax.autoscale( enable=False, axis='x' )
                            ax.set_xlim( tf_xlim[0],tf_xlim[1] )
                        ax.set_yscale( s_yscale )
                        ax.set_xscale( s_xscale )
                        # legend
                        legend = ax.legend( loc='lower left', bbox_to_anchor=(0,1), ncol=9, fontsize='xx-small', fancybox=True )
                        legend.get_frame().set_alpha(0.3)
                        # output
                        #plt.show()
                        plt.savefig( s_out+'_pltscatter_intensitycellcount.pdf', format='pdf' )
    # ende
    print( "o pyd7tbseluowr2pltscatter_intensitycellcount s_outfile :", s_outfile )
    return( s_outfile )



# module self test  
if __name__=='__main__':
    # seftest obj
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='endpointset1', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', s_dose='1', s_well='well1', t_response=(1,'rawabc',), d7t_bseluowr={})
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='endpointset1', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', s_dose='2', s_well='well2', t_response=(2,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='endpointset1', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well3', t_response=(8,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='endpointset1', s_endpoint='cellcount1', s_layout='layout1', s_drug='drug1', s_dose='1', s_well='well1', t_response=(16,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='endpointset1', s_endpoint='cellcount1', s_layout='layout1', s_drug='drug1', s_dose='2', s_well='well2', t_response=(1,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='endpointset1', s_endpoint='cellcount1', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well3', t_response=(2,'rawabc',), d7t_bseluowr=d7t_test)
   
    # scatter
    print('\n*** selftest py d7t bseluowr 2 plt box cellcount plot ***')
    pyd7tbseluowr2pltscatter_intensitycellcount( d7t_bseluowr=d7t_test, ts_cellcount=('cellcount1',), s_yscale='log', s_xscale='linear', tf_ylim=None, tf_xlim=None, t_figsize=(8.5,8.5), s_outfile='selftest' )
