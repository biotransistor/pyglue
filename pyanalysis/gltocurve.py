"""
# file : gltogridcurve.py
# 
# history: 
#     author: bue
#     date: 2014-01-03 
#     license: >= GPL3 
#     language: Python 3.3.3
#     description: biology drug endpoint response curve grid plot
#
"""

# load libraries
import math
import statistics
import sys 
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from pyanalysis.glueto import pyd7tbseluowr2populate # test case

def pyd7tbseluowr2pltcurveendpoint( d7t_bseluowr, ts_cellcount=None, s_outputfile='prj_norm', tf_ylim=None, t_figsize=(8.5, 8.5), b_ic=False ):
    """
    plot curve grid per biology drug combination out of a d7t_bseluowr object 
    input: 
        d7t_bseluowr: python dictionary of dictionary of biology of endpointset of endpoint of layout of drug of dose of well of response tuple ([measure],[rawdatafile]) 
        ts_cellcount : tuple of cellcount endpoints 
        s_outputfile : string with outputfile name. without extension
        tf_ylim : set y axis limit. None = auto scaling 
        t_figsize : tuple of flots, contains paper y length and x whide in inch 
        b_ic : boolean defined if ic50 nearest dose should be highlided, and checked if any drug dose at all cause at least ic20 
            works only correct on on dmso normaliced values where cellcount data is between 1 and 0 !  
    output: 
        dt_ic : if b_ic = False, dt_ic will be None
            if b_ic is True, a dictionary of tuples about each biology : cellcount_endpointset : drug combination with the ic results will be outputed
        png file with plot 
    """
    # set color and marker palette 
    # purple, blue, lightblue, cyan, green, lightgreen, yellow, orange, lightred, red, brown, lightgrey, grey, darkgrey, 
    ls_color = ['#6F3D86','#352879','#6C5EB5','#70A4B2','#588D43','#9AD284','#B8C76F','#6F4F25','#9A6759','#68372B','#433900','#959595','#6C6C6C','#444444',]  
    ls_marker = ['.','v','*', 'o','^','s', '8','<','p',  'h','>','d',  'H',',','D',]
    ls_markercellcount = ['+','x','|','_',]
    #ls_marker =[',','.','_','|','+','x','*','v','^','<','>','s','d','D','p','h','H','8','o',] 

    # input 
    print( "i pyd7tbseluowr2pltcurveendpoint d7t_bseluowr : print suppressed" )
    #print( "i pyd7tbseluowr2pltcurveendpoint d7t_bseluowr :\n", d7t_bseluowr )
    print( "i pyd7tbseluowr2pltcurveendpoint ts_cellcount :", ts_cellcount )
    print( "i pyd7tbseluowr2pltcurveendpoint s_outputfile :", s_outputfile )
    print( "i pyd7tbseluowr2pltcurveendpoint tf_ylim :", tf_ylim )
    print( "i pyd7tbseluowr2pltcurveendpoint t_figsize :", t_figsize )
    print( "i pyd7tbseluowr2pltcurveendpoint b_ic :", b_ic )
    # set empty output variable 
    dt_ic = None
    # empty set
    es_biology = set()
    es_endpointset = set()
    es_endpoint = set()
    es_layout = set()
    es_drug = set()
    # populate sets
    for s_biology in d7t_bseluowr.keys():
        es_biology.update( [s_biology,] )  # handle biology
        for s_endpointset in d7t_bseluowr[s_biology].keys():
            es_endpointset.update( [s_endpointset,] )  # handle endpointset
            for s_endpoint in d7t_bseluowr[s_biology][s_endpointset].keys():
                es_endpoint.update( [s_endpoint,] )  # handle endpoint
                for s_layout in d7t_bseluowr[s_biology][s_endpointset][s_endpoint].keys(): 
                    es_layout.update( [s_layout,] )  # handle layout
                    for s_drug in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout].keys(): 
                        es_drug.update( [s_drug,] )  # handle drug
    # oder endpoints
    ls_endpointset = list( es_endpointset )
    ls_endpointset.sort()
    ls_endpoint = list( es_endpoint )
    ls_endpoint.sort() 
    # for each biology 
    for s_biology in es_biology: 
        # for each biology layout combination
        for s_layout in es_layout: 
            # for each biology layout drug combination == plot
            for s_drug in es_drug:
                # set print plot flag
                b_plot = False   
                for s_endpointset in ls_endpointset:
                    # for each biology, endpointset,  endpoint, layout, drug, combination  == curve
                    for s_endpoint in ls_endpoint: 
                        # get dose key values for this s_biology, s_endpoint, s_layout, s_drug combination if they exist
                        try:
                            # bue: None response values get kicked later 
                            ls_dose = list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys() )
                            ls_dose.sort( key=float )
                            # for each plot one time 
                            if ( not b_plot ):
                                b_plot = True  # set plot flag 
                                plt.figure( figsize=t_figsize )  # set figure figsize=(11,8.5) figsize=(8.5,11) 
                                ax = plt.subplot( 1,1,1 )  # generate just one figure and one subplot
                                if ( tf_ylim != None ): 
                                    ax.set_ylim( tf_ylim )
                                plt.ylabel( '[endpoint]\n'+s_drug )  # set drug label
                                plt.xlabel( s_biology+'\n[dose_nM]', color='k' )  # set biology label
                                axtwin = ax.twinx()  # set twin axis
                                axtwin.yaxis.set_major_locator( MaxNLocator(nbins = 5) )
                                axtwin.set_ylim( [0, 1.5] )
                                i_color = 0  # set first color for refular endpoints
                                i_marker = 0  # set first marker for refular endpoints
                                i_markercellcount = 0  # seyt first marker for cellcount endpoints
                            # for each plot every time 
                            # set empty dose respone values 
                            ls_outdose = []
                            ls_outdosemed =[]
                            lf_outresponse = []
                            lf_outresponsemed = []
                            # get ajusted zero dose value
                            if ( ls_dose[0] == '0' ):
                                #s_dosezero = str(math.exp( ( math.log(float(ls_dose[1])) - ( ( math.log(float(ls_dose[-1])) - math.log(float(ls_dose[1])) ) / (len(ls_dose)-1) ) ) ))
                                s_dosezero = str( math.exp(( math.log(float(ls_dose[1])) - ( math.log(float(ls_dose[-1])) / (len(ls_dose)-1)) )) )
                            else: 
                                s_dosezero = ls_dose[0] 
                            # for each biology, endpointset, endpoint, layout, drug, dose get well and response float values median 
                            # bue : median should by this little number of replicates by  more robust then mean
                            for s_dose in ls_dose: 
                                lf_getresponse = []
                                for s_well in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                    s_measure = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][0]
                                    if ( s_measure != 'None' ):  # kick out None response values
                                        f_measure = float( s_measure )
                                        lf_getresponse.append( f_measure ) 
                                # handle dose zero
                                if ( s_dose == '0' ):
                                    s_dose = s_dosezero
                                # set plot coordinates  
                                if ( len(lf_getresponse) != 0 ):  # jump over n/a [] case
                                    f_getmedresponse = statistics.median( lf_getresponse )  
                                    lf_outresponse = lf_outresponse + lf_getresponse
                                    lf_outresponsemed.append( f_getmedresponse ) 
                                    ls_outdose = ls_outdose + ([s_dose] * len(lf_getresponse))  
                                    ls_outdosemed.append( s_dose )
                            # plot  
                            lf_outdosemed = [float(f) for f in ls_outdosemed]
                            lf_outdose = [float(f) for f in ls_outdose]
                            # plot cell count endpoint
                            if (s_endpoint in ts_cellcount):
                                axtwin.set_ylabel( '[cell_count]' )  # set y twin axis label (cell_count)
                                s_markercellcount = ls_markercellcount[i_markercellcount]  # get endpoint plot marker
                                axtwin.plot( lf_outdosemed, lf_outresponsemed, color='k', marker=s_markercellcount, fillstyle='none', linestyle='--', linewidth=3 )  # plot data slice
                                axtwin.scatter( lf_outdose, lf_outresponse, color='k', marker=s_markercellcount, alpha='0.5' )  # plot data slice, label=s_endpoint
                                i_markercellcount += 1
                                # ic calculation 
                                if ( b_ic ):
                                    # get intensity ic50 closest dose value 
                                    lf_diffic = [abs(f - 0.5) for f in lf_outresponsemed]
                                    f_doseic = lf_outdosemed[lf_diffic.index(min(lf_diffic))]
                                    f_responseic = lf_outresponsemed[lf_diffic.index(min(lf_diffic))]
                                    # check if any intensitivalue > ic20 
                                    b_ic20 = False 
                                    if ( min(lf_outresponsemed) < 0.8 ):
                                        b_ic20 = True 
                                    # add to output list
                                    s_key = s_biology+'_'+s_endpointset+'_'+s_endpoint+'_'+s_layout+'_'+s_drug
                                    t_output = ( s_biology, s_endpointset, s_endpoint, s_layout, s_drug, f_doseic, f_responseic, b_ic20 )
                                    if ( dt_ic == None ): 
                                        dt_ic = {s_key : t_output}
                                    else:
                                        dt_ic.update( {s_key:t_output} )
                                    # plot vertical ic line
                                    plt.vlines( f_doseic,0,1, colors='y' )
                            # plot any other endpoint
                            else:  
                                s_color = ls_color[i_color]  # get endpoint plot color 
                                s_marker = ls_marker[i_marker]  # get endpoint plot marker
                                ax.plot( lf_outdosemed, lf_outresponsemed, color=s_color, marker=s_marker, fillstyle='none', linestyle='-', linewidth=3 )  # plot data slice
                                ax.scatter( lf_outdose, lf_outresponse, color=s_color, marker=s_marker, alpha='0.5', label=s_endpoint )  # plot data slice
                                # set next regular color 
                                i_color += 1  
                                i_marker += 1
                        # nop biology endpointset endpoint layout drug combination is not existent 
                        except: 
                            pass 

                # is there any such biology endpoint drug dose combination plot
                if ( b_plot ):
                    # axes  
                    axtwin.set_xscale( 'log' )
                    ax.set_xscale( 'log' )
                    ax.yaxis.set_major_locator( MaxNLocator(nbins = 5) )
                    # grid
                    ax.grid( which='major', axis='y', linewidth=0.3, linestyle='-', color='0.9' )
                    ax.grid( which='major', axis='x', linewidth=0.3, linestyle='-', color='0.9' )
                    # legend
                    legend = ax.legend( loc='lower left', bbox_to_anchor=(0,1), ncol=6, fontsize='small', fancybox=True )
                    legend.get_frame().set_alpha( 0.3 )
                    # output
                    plt.savefig( s_outputfile + '_' + s_biology + '_' + s_layout + '_' + s_drug + '_pltcurve_endpoint.pdf', format='pdf' ) #file format png, svg, pdf and dots per inch dpi can be set!
                    #plt.show()
    # end 
    print( "o pyd7tbseluowr2pltcurveendpoint lt_ic :", dt_ic )
    return( dt_ic )



def pyd7tbseluowr2pltcurveviability( d7t_bseluowr, ts_cellcount=None, s_outputfile='prj_norm', tf_ylim=(0,1.5), t_figsize=(8.5, 8.5) ):
    """
    plot curve viability per drug out of a dddllf d7t_bseluowr object 
    input: 
        d7t_bseluowr: python dictionry of dictionay of biology, endpointset, endpoint, layout, drug, dose, well of response tuple (measure,rawdatafile) 
        ts_cellcount : tuple of cellcount endpoints. 
        s_outputfile : string with outputfile name. without extension. 
        tf_ylim : set y axis limit. None = auto scaling 
        t_figsize : tuple of flots, contains paper y length and x whide in inch.  
    output: 
        png file with plot 
    """
    # set color and marker palette 
    # purple, blue, lightblue, cyan, green, lightgreen, yellow, orange, lightred, red, brown, lightgrey, grey, darkgrey, 
    ls_color = ['#6F3D86','#352879','#6C5EB5','#70A4B2','#588D43','#9AD284','#B8C76F','#6F4F25','#9A6759','#68372B','#433900','#959595','#6C6C6C','#444444',]  
    ls_marker = ['.','v','*', 'o','^','s', '8','<','p',  'h','>','d',  'H',',','D',]
    ls_markercellcount = ['+','x','|','_',]
    #ls_marker =[',','.','_','|','+','x','*','v','^','<','>','s','d','D','p','h','H','8','o',] 

    # input 
    print( "i pyd7tbseluowr2pltcurveviability d7t_bseluowr : print suppresed" )
    #print( "i pyd7tbseluowr2pltcurveviability d7t_bseluowr :\n", d7t_bseluowr )
    print( "i pyd7tbseluowr2pltcurveviability ts_cellcount :", ts_cellcount )
    print( "i pyd7tbseluowr2pltcurveviability s_outputfile :", s_outputfile )
    print( "i pyd7tbseluowr2pltcurveviability tf_ylim :", tf_ylim )
    print( "i pyd7tbseluowr2pltcurveviability t_figsize :", t_figsize )
    # set empty output variable 
    b_output = False
    # empty set
    es_biology = set()
    es_endpointset = set()
    es_endpoint = set()
    es_layout = set()
    es_drug = set()
    # populate sets
    for s_biology in d7t_bseluowr.keys():
        es_biology.update( [s_biology,] )  # handle biology
        for s_endpointset in d7t_bseluowr[s_biology].keys():
            es_endpointset.update( [s_endpointset,] )  # handle endpoint
            for s_endpoint in d7t_bseluowr[s_biology][s_endpointset].keys():
                es_endpoint.update( [s_endpoint,] )  # handle endpoint
                for s_layout in d7t_bseluowr[s_biology][s_endpointset][s_endpoint].keys(): 
                    es_layout.update( [s_layout,] )  # handle layout
                    for s_drug in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout].keys(): 
                        es_drug.update( [s_drug,] )  # handle drug
    # oder biology
    ls_biology = list( es_biology )
    ls_biology.sort()
    # for each layout
    for s_layout in es_layout: 
        # for each drug  == plot 
        for s_drug in es_drug:
            b_plot = False # set print plot flag
            i_color = 0  # set first color for regular biology 
            # for each biology 
            for s_biology in ls_biology: 
                i_marker = 0  # set first marker for regular cellcount endpoint
                for s_endpointset in es_endpointset:
                    # for each biology : cell count endpoint combination  == curve
                    for s_endpoint in ts_cellcount:
                        # get dose key values for this s_biology, s_endpoint, s_layout, s_drug combination if they exist
                        try:
                            # bue: None response values get kicked later 
                            ls_dose = list( d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug].keys() )
                            ls_dose.sort( key=float )
                            # for each plot one time 
                            if ( not b_plot ):
                                b_plot = True  # set plot flag 
                                plt.figure( figsize=t_figsize )  # set figure figsize=(11,8.5) figsize=(8.5,11) 
                                ax = plt.subplot( 1,1,1 )  # generate just one figure and one subplot
                                plt.ylabel( '[viability]',  color='k' )  # set label
                                plt.xlabel( s_drug+'\n[dose_nM]', color='k' )  # set drug label
                            # for each plot every time 
                            # set empty dose respone values 
                            ls_outdose = []
                            ls_outdosemed =[]
                            lf_outresponse = []
                            lf_outresponsemed = []
                            # get ajusted zero dose value
                            if ( ls_dose[0] == '0' ):
                                #s_dosezero = str( math.exp(( math.log(float(ls_dose[1])) - ( ( math.log(float(ls_dose[-1])) - math.log(float(ls_dose[1])) ) / (len(ls_dose)-1) ) )) )
                                s_dosezero = str( math.exp(( math.log(float(ls_dose[1])) - ( math.log(float(ls_dose[-1])) / (len(ls_dose)-1)) )) )
                            else: 
                                s_dosezero = ls_dose[0] 
                            # for each biology, endpoint, layout, drug, dose get well and response float values median 
                            # bue : median should by this little number of replicates by  more robust then mean
                            for s_dose in ls_dose: 
                                lf_getresponse = []
                                for s_well in d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose].keys():
                                    s_measure = d7t_bseluowr[s_biology][s_endpointset][s_endpoint][s_layout][s_drug][s_dose][s_well][0]
                                    if ( s_measure != 'None' ):  # kick out None response values
                                        f_measure = float( s_measure) 
                                        lf_getresponse.append( f_measure ) 
                                # handle dose zero
                                if ( s_dose == '0' ):
                                    s_dose = s_dosezero
                                # set plot coordinates  
                                if ( len(lf_getresponse) != 0 ):  # jump over n/a [] case
                                    f_getmedresponse = statistics.median( lf_getresponse )  
                                    lf_outresponse = lf_outresponse + lf_getresponse
                                    lf_outresponsemed.append( f_getmedresponse ) 
                                    ls_outdose = ls_outdose + ([s_dose] * len(lf_getresponse))  
                                    ls_outdosemed.append( s_dose )
                            # plot any endpoint 
                            lf_outdosemed = [float(f) for f in ls_outdosemed] # get float median values 
                            lf_outdose = [float(f) for f in ls_outdose]  # get float real values
                            s_color = ls_color[i_color]  # get endpoint plot color 
                            s_marker = ls_marker[i_marker]  # get endpoint plot marker
                            ax.plot( lf_outdosemed, lf_outresponsemed, color=s_color, marker=s_marker, linestyle='-', linewidth=3, label=s_biology )  # plot data slice, fillstyle='none'
                            ax.scatter( lf_outdose, lf_outresponse, color=s_color, marker=s_marker, alpha='0.5' )  # bue: can be commented out to get the plot not to chaotic
                            # set next cell_count endpoint based marker 
                            i_marker += 1
                        # nop biology endpoint layout drug combination is not existent 
                        except: 
                            pass 
                    # set next biology based color 
                    i_color += 1

            # is there any such biology endpoint drug dose combination plot
            if ( b_plot ): 
                # axes  
                if ( tf_ylim != None ): 
                    ax.set_ylim( tf_ylim )
                ax.set_xscale( 'log' )
                ax.yaxis.set_major_locator( MaxNLocator( nbins=5 ) )
                # grid
                ax.grid( which='major', axis='y', linewidth=0.3, linestyle='-', color='0.9' )
                ax.grid( which='major', axis='x', linewidth=0.3, linestyle='-', color='0.9' )
                # legend
                legend = ax.legend( loc='lower left', bbox_to_anchor=(0,1), ncol=6, fontsize='small', fancybox=True )
                legend.get_frame().set_alpha( 0.3 )
                # output
                plt.savefig( s_outputfile + '_' + s_layout + '_' + s_drug + '_pltcurve_viability.pdf', format='pdf' )  # file format: png, svg, pdf; bue 2014-01-10: dpi dots per inch can be set!
                #plt.show()
    # end 
    b_output = True
    print( "o pyd7tbseluowr2pltcurveviability b_output :", b_output )
    return( b_output )


# module self test  
if __name__=='__main__':
    # drug response curve grid
    print('\n*** selftest py d7t bseluowr 2 plt curve plot ***')
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', s_dose='1', s_well='well1', t_response=(1,'rawabc',), d7t_bseluowr={})
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', s_dose='2', s_well='well2', t_response=(2,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='endpoint1', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well3', t_response=(2,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='endpoint2', s_layout='layout1', s_drug='drug1', s_dose='1', s_well='well4', t_response=(1,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='endpoint2', s_layout='layout1', s_drug='drug1', s_dose='2', s_well='well5', t_response=(3,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='endpoint2', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well6', t_response=(3,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='cellcount1', s_layout='layout1', s_drug='drug1', s_dose='1', s_well='well7', t_response=(2,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='cellcount1', s_layout='layout1', s_drug='drug1', s_dose='2', s_well='well8', t_response=(1,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='cellcount1', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well9', t_response=(1,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='cellcount2', s_layout='layout1', s_drug='drug1', s_dose='1', s_well='well10', t_response=(3,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='cellcount2', s_layout='layout1', s_drug='drug1', s_dose='2', s_well='well11', t_response=(2,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='cellcount2', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well12', t_response=(1,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='cellcount2', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well13', t_response=(1.1,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio1', s_endpointset='epset1', s_endpoint='cellcount2', s_layout='layout1', s_drug='drug1', s_dose='3', s_well='well14', t_response=(1.4,'rawabc',), d7t_bseluowr=d7t_test)

    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='endpoint1', s_layout='layout2', s_drug='drug2', s_dose='10', s_well='well1', t_response=(10,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='endpoint1', s_layout='layout2', s_drug='drug2', s_dose='20', s_well='well2', t_response=(20,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='endpoint1', s_layout='layout2', s_drug='drug2', s_dose='30', s_well='well3', t_response=(20,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='endpoint2', s_layout='layout2', s_drug='drug2', s_dose='10', s_well='well4', t_response=(10,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='endpoint2', s_layout='layout2', s_drug='drug2', s_dose='20', s_well='well5', t_response=(30,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='endpoint2', s_layout='layout2', s_drug='drug2', s_dose='30', s_well='well6', t_response=(30,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='cellcount1', s_layout='layout2', s_drug='drug2', s_dose='10', s_well='well7', t_response=(20,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='cellcount1', s_layout='layout2', s_drug='drug2', s_dose='20', s_well='well8', t_response=(10,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='cellcount1', s_layout='layout2', s_drug='drug2', s_dose='30', s_well='well9', t_response=(10,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='cellcount2', s_layout='layout2', s_drug='drug2', s_dose='10', s_well='well10', t_response=(30,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='cellcount2', s_layout='layout2', s_drug='drug2', s_dose='20', s_well='well11', t_response=(20,'rawabc',), d7t_bseluowr=d7t_test)
    d7t_test = pyd7tbseluowr2populate(s_biology='bio2', s_endpointset='epset1', s_endpoint='cellcount2', s_layout='layout2', s_drug='drug2', s_dose='30', s_well='well12', t_response=(10,'rawabc',), d7t_bseluowr=d7t_test)


    # test runs 
    print('\n*** selftest py d7t bseluowr 2 plt curve endpoint plot ***')
    dt_out = pyd7tbseluowr2pltcurveendpoint(d7t_bseluowr=d7t_test, ts_cellcount=('cellcount1','cellcount2',), s_outputfile='selftest', tf_ylim=None, t_figsize=(8.5,8.5), b_ic=True)
    print("o ic dt_out : ", dt_out)

    print('\n*** selftest py d7t bseluowr 2 plt curve viability plot ***')
    pyd7tbseluowr2pltcurveviability(d7t_bseluowr=d7t_test, ts_cellcount=('cellcount1','cellcount2',), s_outputfile='selftest', tf_ylim=(0,1.5), t_figsize=(8.5,8.5))
